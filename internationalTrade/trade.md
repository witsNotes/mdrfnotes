---
title : "International trade"
---

# Autarky

This is a state of not trading with any other nation

# Absolute advantage

Being able to produce a good at a lower labour cost (measured in hours) than other countries.

# Comparative advantage

Being able to produce a good at a lower opportunity cost in terms of other goods than another country.

Country A has comparative advantage in good x if

$$\frac{a_x}{a_y} < {b_x}{b_y}$$

If country A has a comparitive advantage in production of good x then country B must have a comparitive advantage in the production of good y.

# Riccardian model

##Assumptions

 #. Two country 2 commodity world
 #. Perfect compition
 #. No transport costs
 #. Factors mobile internally, immobile
 #. Constant costs of production
 #. Fixed technology for each country
 #. All resources are fully employed
 #. The labour theory of value holds

## Notation

let

$a_x$ be the labour time to produce one unit of x in country a.

## Gains from trade

Ricardo's argument is that trade will be mutually advantageous as long as the two countries' autarky price ratios are different

## Production Possibility Frountier

The PPF is the set of all combnations of goods that a country is cable of producing, given available tchnology and resources.

### Proof

Show that given a certain amount of one good consumed each country can then consume more of the other good. Outside the original ppf. 

## Specialization

In general the ricardian model predicts complete specialization. However if the two economies differ dramatically in size the smaller country may not bee able to produce enough of the good they specialize in to fufile both economies demand.


## Consumption Possibilities Frontier

The CPF is the collection of points tha represent combinations of corn and blankets that a country can consume if it trades.

the CPFS slope is the same as the terms of trade.

The CPF pivots around the production point

If trade is to benefit og a country the cpf lies outside the ppf.

## Limits to mutually advantagious trade

The "Exchange rate" must be at least t
