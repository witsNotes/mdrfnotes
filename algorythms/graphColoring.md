---
title: "Graph coloring"
---

# Graph coloring

Sort the vertexes by thier degree and then set the color to be the smallest color not used by a neighbor.
 
Do this with a quick sort. 

```{haskell}

data Vertex x = Node {x [Vertex]}

instance x Ord where
>= is definied

```

Vertex

```{haskell}

graphColoring [color] -> [Vertex a ] -> [colors] 
graphColoring color head 
```

Make a list of unused colors of neighbouer.

This used array must be cleared after every matrix.

The full cost is 

In the worst case we are using $$\Theta(n^{2})$$
