---
title : "graphs"
---


# Definitions

Vertices or node

Edges or links
 
* Neighbours
    Two vertices are neighbours if a edge connects them

* A connected graph
    It is possible to get from every node to every other node via some combination of edges.

* A walk
    A list of vertices with vertices in between them
    Or a list of edges
    (1,3)(3,1)(1,4)(4,3)(3,0)
    No teleportation allowed in walks

* A path
    A walk in which all the vertices are distinct

For any graph we say the graph is complete if there is an edge between any vertex and any other vertex.


We define a graph by a list of vertices and edges

V = {1,2,3}
E = {(1,2)(1,3)}

By this principle there is only one complete graphs for any number

# Directed graph

May contain a two way edge.

In the same way we must see if the two graphs are connected.

# Weighted graph 

Where all edges have a weight.

# Computer representation

## Primitive idea

Store a list of node and a list of edges.

## Adjacency list

An adjacency is an edge.

```{haskell}
data Vertex a = {Node :: a , Nieghbours :: [Vertex a]

data weightedVertex a ={Node :: a Neighbours ::[(int, weightedEdgeVertex a )}
-- Same graph function.
data Graph a= [Vertex a]
```


## Adjacency matrix

Make a matrix with all nodes listed above and down the side. Mark all connections as true(Or at cost) and false if there is no connection.

Often we can just stick -1 into none existent nodes.
Or just make them max values.

## Times

n := number of nodes
m := number of edges

Check if a connection exists.

* Adjacency matrix O(1)
* Adjacency list $\Theta (m)$

Construction time

* Adjacency list 0(m) worst case m $\approx n^{2}$
* Adjacency matrix $O(n^{2})$

### Degree of a matrix

* Adjacency matrix $\Theta(n)$
* Adjacency list $\theta(1)$ or n dependeing on the language

# Result.

For the most parts lists are more efficient.

Paths can be found easily of a given length by doing matrix opperations on the matrix.


# Trees

This means that the graph is a cyclic So no looping back on yourself

So every vertex can have only one parent

## Parent array

For each array we only keep the parent of that node.

# Planner graph

One in which the lines can't overlap.

# Algorithms

## Graph coloring.

You have some vertices and edges.

Use the minimum colours to color each node separately.

For planner graphs all we can use 4 colors.

N(P) problems.
