---
title : "Trees"
---

# Trees

A connected a cyclic graph.

Is equivalent to saying the graph is connected and has n-1 edges.

# Proof

# Prove by induction

## Base case

1 vertices and no edges

If we add a node on to any tree we must attach it and to only one parent. 

Assume true for a tree of size n.

RTP That the statment holds for trees with n+1 vertices.


Let T be a tree with n+1 vertices

So lets remove a vertex to get something we know about. This must be a leaf as trees have to be connected.

T prime is connected becouse v could not have been on the path between any other vertexes.

T prime is a cylcic becouse removeing an edge can't produce a cycle so t prime has n vertixes and is a tree so it has n edges.
