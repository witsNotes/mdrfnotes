---
title : "algorithms"
---

We can get ride of our constant factors

```{java}
public void search(int[] arr, int key{
  For int i = 0, i < arr.length();i++{
  if arr[i] = key
  return i
  }
  return -1
}
```

We don't know what percentage of the time we spend on each case so we must use best case and worst.

Best case $\Theta 1$

Worst case $\theta n$

We only count the expression which dominates the cost asymptotically.

# Bubble sort is 

$n^2$



# Merge sort

```{haskell}
mergeSort:: [a] ->  [a]
mergeSort a:[] = a:[]
mergeSort = merge {--theta n--} (mergeSort take half a) (mergeSorta drop half )


merge :: [a] -> [a] -> [a]
merge a b
    | isEmpty a = b
    | isEmpty b = a
    | head a > head b = head b : merge (tail b) (a);
    | otherwise = head a : merge (tail a) (b)
```


If we copy each item each time we split the array it costs us n each time.

If we are combining we get can at most have n comparisons so over the entire stretch.

So we have n times height.

## Height

$$2^{levels} = n$$
Solving for n we get that the number of levels is log n.

Graphs can have two or more parameters.


This analogous to the height of a binary tree O(n)
```
