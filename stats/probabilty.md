---
title: "Probability"
---

# Probability

Probabilty is a measure of the likelyhood that an event will occor, it qauntifys an event into a number and is measured on a scale between zero and one. where one is absolute certainty and zero is immposibilty.

# Sample space

Is all possible outcomes with thier probabilities.

All Probabilities sum to 1

The probability of two independent events is the product of the probabilities

A' is the probability of a not happening.

let b be the event that the card chosen is a prime. 2,3,5,7

Thus the probability of picking a prime is $\frac{4}{9}$

# Mutually execulsive events.

For two mutually exclusive events the probability of a or b is probability of a plus the probability of b.

# Non mutally exculsive events.

The proabilty of a or c = probabilty of a plus the probabilty of b - the probability of a and b.

s can be written in a venn diagram.

Complete a Venn diagram with events a be and c.

A set a is said to be a subset of set b if each element of a is also an element of set b.

# Counting methods

Counting the number of arrangement

Number of arrangements is given by a factorial.

# Arrangements with smaller number of elements than the population.

Choosing $nPr$ = $\frac{n!}{n-r}$

# Permutations with non distinct objects

take the word mississippi 11 different letters

$$=\frac{11!}{4!4!2!}$$

## Order

In a permutation the order of the objects is important in counting the number of permutations.

## Combinations

Here the order does not matter when counting the number of arrangments.

$$\frac{n!}{(n-1!)n!}$$

# Example 3.7

Arrange 3 letters from the word

Atmospheric
$$11C3 = \frac{11!}{11-3}{3!}= 165$$

# Two populations

The ways in which one can choose from the one populations $\time$ the ways to choose from the second population.



# Porability laws

  #. $p(A \union B') = p(A) - p(A \union B)$
  #. $p(A \union B') = p(A \union) - p(B)$

Given
P(A) = 0.4
P(B) = 0.3
P(a union B) = 0.1

find

*P(A|B)
* P(B|A)
* P(A| A \union B)
* P(A | A \intersection B)
* P(A \intersection B | A \union B)
