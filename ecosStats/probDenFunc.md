---
title : "Probability density function"
---

# Probability density function

This is the derivative of the cumulative density function for a continuous probability distribution.
##__ Cumulative density function __


# Cumulative density function.

This function gives the probability that x will take a value of less than or equal to a $x_0$
