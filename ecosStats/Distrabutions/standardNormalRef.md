---
title : "Standard Normal Distribution"
---

# Imports

import distroFuncs as df

import zScore as zS


# Normal distribution

This is a very common continuous distribution 

There are numerous normal distributions which can all be simple transformed onto one standard normal distribution which all the values are than calculated of.


>### Definition
>
>This is a normal distribution with a mean of 0 and a variance and standard deviation of 1
>

In order to find the probability the x is less than X we use the $Phi$ on the equivalent value to X from the standard normal distribution.

Giving us

$$CDF(X) = \Phi \circ z(X)$$

>### $\Phi$
>
> ># CDF
>>
>>The cumulative density function (CDF)
>>
>>This function gives the probability that x will take a value of less than or equal to a $x_0$
>
>
>The cumulative function of the standard normal distribution is often called $\Phi$. It is worth noting that $\Phi$ is symmetrical about 0 such that for $X < 0$ 
>
>$$\Phi(X) = P(x<X) = 1 -P(x > X) \text(Continious) = 1 - P(-x < -X) = 1 - P(x < -X)\text{Symmetry} = 1 - \phi{-X}$$
>

># Z score
>
>A z score is a measure of how many standard deviations a measurement is from the mean
>
>This is given by
>
>$$z(x) = frac{x-\mu}{\sigma}$$


## Standard normal distribution

### Definition

This is a normal distribution with a mean of 0 and a variance and standard deviation of 1

### $\Phi$

># CDF
>
>The cumulative density function (CDF)
>
>This function gives the probability that x will take a value of less than or equal to a $x_0$


The cumulative function of the standard normal distribution is often called $\Phi$. It is worth noting that $\Phi$ is symmetrical about 0 such that for $X < 0$ 

$$\Phi(X) = P(x<X) = 1 -P(x > X) \text(Continious) = 1 - P(-x < -X) = 1 - P(x < -X)\text{Symmetry} = 1 - \phi{-X}$$


# Appendix

