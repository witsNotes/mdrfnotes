---
title : "Standard Normal Distribution"
---

# Imports

import distroFuncs as df

import zScore as zS


# Normal distribution

This is a very common continuous distribution 

There are numerous normal distributions which can all be simple transformed onto one standard normal distribution which all the values are than calculated of.


## __ Standard normal distribution ; Definition __

In order to find the probability the x is less than X we use the $Phi$ on the equivalent value to X from the standard normal distribution.

Giving us

$$CDF(X) = \Phi \circ z(X)$$

### __ $\Phi$ __

### __zS: Z score __


## Standard normal distribution

### Definition

This is a normal distribution with a mean of 0 and a variance and standard deviation of 1

### $\Phi$

####__df: CDF __


The cumulative function of the standard normal distribution is often called $\Phi$. It is worth noting that $\Phi$ is symmetrical about 0 such that for $X < 0$ 

$$\Phi(X) = P(x<X) = 1 -P(x > X) \text(Continious) = 1 - P(-x < -X) = 1 - P(x < -X)\text{Symmetry} = 1 - \phi{-X}$$

