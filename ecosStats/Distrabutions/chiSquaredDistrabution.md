---
title: "Chi Squared Distrabution"
---

# Chi squared distribution 
# Summary

The chi squared distribution is used to test how a population may be devided into descrete groups. ie what proportion of the population falls into each group.

This is done by calculating a goodness of fit measure between how many observations are predicted by a model to fall into each category and how many observations actually do.

# $\chi^{2}$ The goodness of fit statistic

$$\chi^{2} = \sum \frac{\text{observed cell count - expected cell count}}{\text expected cell count}$$

$\chi^{2}$ measures the difference between the expected and observed cell count. So large values of $\chi ^{2}$ mean that the model used to generate the expected cell count is unlikely to be an accurate estimator for the population.




