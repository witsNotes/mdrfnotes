---
title : "Student t Distribution"
---

# Imports
import distroFuncs as df

# Student t distribution

A Student t distribution is the distribution of a sample taken from a normally distributed population. The exact distribution is different depending on the sample size.

First we find a t score in a analogous we to moving a normal distribution onto onto the standard normal distribution.

$$t = \frac{\bar{x} - x}{s_\bar{x}}$$

Than compare this value with the equivalent value of a certain percentage chance from the CDF of the T distribution.

The t score that is taken for comparison is dependent on the exact T distribution based on the degrees of freedom. Given by n-1
