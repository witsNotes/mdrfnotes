---
title : "Probability density function"
---

# Probability density function

This is the derivative of the cumulative density function for a continuous probability distribution.
##__ CDF __


# CDF

The cumulative density function (CDF)

This function gives the probability that x will take a value of less than or equal to a $x_0$
