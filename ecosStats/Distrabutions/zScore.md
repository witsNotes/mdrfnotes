---
title: "z Score"
---

# Z score

A z score is a measure of how many standard deviations a measurement is from the mean

This is given by

$$z(x) = frac{x-\mu}{\sigma}$$
