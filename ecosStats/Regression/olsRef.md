---
title: "OLS estimator"
---

### OLS estimator


### OLS


The OLS is an estimator. Is a way of estimating parameters of a statistical model by finding values of the parameters which minimize the square of the difference of the observed data points from equivalent data points predated by the model.
The OLS is the ordinary(unweighted least squares this is considered a desirable estimator.

The estimator is unbiased

As the number of samples increase the estimator consistently has a better chance of of being closer to the population mean.

Efficiency is how fast the distribution of the sample converges to yield the same value for parameters as the population as the sample size increases

### BLUE

It can be shown under guass-merkov conditions that the least of of sqaures yields a  blue (best(most efficient) linear unbiased estimator).

### Derivation

The derivation of the ols estimator is usually done by using first order condition  $\frac{\partial{\epsilon}}{\partial{x_i}} = 0$
And solving the simultaneous equations given for the all the parameters.



# Appendix

