---
title : "Simple Regression"
---

### Imports

import ols as ols

### Simple regression

Here we model the relationship between two variables using a straight line

We calculate a line of best fit to find the values of our parameters. 

The mathematical model for our data should be in the form

### Mathematical model

$$y = \beta_0 + \beta_1 x$$

where $\frac{\partial{y}}{\partial{x}} = \beta_0$

### Stochastic model

$$y = \beta_0 + \beta_1 x + \epsilon$$

># Line of best fit
>
>If we only have a sample. We use the OLS to find the line of best fit for the sample.
>
> ># OLS estimator
>>
>>
> >## OLS
>>
>>
>>The OLS is an estimator. Is a way of estimating parameters of a statistical model by finding values of the parameters which minimize the square of the difference of the observed data points from equivalent data points predated by the model.
>>The OLS is the ordinary(unweighted least squares this is considered a desirable estimator.
>>
>>The estimator is unbiased
>>
>>As the number of samples increase the estimator consistently has a better chance of of being closer to the population mean.
>>
>>Efficiency is how fast the distribution of the sample converges to yield the same value for parameters as the population as the sample size increases
>>
> >### BLUE
>>
>>It can be shown under guass-merkov conditions that the least of of sqaures yields a  blue (best(most efficient) linear unbiased estimator).
>>
> >### Derivation
>>
>>The derivation of the ols estimator is usually done by using first order condition  $\frac{\partial{\epsilon}}{\partial{x_i}} = 0$
>>And solving the simultaneous equations given for the all the parameters.
>>
>>
>
>## Derivation
>
>We want to find $\beta_1$ and $\beta_2$ such that we minimize the sum of squares
>
> >## Minimizing
>>
> >### Result
>>
>>For estimator taking the form
>>$$\hat{y} = \beta_1 + \beta_2 x$$
>>
>>We must find values of the parameters which minimize.
>>
>>$$S = \sum(y_i - \hat{y})^2$$
>>
>>Which are given by
>>
>>$$\beta_2 = \frac{\sum(x-x_i)(y-y_i)}{\sum(x-x_i)}$$
>>
>>And
>>
>>$$\beta_1 = \bar{y} - \beta{2}\bar{x}$$
>>
> >### Proof
>>
>>We do this by finding the stationary points with respect to our parameters($\beta_1, \beta_2$).
>>
> > >#### FOC i
>>>
> > >##### Result
>>>
>>>$$\sum (y_i -\beta_1 - \beta_2 x_i) = 0$$
>>>
> > >##### Proof
>>>$$\frac{d}{d\hat{\beta_1}}S = 0$$
>>>We then substitute $\hat{y}$ with $\beta_1 + x\beta_2$ in and do the differentiation
>>>$$-2 \sum (y_i -\beta_1 - \beta_2 x_i) = 0$$
>>>Dividing out $-2$
>>>$$\sum (y_i -\beta_1 - \beta_2 x_i) = 0$$
>>>
>>
>>Gives us
>>
>>i) $\sum (y_i -\beta_1 - \beta_2 x_i) = 0$
>>
>>
> > >#### FOC ii
>>>
> > >##### Result
>>>
>>>$$=\sum x_i (y_i - \beta_1 - \beta_2 x_i)=0$$
>>>
> > >##### Proof
>>>
>>>$$= \frac{d}{d\hat{\beta_2}}S$$
>>>We then substitute $\hat{y}$ with $\beta_1 + x\beta_2$ in and do the differentiation
>>>$$= -2 \sum x_i (y_i - \beta_1 - \beta_2 x_i)=0$$
>>>$$=\sum x_i (y_i - \beta_1 - \beta_2 x_i)=0$$
>>>
>>
>>Gives us 
>>
>>ii)$\sum (y_i -\beta_1 - \beta_2 x_i) = 0$
>>
>>
>>
> > >#### Solving i for $\bar{y}$
>>>
> > >##### Result
>>>
>>>$$\bar{y} = \beta_1 + \beta_2 \bar{x}$$
>>>
> > >##### Proof
>>>
>>>
>>>$$\sum (y_i -\beta_1 - \beta_2 x_i) = 0$$
>>>
>>>$$\implies \sum (y_i) = \sum\beta_1 + \sum \beta_2 x_i$$
>>>
> > > >### Lemma 1
>>>>
>>>>$$\sum x_i = n \bar{x}$$
>>>>
>>>
>>>$$\implies n \bar{y} = n \beta_1 + n\beta_2 \bar{x}$$
>>>
>>>$$\implies \bar{y} =  \beta_1 + \beta_2 \bar{x}$$
>>>
>>>
>>
>>Gives use 
>>
>>iii)$\bar{y} = \beta_1 + \beta_2 \bar{x}$
>>and
>>iv)$\beta_1 =\bar{y}-+ \beta_2 \bar{x}$
>>
> > >#### Solving ii and iv for $\sum x_i y_i$
>>>
> > >##### Result
>>>
>>>$$\sum x_i y_i = n \bar{x} \bar{y} -\beta_2 n \bar{x}^2 + \beta_2 {x_i}^2$$
>>>
> > >##### Proof
>>>
>>>
>>>$$\sum x_i y_i = \beta_1 \sum x_i + \beta_2 {x_i}^2$$
>>>
>>>Using Lemma 1
>>>
>>>$$\sum x_i y_i = \beta_1 n \bar{x} + \beta_2 {x_i}^2$$
>>>
>>>Using iv
>>>
>>>$$\sum x_i y_i = (\bar{y} - \beta_2\bar{x})n \bar{x} + \beta_2 {x_i}^2$$
>>>
>>>Giving
>>>$$= n \bar{x} \bar{y} -\beta_2 n \bar{x}^2 + \beta_2 {x_i}^2$$
>>>
>>
>>Gives us 
>>
>>v)$\sum x_i y_i = n \bar{x} \bar{y} -\beta_2 n \bar{x}^2 + \beta_2 {x_i}^2$
>>
> > >#### Solving v for $\beta_2$
>>>
>>>
> > >##### Result
>>>
>>>$$\beta_2 = \frac{\sum(x-x_i)(y-y_i)}{\sum (x-x_i)}$$
>>>
> > >##### Proof
>>>
>>>Solving v for $\beta_2$ gives
>>>
>>>$$\beta_2 = \frac{\sum (x_i y_i) - n \bar{x} \bar{y}}{\sum x - n \bar{x}}$$
>>>
>>>Applying lemma 2
>>>
>>>$$\beta_2 = \frac{\sum (x_i y_i ) - \bar{x} \bar{y}}{\sum x - n \bar{x}}$$
>>>
>>>$$\beta_2 = \frac{\sum (x_i y_i) - n \bar{x} \bar{y}}{\sum x_i^2 - \bar{x} x_i}$$
>>>
>>
>>Gives us 
>>
>>vi) $\beta_2 = \frac{\sum(x-x_i)(y-y_i)}{\sum (x-x_i)}$
>>
>>And finally solving for $\beta_1$ gives us
>>
>>vii) $\beta_1 = \bar{y} - \beta_2 \bar{x}$
>>
>


# Appendix

