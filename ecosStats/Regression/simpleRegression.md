---
title : "Simple Regression"
---

# Imports

import ols as ols

# Simple regression

Here we model the relationship between two variables using a straight line

We calculate a line of best fit to find the values of our parameters. 

The mathematical model for our data should be in the form

## Mathematical model

$$y = \beta_0 + \beta_1 x$$

where $\frac{\partial{y}}{\partial{x}} = \beta_0$

## Stochastic model

$$y = \beta_0 + \beta_1 x + \epsilon$$

### __Line of best fit__

# **Line of best fit**

If we only have a sample. We use the OLS to find the line of best fit for the sample.

## __ols:OLS estimator__

## Derivation

We want to find $\beta_1$ and $\beta_2$ such that we minimize the sum of squares

### __Line of best fit; Minimizing __

## **Minimizing**

### Result

For estimator taking the form
$$\hat{y} = \beta_1 + \beta_2 x$$

We must find values of the parameters which minimize.

$$S = \sum(y_i - \hat{y})^2$$

Which are given by

$$\beta_2 = \frac{\sum(x-x_i)(y-y_i)}{\sum(x-x_i)}$$

And

$$\beta_1 = \bar{y} - \beta{2}\bar{x}$$

### Proof

We do this by finding the stationary points with respect to our parameters($\beta_1, \beta_2$).

#### __ Line of best fit; Minimizing; Proof; FOC i __

Gives us

i) $\sum (y_i -\beta_1 - \beta_2 x_i) = 0$


#### __ Line of best fit; Minimizing; Proof; FOC ii __

Gives us 

ii)$\sum (y_i -\beta_1 - \beta_2 x_i) = 0$



#### __ Line of best fit; Minimizing ; Proof ;Solving i for $\bar{y}$ __

Gives use 

iii)$\bar{y} = \beta_1 + \beta_2 \bar{x}$
and
iv)$\beta_1 =\bar{y}-+ \beta_2 \bar{x}$

#### __ Line of best fit; Minimizing;Proof ;Solving ii and iv for $\sum x_i y_i$ __

Gives us 

v)$\sum x_i y_i = n \bar{x} \bar{y} -\beta_2 n \bar{x}^2 + \beta_2 {x_i}^2$

#### __Line of best fit; Minimizing; Proof; Solving v for $\beta_2$__

Gives us 

vi) $\beta_2 = \frac{\sum(x-x_i)(y-y_i)}{\sum (x-x_i)}$

And finally solving for $\beta_1$ gives us

vii) $\beta_1 = \bar{y} - \beta_2 \bar{x}$

#### **FOC i**

##### Result

$$\sum (y_i -\beta_1 - \beta_2 x_i) = 0$$

##### Proof
$$\frac{d}{d\hat{\beta_1}}S = 0$$
We then substitute $\hat{y}$ with $\beta_1 + x\beta_2$ in and do the differentiation
$$-2 \sum (y_i -\beta_1 - \beta_2 x_i) = 0$$
Dividing out $-2$
$$\sum (y_i -\beta_1 - \beta_2 x_i) = 0$$

#### **FOC ii**

##### Result

$$=\sum x_i (y_i - \beta_1 - \beta_2 x_i)=0$$

##### Proof

$$= \frac{d}{d\hat{\beta_2}}S$$
We then substitute $\hat{y}$ with $\beta_1 + x\beta_2$ in and do the differentiation
$$= -2 \sum x_i (y_i - \beta_1 - \beta_2 x_i)=0$$
$$=\sum x_i (y_i - \beta_1 - \beta_2 x_i)=0$$

#### **Solving v for $\beta_2$**


##### Result

$$\beta_2 = \frac{\sum(x-x_i)(y-y_i)}{\sum (x-x_i)}$$

##### Proof

Solving v for $\beta_2$ gives

$$\beta_2 = \frac{\sum (x_i y_i) - n \bar{x} \bar{y}}{\sum x - n \bar{x}}$$

Applying lemma 2

$$\beta_2 = \frac{\sum (x_i y_i ) - \bar{x} \bar{y}}{\sum x - n \bar{x}}$$

$$\beta_2 = \frac{\sum (x_i y_i) - n \bar{x} \bar{y}}{\sum x_i^2 - \bar{x} x_i}$$

#### **Solving ii and iv for $\sum x_i y_i$**

##### Result

$$\sum x_i y_i = n \bar{x} \bar{y} -\beta_2 n \bar{x}^2 + \beta_2 {x_i}^2$$

##### Proof


$$\sum x_i y_i = \beta_1 \sum x_i + \beta_2 {x_i}^2$$

Using Lemma 1

$$\sum x_i y_i = \beta_1 n \bar{x} + \beta_2 {x_i}^2$$

Using iv

$$\sum x_i y_i = (\bar{y} - \beta_2\bar{x})n \bar{x} + \beta_2 {x_i}^2$$

Giving
$$= n \bar{x} \bar{y} -\beta_2 n \bar{x}^2 + \beta_2 {x_i}^2$$

#### **Solving i for $\bar{y}$**

##### Result

$$\bar{y} = \beta_1 + \beta_2 \bar{x}$$

##### Proof


$$\sum (y_i -\beta_1 - \beta_2 x_i) = 0$$

$$\implies \sum (y_i) = \sum\beta_1 + \sum \beta_2 x_i$$

###### __ Line of best fit; Minimizing; Lemma 1 __ 

$$\implies n \bar{y} = n \beta_1 + n\beta_2 \bar{x}$$

$$\implies \bar{y} =  \beta_1 + \beta_2 \bar{x}$$


### **Lemma 1**

$$\sum x_i = n \bar{x}$$

### **Lemma 2**

#### Result

$$\sum (x_i - \bar{x})(y_i - \bar{y}) =\sum y_i (x-\bar{x})$$

#### Proof
Multiplying out and using associativity of addition
$$sum (x_i - \bar{x})(y_i - \bar{y}) &= \sum (x_i y_i) - \sum x_i \bar{y} - \sum \bar{x} y_i + \sum \bar{x}\bar{y}$$

Factoring out constants

$$= \sum (x_iy y_i) - \bar{y}\sum{x_i} - \bar{x}\sum (y_i) + \bar{x}\bar{y}\sum 1$$

Using __Line of best fit; Minimizing; Lemma 1__ and the sum of a constant sequence

$$=\sum (x_i y_i) - n \bar{x}\bar{y} - n \bar{x}\bar{y} + n\bar{x}\bar{y}$$


Canceling out gives

$$\sum (x_i y_i) - n \bar{x}\bar{y}$$

Using Lemma 1 in reverse

$$=\sum (x_i y_i) - \bar{x} \sum y_i = \sum x_i y_i - \bar{x} y_i$$

$$=\sum y_i (x-\bar{x})$$

se a least squared estimator
