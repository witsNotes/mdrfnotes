---
title: "Goodness of fit"
---

# $R^{2}$

This is a test for the overall goodness of fit of a linear regression model

Is a model adequate or not.

# $r^2$

Show what proportion of the variation of y that the model predicts.

Decomposition of total variation

$$Y_i = \hat{Y_i} - e_i$$

$$Y_i = \hat{Y_i} - 


# Coefficient of determination.

$$Y_i - \bar{Y} = (\hat($$

$$0 \leq r^2 \leq 1$$ 

1 means perfect correlation

0 means no relationship



