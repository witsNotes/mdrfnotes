---
title: "Regression analysis"
---

# Imports

import ols as ols

import simpleRegression as sr

import classicalRegression as cr

import multiDimAnalysis as mr 

# Regression analysis

## Assumptions

Since the points are unlikely to fall precisely on the line the exact linear equation must be modified to include a random disturbance or stochastic term $\mu_i$

The term is assumed to be

* Normally distributed
* Have an expected value of zero
* Have a constant variance 
* That the error terms are uncorrelated with each other
* That the explanatory variable assumes fixed values in repeated sampling

### Linearity in parameters

The two variable linear model is used for testing hypotheses about the relationship between a dependent variable Y and an independent to explanatory variable X and for prediction.

Simple regression analysis involves plotting a scatter diagram of x and y values and seeing if there is an approximate linear relationship between the two.

$Y_i = b_0 + b_1X_1$

## Warnings

Not x is fixed by assumption not by default.

## Common values

### RSS

The residual sum of squares measures the sum of the squares of the differences between the observations and the mathematical model predications. 

$$RSS = \sum e_i^{2}$$

## Specific models

## __ sr :Simple regression __ 

## __ cr: Classical regression model__ 

## __ mr :Multi variable regression__

