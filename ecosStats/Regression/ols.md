---
title: "OLS estimator"
---

# OLS estimator


## OLS


The OLS is an estimator. Is a way of estimating parameters of a statistical model by finding values of the parameters which minimize the square of the difference of the observed data points from equivalent data points predated by the model.
The OLS is the ordinary(unweighted least squares this is considered a desirable estimator.

The estimator is unbiased

As the number of samples increase the estimator consistently has a better chance of of being closer to the population mean.

Efficiency is how fast the distribution of the sample converges to yield the same value for parameters as the population as the sample size increases

### BLUE

It can be shown under guass-merkov conditions that the least of of sqaures yields a  blue (best(most efficient) linear unbiased estimator).

### Derivation

The derivation of the ols estimator is usually done by using first order condition  $\frac{\partial{\epsilon}}{\partial{x_i}} = 0$
And solving the simultaneous equations given for the all the parameters. 


### Sampling distribution

In order to find the sampling distribution we use the clrm assumptions.

This is that all the samples form a normal distribution  around the line of regression.

![Normal distrabution around regression line](olsPics/normalDistrabutionAroundRegressionLine.jpg)


If the error term is normally distributed by some property of the normal distribution so are the observed values of y with a mean of the mathimatical model and the same variance as the error terms.


#### Variance and standard error

The variance and standard error of the estimators is given by

\begin{align*}

\sigma^{2}_{\hat{\Beta_1} = \frac{\sum X_i^{2}}{n \sum (X_i - \bar{X})^{2}}\times \sigma^{2}}
\sigma^{2}_{\hat{\Beta_2} = \frac{\sigma^{2}}{\sum x_i^{2}}\times \sigma}

\end{align*}

#### T score

ore often we use the t score calculated by

$$t_{n-2} \approx \frac{\hat{\Beta_2 - \Beta_{2H}}}{se(\hat{\Beta_2})}$$

We lose 2 degrees of freedom in calculating $\hat{\sigma^{2}}$

$$se(\hat{\Beta}) = \frac{\hat{\sigma}}{\sqrt{\sum x_i^{2}}}$$

$$\hat{\sigma}^{2} = \frac{\sum e_i^{2}}{n-k}$$
$$$$
