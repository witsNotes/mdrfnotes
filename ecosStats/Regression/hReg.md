# Expect the error term to be normally distributed. We can't use the normal test as we need population statistics which we dont have.
So we must use a t test.

If we are not explicit about the relationship between the two variables we have a two tailed test.

\begin{align*}
H_0: \beta_2 = 0 \\
H_1: \beta_2 \neq 0 \\
\alpha = 0.05\\
df = 5-2 = 3
\end{align*}

Step 2 

$$t = \frac{\hat{\beta_2} ...}$ 3.657

Find a t score

Compare with the $5\%$ score from the t distros 3.184

Conclusion there is come evidence for a significant relationship.

## Confidence interval approach

The confidence interval

$P[\Beta_2 \pm t_{\frac{a}{2} n-k} se{\beta_2}} = 1 - \alpha$

So our hypothisized beta falls outside our confidence interval.

# One tail test

\begin{align*}

H_0: \beta_2 \geq 0 \\
H_1: \beta_2 > 0 
\end{align*}

We use the first row on the table for one tailed tests.

For a hypothisis not with 0 use the hypothsed value to calculate the t statistic.
