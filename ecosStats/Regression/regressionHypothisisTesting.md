---
title : "Hypothesis testing for regression analysis"
author : "Jezri Krinsky"
---

# Imports

import ../HypothisisTesting/hTesting as ht
import ../Distrabutions/studentT as st

# Regression analysis hypothesis testing

## Testing the significance of one parameter
The point of hypothesis testing with regression analysis is to test weather a parameter is significantly different from some hypothisised value $\Beta_{nH}$.

Normally on of the coefficients of correlation $\Beta_n | n> 0$

Our null hypothesis is generally that $|Beta_n$ is zero

This may be a one or a two tailed test. I.e is the parameter different from zero vs is the parameter greater/less than zero.

The degrees of freedom is normally the number of samples minus the number of dependent and independent variables.

Calculating the t score

$$ t = \frac{\hat{\Beta_n} - \hat{\Beta_{nH}}}{se{\hat{Beta_n}}}$$


Calculate the rejection region using the t distribution.

### __ st: Student t distribution __

The standard deviation of a parameter is $\frac{s}{n}$

## Testing the significance of the whole regression

Null hypothesis $\left\{\hat{\Beta_n}\right} \forall \in \mathbb{Z} \neq 0$ or $R^{2} = 0$

Alternative hypothesis


$(\hat{B_n} \neq 0 \forall n \in \mathbb{Z})$ or $R^{2} \neq 0$

This is done by a technique called anova (analysis of variance)

### ANOVA


