---
title: "Multiple regression analysis"
---

### Multi variable regression

### 3 variable

### None stochastic form

$$E(Y_i) = B_1 + B_2x_2 + B_3 X_3$$

### Stochastic form

$$Y_i = E(Y_i) \mu_i= B_1 + B_2x_2 + B_3 X_3 + \mu_i$$

Where Y is dependent and $x_2$ and $x_3$ are explanatory veriables
$\mu$ is a stochastic disturbance term

Systematic or deterministic component: $B_1 + B_2 X_2 +B_3 X_3$


$$B_j = \frac{\partial y}{\partial x_j}$$

$B_2$ measures the change in the mean value of Y, E(Y) per unit change in X_2 holding the value of X_3

Similar interpretation for $B_3$

### Example

$$E(Y_t) = 15 + 1.2X_2 + 0.8x_3$$

Let X_3 be held constant at the value og 10 this yeils

$$= 23 - 1.2 X_2$$

Similarly, let $X_2$ be held at a constant value of 5.

The slope coefficient  of B3 = 0.8 indicates that the mean value of Y increases by 0.8 units per unit increase in X_3 when X_2 is held constant at  


### Partial regression coefficients

The estimators of b1 and b2. ($\hat{b_1}, \hat{b_2}$) are called the partial regression coefficients.


### Minimizing

We must solve for the minimum $\sum(y_i - \hat{y_i})$

We do the by solving for when the partial derivatives of y with respect to the bs is zero.

This give the results.

$$\hat{b_1} = \frac{\left(\sum x_1 y\right)\left(\sum x_2^{2}\right) -\left(\sum x_2 y\right)\left(x_2y\right)\left(x_1x_2\right)}{\left(\sum x_1^{2}\right){\left(\sum x_2^{2}\right)-\left(\sum x_1x_2\right)^{2}}}$$

b2 is similair

$$b_0 = \bar{y} - \hat{b_1}\bar{y}-\hat{b_2}\bar{y}$$


To solve for the minimum

### Testing the significant of parameter estimators

We need to find the probability that the parameter is different from zero. As we are using a sample this will be a t score. 

To estimate the variance of the estimates of the parameters. Given by

$$s_{\hat{b_1}}^{2}= \frac{\sum e_i^{2}}{n-k}\frac{\sum x_2^{2}}{\sum x_1^{2} \sum x_2^{2}-\left(x_1x_2\right)^{2}}$$

And similairly for s2

### The coefficient of multiple determination

The coefficient of multiple determination $R^{2}$ is definied as the ropertion og the total varioation in Y explained by the mulitple regersion of Y on $X_1$ and $X_2$ and it con be calculated as 

$$R^{2} = \frac{RSS}{TSS} = 1 - \frac{\sum{e_i^{2}}{\sum y^{2}_i}} =  \frac{\hat{b_1}\sum y x_1 + \hat{b_2}\sum  x_2}{\sum y^{2}}$$

where

$TSS = \sum{y_i}^{2}$

$RSS = \sum \hat{y_i}^{2}$

However the inclusion of any additional explanitiory veriables is likely to increase this value weather or not it is relevent.

So instead we use an adjusted value

$$\bar{R}^{2} = 1 - ( 1 - R^{2} )\frac{n-1}{n-k}$$

### Coefficient of partial regression

Two variables may give a false positive relation because both a related to a third variable.

We measure the partial coefficient of correlation between two variables by holding all other variables constant.
### Test for overall significants of the regression

The overall significants of he reggesion can be tested with the ratio of the explained to the unexplained varience. This folows an F distrabution. with k-1 and n-k degrees of freedom where
Us estimators

### Assumptions

Some assumption are properties which can be derived in the two dimensional model 

  #. Linearity in parameters and it is specified correctly.
  #. Explanatory variables are uncorrelated with the zero term.
  #. If the assumption holds it implies that the regresses are exogenous
  #. If not then those regressors that.
  #. The error term has a mean value of zero.
  #. Homoscedasticity
    #. The variance of $\mu$ is constant, that is, var(u_i)=\sigma_2$
  #. No autocorrelation between terms.
  #. No multicollinearity between explanitory veriables
    #. There is no exact linear relationship between the two or more variables.
    #. if multicollineaity exists we cannot isolate the individual effects of the two related veriables on Y.
  #. The error term follows or is close to the normal distrabution. (This is nessesary only for hypothisis testing but not for regression)


To find OLS Estimator use the sample regession model

OLS 

We now have three first order conditions.

Variance

Page 194 in Gujarati and Porter:
    The varience will be a function of the population varience and the sample size and the sample size and the veriabilty in the x range.





# Appendix

