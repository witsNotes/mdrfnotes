---
title : "Integrated Notes"
---

# Imports

import econometrics as econM

import Distrabutions/chiSquaredDistrabution as chi

import Distrabutions/F as f

import Distrabutions/standardNormal as norm

# Integrated notes

## __ econM: Econometrics __

## __chi: Chi squared distribution__

## __norm: Normal distribution__

##__f: F distribution__

