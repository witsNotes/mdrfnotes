---
title: "Hypothesis Testing"
---

# Imports

import samplingDistroOfMean as sd

import sampling as sampling

# Hypothesis testing

## Procedure

 #. State the null hypothesis $H_{0}$ and the alternative hypothesis $H_a$

## __Null and alternative hypothesis__

 2. Calculate the value of the test statistic
 #. Draw a picture of what $H_a$ look like, and find the critical value of the rejection region.
 #. Decide on weather or not to reject the null hypothisis
 #. State the conclusion

## **Null and alternative hypothesis**

$H_0$ is our null hypothesis

This should contain information about weather the test is one or two tailed and the level of significance of the test.

$H_1$ is our alternative hypothesis.

Our null hypothesis predicts some quality of the population(Mean values in our case). This will translate into some quality about the distribution of sample means. 

## __sd: Sampling distribution of the mean __

We know this is approximately normally distrabuted becouse of the central limit theorem.

## __ sd: Central limit theorem __

We use our level of significance and weather we have a one or two tailed test and find our rejection region which is normally a good idea to draw out.

If we know the standard deviation of the whole population we use the normal distribution and find the z scores at the limit of the observation. We than compare this to the z score of the sample mean from the hypothisied mean over the standard mean error.

If we do not know the standard error (because we do not know the standard deviation of the population) we must estimate it

## __ sampling: Estimating standard error __

We have to compare this two T scores from a T distribution with the relevant number of degrees of freedom. Sample size -1.

## Types of error

### Type 1 error

The error of rejecting a hypothesis which is true

### Type 2 error

The error of accepting a false hypothisis
