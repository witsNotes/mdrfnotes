---
title: "Sampling distribution of the mean"
---

# Sampling distribution of the mean

## Definition
This is the probability distribution of means for all possible random samples of a given size from some population.

## Mean of sampling distributions

$$\mu_\bar{x} = \mu$$

## Standard error of the mean

The standard error of the mean is the standard deviation of the sample itself.

It is given by.

$$\sigma_{\bar{x}} = \frac{\sigma}{\sqrt{n}}$$


# Central limit theorem

The central limit theorem states that as n increases the sampling distribution of the mean
approaches a normal distribution.

##__ Sampling distribution of the mean; Definition __

