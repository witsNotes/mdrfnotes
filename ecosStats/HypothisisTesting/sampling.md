---
title: "Sampling"
---

# Estimating standard deviation

# s

Given a sample from a population, a good estimate of the standard deviation of the population is

$$s=\sqrt{\frac{\sum \left(X-\bar{X} \right)^2}{n-1} = \sqrt{\frac{n\sum x^2 - \left( \sum x\right)^2}{n(n-1)}$$

# Estimating standard error


An estimate of the standard error of a population from a sample is given by.

$$s_{\bar{x}} = \frac{s}{\sqrt{n}}$$


