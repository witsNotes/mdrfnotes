---
title: "Hypothesis Testing"
---

### Imports

import samplingDistroOfMean as sd

import sampling as sampling

### Hypothesis testing

### Procedure

 #. State the null hypothesis $H_{0}$ and the alternative hypothesis $H_a$

>## Null and alternative hypothesis
>
>$H_0$ is our null hypothesis
>
>This should contain information about weather the test is one or two tailed and the level of significance of the test.
>
>$H_1$ is our alternative hypothesis.
>
>Our null hypothesis predicts some quality of the population(Mean values in our case). This will translate into some quality about the distribution of sample means. 
>
> ># Sampling distribution of the mean
>>
> >## Definition
>>This is the probability distribution of means for all possible random samples of a given size from some population.
>>
> >## Mean of sampling distributions
>>
>>$$\mu_\bar{x} = \mu$$
>>
> >## Standard error of the mean
>>
>>The standard error of the mean is the standard deviation of the sample itself.
>>
>>It is given by.
>>
>>$$\sigma_{\bar{x}} = \frac{\sigma}{\sqrt{n}}$$
>>
>>
>
>We know this is approximately normally distrabuted becouse of the central limit theorem.
>
> ># Central limit theorem
>>
>>The central limit theorem states that as n increases the sampling distribution of the mean
>>approaches a normal distribution.
>>
> > >## Definition
>>>This is the probability distribution of means for all possible random samples of a given size from some population.
>>>
>>
>
>We use our level of significance and weather we have a one or two tailed test and find our rejection region which is normally a good idea to draw out.
>
>If we know the standard deviation of the whole population we use the normal distribution and find the z scores at the limit of the observation. We than compare this to the z score of the sample mean from the hypothisied mean over the standard mean error.
>
>If we do not know the standard error (because we do not know the standard deviation of the population) we must estimate it
>
> ># Estimating standard error
>>
>>
>>An estimate of the standard error of a population from a sample is given by.
>>
>>$$s_{\bar{x}} = \frac{s}{\sqrt{n}}$$
>>
>>
>
>We have to compare this two T scores from a T distribution with the relevant number of degrees of freedom. Sample size -1.

 #. Calculate the value of the test statistic
 #. Draw a picture of what $H_a$ look like, and find the critical value of the rejection region.
 #. Decide on weather or not to reject the null hypothisis
 #. State the conclusion


# Appendix

