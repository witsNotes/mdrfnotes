---
title: "Econometrics"
---

### Imports

import Regression/regression as reg
import HypothisisTesting/hTesting as ht

### Econometrics

### Methodology of econometrics

### Formulate an economic model
### Data Collection
  
### Specify model mathematically

$$y = \beta_0 + \beta_1 x_1 \dots$$
    
### Specify econometric model.

$$y = \beta_0 + \beta_1 x_1 \dots + \epsilon$$

### Estimation of model

Using regression analysis
     
># Regression analysis
>
>## Assumptions
>
>Since the points are unlikely to fall precisely on the line the exact linear equation must be modified to include a random disturbance or stochastic term $\mu_i$
>
>The term is assumed to be
>
>* Normally distributed
>* Have an expected value of zero
>* Have a constant variance 
>* That the error terms are uncorrelated with each other
>* That the explanatory variable assumes fixed values in repeated sampling
>
>### Linearity in parameters
>
>The two variable linear model is used for testing hypotheses about the relationship between a dependent variable Y and an independent to explanatory variable X and for prediction.
>
>Simple regression analysis involves plotting a scatter diagram of x and y values and seeing if there is an approximate linear relationship between the two.
>
>$Y_i = b_0 + b_1X_1$
>
>## Warnings
>
>Not x is fixed by assumption not by default.
>
>## Specific models
>
> ># Simple regression
>>
>>Here we model the relationship between two variables using a straight line
>>
>>We calculate a line of best fit to find the values of our parameters. 
>>
>>The mathematical model for our data should be in the form
>>
> >## Mathematical model
>>
>>$$y = \beta_0 + \beta_1 x$$
>>
>>where $\frac{\partial{y}}{\partial{x}} = \beta_0$
>>
> >## Stochastic model
>>
>>$$y = \beta_0 + \beta_1 x + \epsilon$$
>>
> > ># Line of best fit
>>>
>>>If we only have a sample. We use the OLS to find the line of best fit for the sample.
>>>
> > > ># OLS estimator
>>>>
>>>>
> > > >## OLS
>>>>
>>>>
>>>>The OLS is an estimator. Is a way of estimating parameters of a statistical model by finding values of the parameters which minimize the square of the difference of the observed data points from equivalent data points predated by the model.
>>>>The OLS is the ordinary(unweighted least squares this is considered a desirable estimator.
>>>>
>>>>The estimator is unbiased
>>>>
>>>>As the number of samples increase the estimator consistently has a better chance of of being closer to the population mean.
>>>>
>>>>Efficiency is how fast the distribution of the sample converges to yield the same value for parameters as the population as the sample size increases
>>>>
> > > >### BLUE
>>>>
>>>>It can be shown under guass-merkov conditions that the least of of sqaures yields a  blue (best(most efficient) linear unbiased estimator).
>>>>
> > > >### Derivation
>>>>
>>>>The derivation of the ols estimator is usually done by using first order condition  $\frac{\partial{\epsilon}}{\partial{x_i}} = 0$
>>>>And solving the simultaneous equations given for the all the parameters.
>>>>
>>>>
>>>
> > >## Derivation
>>>
>>>We want to find $\beta_1$ and $\beta_2$ such that we minimize the sum of squares
>>>
> > > >## Minimizing
>>>>
> > > >### Result
>>>>
>>>>For estimator taking the form
>>>>$$\hat{y} = \beta_1 + \beta_2 x$$
>>>>
>>>>We must find values of the parameters which minimize.
>>>>
>>>>$$S = \sum(y_i - \hat{y})^2$$
>>>>
>>>>Which are given by
>>>>
>>>>$$\beta_2 = \frac{\sum(x-x_i)(y-y_i)}{\sum(x-x_i)}$$
>>>>
>>>>And
>>>>
>>>>$$\beta_1 = \bar{y} - \beta{2}\bar{x}$$
>>>>
> > > >### Proof
>>>>
>>>>We do this by finding the stationary points with respect to our parameters($\beta_1, \beta_2$).
>>>>
> > > > >#### FOC i
>>>>>
> > > > >##### Result
>>>>>
>>>>>$$\sum (y_i -\beta_1 - \beta_2 x_i) = 0$$
>>>>>
> > > > >##### Proof
>>>>>$$\frac{d}{d\hat{\beta_1}}S = 0$$
>>>>>We then substitute $\hat{y}$ with $\beta_1 + x\beta_2$ in and do the differentiation
>>>>>$$-2 \sum (y_i -\beta_1 - \beta_2 x_i) = 0$$
>>>>>Dividing out $-2$
>>>>>$$\sum (y_i -\beta_1 - \beta_2 x_i) = 0$$
>>>>>
>>>>
>>>>Gives us
>>>>
>>>>i) $\sum (y_i -\beta_1 - \beta_2 x_i) = 0$
>>>>
>>>>
> > > > >#### FOC ii
>>>>>
> > > > >##### Result
>>>>>
>>>>>$$=\sum x_i (y_i - \beta_1 - \beta_2 x_i)=0$$
>>>>>
> > > > >##### Proof
>>>>>
>>>>>$$= \frac{d}{d\hat{\beta_2}}S$$
>>>>>We then substitute $\hat{y}$ with $\beta_1 + x\beta_2$ in and do the differentiation
>>>>>$$= -2 \sum x_i (y_i - \beta_1 - \beta_2 x_i)=0$$
>>>>>$$=\sum x_i (y_i - \beta_1 - \beta_2 x_i)=0$$
>>>>>
>>>>
>>>>Gives us 
>>>>
>>>>ii)$\sum (y_i -\beta_1 - \beta_2 x_i) = 0$
>>>>
>>>>
>>>>
> > > > >#### Solving i for $\bar{y}$
>>>>>
> > > > >##### Result
>>>>>
>>>>>$$\bar{y} = \beta_1 + \beta_2 \bar{x}$$
>>>>>
> > > > >##### Proof
>>>>>
>>>>>
>>>>>$$\sum (y_i -\beta_1 - \beta_2 x_i) = 0$$
>>>>>
>>>>>$$\implies \sum (y_i) = \sum\beta_1 + \sum \beta_2 x_i$$
>>>>>
> > > > > >### Lemma 1
>>>>>>
>>>>>>$$\sum x_i = n \bar{x}$$
>>>>>>
>>>>>
>>>>>$$\implies n \bar{y} = n \beta_1 + n\beta_2 \bar{x}$$
>>>>>
>>>>>$$\implies \bar{y} =  \beta_1 + \beta_2 \bar{x}$$
>>>>>
>>>>>
>>>>
>>>>Gives use 
>>>>
>>>>iii)$\bar{y} = \beta_1 + \beta_2 \bar{x}$
>>>>and
>>>>iv)$\beta_1 =\bar{y}-+ \beta_2 \bar{x}$
>>>>
> > > > >#### Solving ii and iv for $\sum x_i y_i$
>>>>>
> > > > >##### Result
>>>>>
>>>>>$$\sum x_i y_i = n \bar{x} \bar{y} -\beta_2 n \bar{x}^2 + \beta_2 {x_i}^2$$
>>>>>
> > > > >##### Proof
>>>>>
>>>>>
>>>>>$$\sum x_i y_i = \beta_1 \sum x_i + \beta_2 {x_i}^2$$
>>>>>
>>>>>Using Lemma 1
>>>>>
>>>>>$$\sum x_i y_i = \beta_1 n \bar{x} + \beta_2 {x_i}^2$$
>>>>>
>>>>>Using iv
>>>>>
>>>>>$$\sum x_i y_i = (\bar{y} - \beta_2\bar{x})n \bar{x} + \beta_2 {x_i}^2$$
>>>>>
>>>>>Giving
>>>>>$$= n \bar{x} \bar{y} -\beta_2 n \bar{x}^2 + \beta_2 {x_i}^2$$
>>>>>
>>>>
>>>>Gives us 
>>>>
>>>>v)$\sum x_i y_i = n \bar{x} \bar{y} -\beta_2 n \bar{x}^2 + \beta_2 {x_i}^2$
>>>>
> > > > >#### Solving v for $\beta_2$
>>>>>
>>>>>
> > > > >##### Result
>>>>>
>>>>>$$\beta_2 = \frac{\sum(x-x_i)(y-y_i)}{\sum (x-x_i)}$$
>>>>>
> > > > >##### Proof
>>>>>
>>>>>Solving v for $\beta_2$ gives
>>>>>
>>>>>$$\beta_2 = \frac{\sum (x_i y_i) - n \bar{x} \bar{y}}{\sum x - n \bar{x}}$$
>>>>>
>>>>>Applying lemma 2
>>>>>
>>>>>$$\beta_2 = \frac{\sum (x_i y_i ) - \bar{x} \bar{y}}{\sum x - n \bar{x}}$$
>>>>>
>>>>>$$\beta_2 = \frac{\sum (x_i y_i) - n \bar{x} \bar{y}}{\sum x_i^2 - \bar{x} x_i}$$
>>>>>
>>>>
>>>>Gives us 
>>>>
>>>>vi) $\beta_2 = \frac{\sum(x-x_i)(y-y_i)}{\sum (x-x_i)}$
>>>>
>>>>And finally solving for $\beta_1$ gives us
>>>>
>>>>vii) $\beta_1 = \bar{y} - \beta_2 \bar{x}$
>>>>
>>>
>>
>
> ># Classical regression model
>>
>>Same as OLS except error terms are not normally distributed
>>
> >## Classical normal regression model
>>
>>Error terms are assumed to be noramally distrabuted.
>>
>>$y = beta_1 + \beta_2x_ + \epsilon_i$
>>
>>If ur exected error term is equal then the expected x ust be equal to the expected y.
>>
>
> ># Multi variable regression
>>
> >## 3 variable
>>
> >### None stochastic form
>>
>>$$E(Y_i) = B_1 + B_2x_2 + B_3 X_3$$
>>
> >### Stochastic form
>>
>>$$Y_i = E(Y_i) \mu_i= B_1 + B_2x_2 + B_3 X_3 + \mu_i$$
>>
>>Where Y is dependent and $x_2$ and $x_3$ are explanatory veriables
>>$\mu$ is a stochastic disturbance term
>>
>>Systematic or deterministic component: $B_1 + B_2 X_2 +B_3 X_3$
>>
>>
>>$$B_j = \frac{\partial y}{\partial x_j}$$
>>
>>$B_2$ measures the change in the mean value of Y, E(Y) per unit change in X_2 holding the value of X_3
>>
>>Similar interpretation for $B_3$
>>
> >#### Example
>>
>>$$E(Y_t) = 15 + 1.2X_2 + 0.8x_3$$
>>
>>Let X_3 be held constant at the value og 10 this yeils
>>
>>$$= 23 - 1.2 X_2$$
>>
>>Similarly, let $X_2$ be held at a constant value of 5.
>>
>>The slope coefficient  of B3 = 0.8 indicates that the mean value of Y increases by 0.8 units per unit increase in X_3 when X_2 is held constant at  
>>
>>
> >### Partial regression coefficients
>>
>>The estimators of b1 and b2. ($\hat{b_1}, \hat{b_2}$) are called the partial regression coefficients.
>>
>>
> >### Minimizing
>>
>>We must solve for the minimum $\sum(y_i - \hat{y_i})$
>>
>>We do the by solving for when the partial derivatives of y with respect to the bs is zero.
>>
>>This give the results.
>>
>>$$\hat{b_1} = \frac{\left(\sum x_1 y\right)\left(\sum x_2^{2}\right) -\left(\sum x_2 y\right)\left(x_2y\right)\left(x_1x_2\right)}{\left(\sum x_1^{2}\right){\left(\sum x_2^{2}\right)-\left(\sum x_1x_2\right)^{2}}}$$
>>
>>b2 is similair
>>
>>$$b_0 = \bar{y} - \hat{b_1}\bar{y}-\hat{b_2}\bar{y}$$
>>
>>
>>To solve for the minimum
>>
> >### Testing the significant of parameter estimators
>>
>>We need to find the probability that the parameter is different from zero. As we are using a sample this will be a t score. 
>>
>>To estimate the variance of the estimates of the parameters. Given by
>>
>>$$s_{\hat{b_1}}^{2}= \frac{\sum e_i^{2}}{n-k}\frac{\sum x_2^{2}}{\sum x_1^{2} \sum x_2^{2}-\left(x_1x_2\right)^{2}}$$
>>
>>And similairly for s2
>>
> >## The coefficient of multiple determination
>>
>>The coefficient of multiple determination $R^{2}$ is definied as the ropertion og the total varioation in Y explained by the mulitple regersion of Y on $X_1$ and $X_2$ and it con be calculated as 
>>
>>$$R^{2} = \frac{RSS}{TSS} = 1 - \frac{\sum{e_i^{2}}{\sum y^{2}_i}} =  \frac{\hat{b_1}\sum y x_1 + \hat{b_2}\sum  x_2}{\sum y^{2}}$$
>>
>>where
>>
>>$TSS = \sum{y_i}^{2}$
>>
>>$RSS = \sum \hat{y_i}^{2}$
>>
>>However the inclusion of any additional explanitiory veriables is likely to increase this value weather or not it is relevent.
>>
>>So instead we use an adjusted value
>>
>>$$\bar{R}^{2} = 1 - ( 1 - R^{2} )\frac{n-1}{n-k}$$
>>
> >## Coefficient of partial regression
>>
>>Two variables may give a false positive relation because both a related to a third variable.
>>
>>We measure the partial coefficient of correlation between two variables by holding all other variables constant.
> >## Test for overall significants of the regression
>>
>>The overall significants of he reggesion can be tested with the ratio of the explained to the unexplained varience. This folows an F distrabution. with k-1 and n-k degrees of freedom where
>>Us estimators
>>
> >## Assumptions
>>
>>Some assumption are properties which can be derived in the two dimensional model 
>>
>>  #. Linearity in parameters and it is specified correctly.
>>  #. Explanatory variables are uncorrelated with the zero term.
>>  #. If the assumption holds it implies that the regresses are exogenous
>>  #. If not then those regressors that.
>>  #. The error term has a mean value of zero.
>>  #. Homoscedasticity
>>    #. The variance of $\mu$ is constant, that is, var(u_i)=\sigma_2$
>>  #. No autocorrelation between terms.
>>  #. No multicollinearity between explanitory veriables
>>    #. There is no exact linear relationship between the two or more variables.
>>    #. if multicollineaity exists we cannot isolate the individual effects of the two related veriables on Y.
>>  #. The error term follows or is close to the normal distrabution. (This is nessesary only for hypothisis testing but not for regression)
>>
>>
>>To find OLS Estimator use the sample regession model
>>
>>OLS 
>>
>>We now have three first order conditions.
>>
>>Variance
>>
>>Page 194 in Gujarati and Porter:
>>    The varience will be a function of the population varience and the sample size and the sample size and the veriabilty in the x range.
>>
>>
>>
>>
>

### Is the model adequate if note go to step 1 if yes continue
### Test any hypothesis suggested by the model

># Hypothesis testing
>
>## Procedure
>
> #. State the null hypothesis $H_{0}$ and the alternative hypothesis $H_a$
>
> >## Null and alternative hypothesis
>>
>>$H_0$ is our null hypothesis
>>
>>This should contain information about weather the test is one or two tailed and the level of significance of the test.
>>
>>$H_1$ is our alternative hypothesis.
>>
>>Our null hypothesis predicts some quality of the population(Mean values in our case). This will translate into some quality about the distribution of sample means. 
>>
> > ># Sampling distribution of the mean
>>>
> > >## Definition
>>>This is the probability distribution of means for all possible random samples of a given size from some population.
>>>
> > >## Mean of sampling distributions
>>>
>>>$$\mu_\bar{x} = \mu$$
>>>
> > >## Standard error of the mean
>>>
>>>The standard error of the mean is the standard deviation of the sample itself.
>>>
>>>It is given by.
>>>
>>>$$\sigma_{\bar{x}} = \frac{\sigma}{\sqrt{n}}$$
>>>
>>>
>>
>>We know this is approximately normally distrabuted becouse of the central limit theorem.
>>
> > ># Central limit theorem
>>>
>>>The central limit theorem states that as n increases the sampling distribution of the mean
>>>approaches a normal distribution.
>>>
> > > >## Definition
>>>>This is the probability distribution of means for all possible random samples of a given size from some population.
>>>>
>>>
>>
>>We use our level of significance and weather we have a one or two tailed test and find our rejection region which is normally a good idea to draw out.
>>
>>If we know the standard deviation of the whole population we use the normal distribution and find the z scores at the limit of the observation. We than compare this to the z score of the sample mean from the hypothisied mean over the standard mean error.
>>
>>If we do not know the standard error (because we do not know the standard deviation of the population) we must estimate it
>>
> > ># Estimating standard error
>>>
>>>
>>>An estimate of the standard error of a population from a sample is given by.
>>>
>>>$$s_{\bar{x}} = \frac{s}{\sqrt{n}}$$
>>>
>>>
>>
>>We have to compare this two T scores from a T distribution with the relevant number of degrees of freedom. Sample size -1.
>
> #. Calculate the value of the test statistic
> #. Draw a picture of what $H_a$ look like, and find the critical value of the rejection region.
> #. Decide on weather or not to reject the null hypothisis
> #. State the conclusion
>

### Use the model for predictions
### Data types

* Cross sectional
* Pooled
  * Two or more cross sectional data collections are used.:

* Time series
* Panel/Logitudanal data

# Appendix

