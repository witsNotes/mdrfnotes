# Coefficient

Measures the degree of linear association between two variables.
Lies between -1 and +1

$$r = \frac{\sum(X_i - \bar{X})\sum{Y_i - \bar{Y})}}{\sqrt{\sum(X_i - \bar{X_i})^2(Y_i - \bar{Y})^2}}$$

$$= \frac{\sum X_i y_i}{\sqrt \sum x_i^2 \sum y_i^2$$

Or simple $\sqrt{r^2}$

r can be very small if the two variables are related but not in a linear manner.

