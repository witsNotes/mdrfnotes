---
title: "Kurtosis"
---

# Kurtosis

For a population

$$K=\frac{\sum_{}^{}f(X-\mu)^{4}}{\sigma^{4}}$$


For a sample
$$K=\frac{\sum_{}^{}f(X-\bar{X})^{4}}{s^{4}}$$

For ungrouped data there is no f term

K=3: Mesokuratic
K>3 Platykurtic
K<3 Leptokurtic

Excel computes excess kurtosis.


