---
title: " Restricted least square"
author : "Jezri Krinsky "
startTime: "27 August 2018"
---

# Restricted least square

This is a way to measure the significant

$$F = \frac{ (R_{wr}^2 - R_{r}^2)/m }{1-(R_wr- R_r)/n-m-1}$$ 

Test if this f score at a appropriate level of significance.



# Measure of the significance of extra veribiables

T values and adjusted R

# Models with nonlinear variables

## Log linear model

$$Y_i = AX_i^{\beta_2} e^{\mu_i}$$ 

Take the log of both side

$$log{Y_i} = log(AX_i)\beta_2 e^{\mu_i}$$ 

$$= ln(A) + \beta_2 ln(X_i) + \mu_i$$ 

### Elasticity example

Change in the natural log

$\Beta_2$ of the log will be the elasticity of x with respect to Y or the average elasticity of the sample.


## Semi log models

Models where either the dependent veribal or the independent veriables are in log forms

log-lin is when the dependent veriable is logarithim

log-lin is oppisite

### Compound interest

$$FV = P (1+r)^t$$ 

$$log (FV) = log(P) + t log(1+r)$$ Remember that t is the variables not (1+r)

this Gives

$$\frac{ \partial{log(y)} }{ \partial{t}}$$ Then sub out 


## Reciprocal models

$$y_i = \beta_1 + \beta_2 \left(\frac{ 1 }{ x_i }\right)  + \mu_i$$ 

This beta no longer shows a positive relationship between y and x

### Philips curve

Negativity can be the change in wages not wages.

## Polynomial regression model

No perfect linear relationship model but polynomial is fine.

## Regression through the origin

There are occasions when the regresssion model assumes the following form:
  
  $$Y_i = \Beta_2 X_i +u$$

The intercept is zero hence the name regression through the origin

One may use zero intercept model if the is strong theoretical reason for it as in Okun's law

### Calculations

$\Beta_2 = \frac{ \sum_{  }^{  }{ XY } }{ \sum{X^2 } }$ 

Solve for $\mu_i$ and differntaite with respect to 0 then this gives the solution.

The sum of the risiduals need not be zero?

ie the expected value of error term may not be zero, so we now may have bais. So this should not be done unless strongly suggested by theory. This can be removed by hypothisis testing.







