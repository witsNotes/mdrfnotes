---
title: "Econometrics"
---

# Imports

import Regression/regression as reg
import Regression/regressionHypothisisTesting as regHT
import HypothisisTesting/hTesting as ht

# Econometrics

## Methodology of econometrics

### Formulate an economic model

### Data Collection
  
### Specify model mathematically

$$y = \beta_0 + \beta_1 x_1 \dots$$
    
### Specify econometric model.

$$y = \beta_0 + \beta_1 x_1 \dots + \epsilon$$

### Estimation of model

Using regression analysis
     
#### __reg: Regression analysis__

### Is the model adequate if note go to step 1 if yes continue

### Test any hypothesis suggested by the model

#### __ ht:Hypothesis testing __

#### __regHT : Regression analysis hypothesis testing __

### Use the model for predictions

## Data types

* Cross sectional
* Pooled
  * Two or more cross sectional data collections are used.:

* Time series
* Panel/Logitudanal data
