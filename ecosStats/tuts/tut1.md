# Population regression function.

The function or an equation describing how the population mean veriable is realted to one or more explanatory veriables.

This is for the population not the sample.

# Linear regression model

A model with linear parameters.

Linear in parameters not in variables

# Regression coefficient of a parameter.

The beta coefficient.

# Stochastic error term

A small error term with an error of zero

$\mu i$

# Risidual error term

$e_i$ 

This is the error term of the sample.

# The explanitory veribiable is the course and and effect.

# Question 3

Fertilizer (lb) | Yeild (lb)
4               | 3
5               | 5.5
10              | 6.5
12              | 9.0
