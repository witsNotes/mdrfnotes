---
title: "tut 4"
---

# Tut 4


Given a budget line and a utility curve.


Show how consumer surplus increases as the price of one good falls.

P(x) falls so consumption of x increases. So the budget line swivels outwards.

Becouse of the we can move to a higher indifference curve. IFC 2.


Moving to a higher indifferece curve means moving to a higher level of satisfaction.

# Substitution effect and income effect

Draw a line parallell to the new budget line and tangent to the old indifference curve. The difference between e to A represents the substitution effect.

The income effect is the difference between a and b.

# Question 2

The individual collects snakes and lizards draw the indifference curve and budget line as well as deriving the optimal bundle for the consumner

$$P_x Q_x + P_y Q_y = M$$

$$10S + 20 L = 60$$

This touches the second indifference curve.

We find that the BL is tangent to the 2nd IC So 4s and 1l

## Question 2 b

Suppose that the price of snakes increases to 20 and the price of a lizard does not change.

So 1s and 2l


## Question 3

Are the snakes and lizards substitutes or compliments.

Because the price of lizards going up leads to a rise in the consumption of snakes the 2 goods are supplements.

## When is the consumer better of

He was better off before the prive hike becouse he could consume on a higher IC

## D

For his supperior ability at work the consumer is given a raise of 60 r. How many lizards and how many snakes does the consumer now consume and are both goods normal.

When income increases the consumption of both goods increases so they are both normal goods becouse he buys more of both as his income increases.

## E

Suppose the farmer is producing at point C (where we at disequilibruim becouse the marginal physica product of the labour is not equal to the price rations)

So less output is obtained by adding one additional unit of labour.

### ii

Explain how with no change in expenditure output can be increased. 

To reach equilibrium the producer will reallocate money towardsd k and away form L untile $\frac{Mpp_l}{Mpp_k} = \frac{w}{r}$

## F

Wage rate decreases and the rental rate increases so the effect is ambiguous. As its effect on the ratio is ambigious

It depends in the relative sizes of the two changes.

# 3

A firm has a budget of 30000 labour cost 10 per houur and kapital cost 100 rand per hour.

Draw their budget line. Calculate the magnitude of the slope of the isocost.

# c

Suppose that the price of labour rises to 15 rand/hour and capital rises by 120 per hour

Calculate the new ratio

### d

What happens to the equilibrium level of income becouse of the change in the prices of these factors.

So they produce at a lower isocost.

Or produce on a lower isoquant.



### e

What will happen to the ratio.

The ratio $\frac{w}{r}$ shifts towards 

So this industry has become more capital intensive. We will be able to check this by the slope of the new isocurve.

# 4

Suppose 

Alternitvely suppose that all production in the y industry results in.


Assume constant returns to scale.

Draw the production possibilty frountier

Production at the midpoint of the diagonal. Half the resources go to x initially and half go to y initially.

Consider different scenarios.

There are lots of different possible scinarios.

# Does production possibility points represent maximum productivity

Yes. These points are determines from the production efficiency locus.

Any point on the locus is Pareto efficient.

So by definition we are now producing at maximum productivity.


# Question 5

Producing less of the k intensive good and more of the l in good. Point V is on a lower Y isoquant. ie producing less Y

higher x __ __ more x

b) a)

pt O_x so industries x is expanding so more labour must be employed so the demand for labour is greater than the demand for capital.

c) w and r rise but the increase in wages is greater than the increase in rent so the ratio of wages to rent increases.

With output expanding and the ratio rising. L becomes relitivly more expensive than K. So some substitution of L with K is needed (but Still L intensive)

d) The absolute value of the slope should also be larger, becouse now a higher isoquant is tangent to the steeper isocost.
