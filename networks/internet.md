---
title : "The internet"
---

# The internet

## Components

### Hosts or end systems

These are the end user systems such as personal computers or smart devices.

These access the internet through the network Service Providers

### Communication links

Physical links between computers such as wires fibre ext.


### Packet switches

A packet switcher is a device wich takes a packet arriving on one of its ingoing communication links and forwards it to one of its outgoing communication links. 

Two common types of packet switches are

#### Routers

Typically used in network core.

#### Link layer switches

Typically used in access networks.

### Route

A route or path through a network is a sequence of connected packet sqitches form a begining to an end system.

### ISP

These are themselves a set a packet switchs.
Internet service providers. Provide internet to both content providers and browsers. Through a high verity of means and differing speeds.

#### Low tier

Low tier providers are smaller local providers that links users to high tier providers.

#### High tier

These are highspeed packet switchs connected nationally and international with fibre. They are run indeendetly but all comform to IP protocol.

# Protocols

All components of the internet interact with eachother through protocols. The most important of these are

## TCP

Transmission control protocol  

## Services

Web applications are often called distributed applications since they involve multiple end systems that exchange data with each other. Importantly, internet applications run on end systems. They do not run in the packet switches in the network core. 


This approach to describing the internet is primarily concerned with how distrabuted newtork appications running on verious computers around the network send information to eachother.

### Application programming interface

The API a standard for progams to send information from one end system to another over the internet.



