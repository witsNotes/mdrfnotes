---
title: " Mappings"
author : "Jezri Krinsky "
startTime: "27 August 2018"
---

# Imports	

# Mappings

## Definition

Let A and B be none empty sets

### Mapping

$$\alpha:A \to B$$ 

is a mapping if it a rule that assigns each element of a to exactly one element of b.

ie $\forall a \in A \exists ! b \in B \quad \text{s.t} \quad \alpha(x) = b$ ! is uniqueness

Examples:

  #. $\alpha (x) = x^2$ $\alpha$ is a mapping
  #. $\alpha:\mathbb{R} \to \mathbb{R} : \alpha (x) = \pm \sqrt[]{x}$ is not a mapping becouse
    #. Doubles exist as fomula gives two values for some values such as nine
    #. No answer for other reals
  #. Definition by list $a_1 \to b_1 a_2 \to b_1$

 Codomain

### Range

## Graphs

## ID

## Inverse

## Well defined mappings

Check if $\alpha$ is is well defined

Let $f = \frac{ 1 }{ 2 } = 1$

and $\alpha(x) = \alpha(\frac{ 2 }{ 4 } ) = 2$ 

So the element x in $\mathbb{Q}$ cannot be assigned to a unique element in Z alpha is not well defined.

Example

Is A = S cross S and B = S than alpha S times S -> S or alpha A and B is a binary operation.

## Binary operations

A binary operation $\alpha$ on a set T is a mapping alpha (T -> T -> T) or (T\cross T) -> T

Ie a mapping

IE a mapping that assigns to each ordered pair (a,b) in T \cross T an element \alpha (a,b) of
T

$$\alpha T \times T \to T$$ with alpha(t_1, t_2) = 2

D(\alpha) = t cross T for well defined

Let t_i and t2 and s_1 s_2 be an element of T \cross T

t_1, t_2 = s_1 s_2

let t_1 = s_1 and t_2 = s_2

Thus alpha (t_1, t_2) = alpha(s_1, s_2) since t_1 and t_1 is well defined and alpha is called a projection of t cross t onto t

alpha is a binary operation.

eg) 

# Test of the definition

Let t_{1} = s_{1}

Similarly

(t_1,t_2) = s_{1}, s_{2}

Some specifice examples

$\alpha$ :: Z \cross Z \to Z where $\alpha$ = \frac{}{numerator}

## Multiplication


### Check if multipleication is weldefined

if equiv ac = equive bd


Start from the begining

$$\bar{a} = \bar{b}$$ 
$$\bar{c} = \bar{d}$$
$$
\implies a equiv b(mod n) \wedge c \equiv d (mod n)
\implies a - b = t,n and c - d = t_{2}n, t_{1}, t_{2}
\implies a = t_{1} n+b and c = t_{2} n + d

\implies ac = ( t_{1}n+b )( t_{2}n+1b )$$

### Properties of mappings compositions and bijections###

#### Definitions

1) \alpha:A-> B and \Beta: A -> B

alpha and bet are equal if 

$$D(\alpha) = D(\beta) \wedge \alpha(a) = \beta(b) | \forall a \in A.$$ 

2) $$\alpha A \to B$$ , A \subset B then

$$\alpha is called the indentity mapping if \alpha(a) = a \forall A a' \in A$$

3.

$$\alpha (A \to B)$$  is one to one or injective if $\alpha$ ( a_{1} ) = \alpha(a_2) a_1 = a_2 forall

4. $\alpha$ from a to b is onto or surjective is $\forall b \in B \exists a \in A \quad \text{s.t} \quad b = \alpha(a)$ 
In this case the range $\alpha$ = CoD x

If alpha is well defined 1 to one and onto we say alpha is a bijection

Examples 5.2.2

Example
1) Equility

If alpha: R \to R where alpha x = x^2 + x + 1

And B: R \to R where B(x) = (x-1) (x+2) + 3

Is alpha = beta

i) D(\alpha) =D(\beta) = \mathbb{R}

ii) \alpha (x) = x^2 d 

ii(2) on to one

i)$$alpha \maps \mathbb{N} \to \mathbb{N} defined by \alpha(n) = 2n+1

  i.e \alpha 1-1?
  let n.m \in \mathbb{N} with
  \alpha m = \alpha n
  \implies 2m+1 = 2m+1
  \implies 2m = 2m
  \implies m = n
  \alpha is 1-1$$

ii)

\alpha : \mathbb{R} \to \mathbb{R} \alpha = n^{2} + 1

First property

If we have the mappings 

iii) \alpha : \mathbb{R} \cross R \to R -> R \alpha (a,b) = ab

Alpha is not one to one proove by counter example


