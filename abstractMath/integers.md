---
title : "integers"
---

# Properties

We need several less familiar properties of divisibility and the primes in \mathbb{Z}

## Well ordering Axiom

Every none-empty set of positive integers has a smallest member

### Proof

From basic analysis we know that every none empty set of reals that is bounded bellow has an infimium. Hence every none empty set of positive integers(bounded below by zero) has an infimium in R and hence a smallest member. (is this complete?)

## Division Algorithm

Theorem 2.2.1

Let $n,d \in \mathbb{Z} | d \geq 1$ 

$\exists q, r \in \mathbb{Z}  s.t n = q d +r \cap 0 \leq r < d$

q and r are called the quotient and remainder respectively and are uniquely determined by n and d.


### Proof

Let $X  = \left\{n-td | t \in \mathbb{Z} , n-td \geq 0 )\right\}$

Show X is not empty.

####__Not empty__

By the well ordering axiom x has a smallest element.

if $X$ has a smallest element we let r be the smallest element in X
So $r \geq 0$ and $r = n-qd$ for some $q \in \mathbb{Z}$

So $n = qd + r$

$q \in Z r \geq 0$

####__Show $r < d$__

Lastly the statement of the theorem requires tat q and r are unique so we will now shw unqueness.

####__Uniqueness__
#####**Not empty**

$n \geq 0 \implies n - 0d \in X$ Will always evaluate at greater than 0

And

$n < 0 \implies n - nd = n(1-d) \geq 0 \implies n-nd \in x$


So $X \neq \phi$ and is bounded bellow by zero


#####**Show $r < d$**


Assume $r \geq d \implies 0 \leq r-d = n -qd -d = n - (q+1) d \in X$

But r-d < r and r is the smallest element in X

Contradiction So our assumption must be incorrect

#####**Uniqueness** 

Assume that two such elements exist.

Without loss of generalities can be written that $r \leq r_1$

Thus if we let $n = q_1 d + r_1$ then

$qd+r = q_1 d + r_1$

$r_1 - r = (q - q_1)d$

but $r_1 - r  \leq r_1 < d$

So $r_1 - r$ is a multiple of d. If the number is a multiple of d which is also less than d implies $r_1 -r = 0$ and $q - q_1 = 0$; so $r_1 = r$ and $q_1 = q$ thereby proving uniqueness. 

## Example 2.2.2

Find the quotient and the remainder of n if

 #. n = -17, d = 5

  $n = qd + r$
  $-17 = -4 5 +34$
$\frac{n}{d} = -\frac{17}{5} = -4 + \frac{3}{5}$

### Geometrically.

Mark of the real line in multiples of d. Then n must lie between the multiple $q_d$ and $q+1 d$ for q in Z thus $q_d \leq n \ leq |q+1) d$ Check


## Divisor Definition

Let $n , d \in Z$ If d is not a divisor of n we write $d | \cross n$

Theorem 2.3.1

* Let $n,m,d \in \mathbb{Z}$

    $n|n \forall n \in \mathbb{Z}$

    Proof 

    $n = 1 n$ and $1 \in \mathbb{Z} \implies n | n$
* $d | m and m | n then d | n$

    Proof

  $d|m  \implies m = kd$
  and 
  $m | n \implies n = k_1d$

  Where $k , k_1 \in \mathbb{Z}$

  $\implies n = k_1 (k d)$
  = $kk_1d$  commutativity

  $d|n$ since$ $kk_1 \in \mathbb{Z}$


* if $d|n$ and $n|d$ then $d = \pm n$

  Proof $d|n$ and $n|d$ 
 
  $\imples n = k * d$ and $d = k_1n$ where $k,k_1 \in \mathbb{Z}$

  So $n= kk_1n$


  So $kk_1 = 1$ 

  $k = k_1 = (1,-1)$ since $k k_1\in \mathbb{Z}$.

*  If $d|n$ and $d|m$ then $d|xn + ym$ where $x,y \in \mathbb{Z}$

Proof 

$n = kd \quad  m = k_1d$ 


$xn+ym =kxd + k_1yd = (kx+k_1y)d$


$\implies d | xn + ym$ as $xk + yk_1 \in \mathbb{Z}$Z

Note $xnn + ym$ is called a linear combination of n and m.

#### Example 2.3.2

If $d \geq 1$ and $d | 3k+5$ and $d| 7k+2$ show
$d = 1$ or $d = 29$

$3k +5 = k_1 d \quad  7k +2 = k_2 d$


Eliminate k

$21k + 35 = 7k1 d 7k$ 

$-35 + 3 k_1 d = -6 + 4 kd$

Take common d to one side

$d(7k_1-3k_2) = 29$

$d|29$

$d = 1,29$


## Euclides Alogorithim

Theore 2.5.1

m,n \in \mathbb{Z} , not both zero

d = gcd(m,n) existas and d = xm + yn for some integers x y

Proof 

Let C be the set of all linear combinations of m n  where the cooeficients are integers and the linear combination is greater than or equal to one.

Show that the set is not emptys

m^2 + n^2 $\geq$ 1

So by the well ordering axiom x has a smallest element.

So choose d to be a smallest element.

Let d be the smallest element in x.

Then d \geq 1 and $d = xm + yn , x,y \in mathbb{Z}$

If k is any divisor of m n than it can devide any linear combination of m and n than it is a divsor of d.

We show that d devides We show tha d is a devisor of both m and n hence d is the gcd(m,n)

By the division algorithm 

n = qd + r

We must show that r = d 

by division algorithm $0\leq r < d, q,r,d \in \mathbb{Z}$$

$r= n-qd$
$= n-q(xm + yn)$
$=-qx m + (1-qy)r$
So r \in X

This is a contradiction as we took d to be the smallests element of X.

Therefor the only possiblity is that R is equel to zero and so if r = 0 d devids a

Similarly d devides n so d is the greatest common devisor of n and m.

### Corollary 2.5.2

If m = qn + r, then gcd(m,n) = gcd(n,r)

Proof

Let d = gcd(m,n) k = gcd(n,r)

$K|n$ and $k|r$ so $k|qn+r$ as this is a linear combination  of r and n.

So K devids m. 

Therefor k divides d since d is the gcd of m and n by definition of gcd

Using r = -qn + m we can show that d|k. Conclusion d = \pm k

Therefor d = k

End of the proof.

## Examples

find the greatest common devisor of (78,30) using the greatest common devisor

Eculidea algorithm. 

Let m = 78 and n=30

78 = m = q_1 n + r_1 = 2.30 + 18

30 = n = q_2 + r_2 = 1\time 18 + 12

18 = r_1 = 1 \times 12 + 6

12 = 2 \times 6 + 0

So r_4 = 0 and 6 is our greatest common devisor.

6= 8(30) - 3(78)

gcd(m,n) = gdc(n,r1) ...


Reverser the euclidian algoryth to get gcd as a linear combination of the two numbers

Find gcd(41,12)

41 = 3 times 12 + 5

12 ,5 = 2time 5 + 2

5 = 2.2 + 1

2 = 2.1 + 0

So r_3 is our greatest common devisor.

gcd of (41,12) = 1

therefor 41 and 12 are coprime (greatest common devisor is 1)

1 = x41 + y(12)

1 = 5 - 2.2
= 5-2(12-2.5)
= 5-2(12) + 4.5
= 5 times 5 - 2.12
= 5.(41 - 3.12) - 2.12
= 4.41 -15.12 -2.12
= 4.41-17.12

therefor x = 5 and y = -17

2.6 Relativly prime

Definition

2.6.1 m,n \in \mathbb{Z} and gcd(m,n) =1 then m and n are relitivly prime 

Theorem 2.6.2

Let m and n \in \mathbb{Z}, not both zero than the greatest common devisor of m n are equal to one iff nad only iff x y in z | sm+yn = 1

Proof

if the gcd(m,n) = we can write it as a lilnear combination and the other way around.

alg 1 = xm + yn as required. If there exist x, in Z such that xm +yn =1 and d is the gcd (m,n) . Then xm + yn = 1 and d is the greatest common devisor than d|1

Therefor d=1

# Relatively Prime

Definition.

Relatively prime. if m, n in $\mathbb{Z}$ and gcd(m,n) is 1 then m and n are relitivly prime.

## Theorem 2.2

Let m and n $\in \mathbb{Z}$ not both zero, gcd(m,n) = 1 iff $\exists x,y\in \mathbb{Z} such tht $xm+yn=1$

This is a stronger stament

### Proof

If gcd(m,n) = 1 than by the euclidian algorythm 1 = xm + yn as required.

To prove the backwards direction.

If there $$exists x,y \in \mathbbPZ[ such that xm + yn =1 and gcd(m,n)=d$$

than d|1 and d|m and d|n so d = 1

## Cor 2.4.3 m,a \in \mathbb{Z} and gcd(m,n) and then $\frac{m}{dS}$ and $\frac{n}{d] are relatively prime.

Proof gcd(m,n) = d

implies d = xm + yn

implies 1 = x(\frac{m}{d} + y(\frac{n}{d}) where $\frac{m}{d}, \frac{n}{d} \in \mathbb{Z}$


## Theorem 2.4.4

### Part 1

Let $gcd(m,n) = 1 Then

i if $m|k$ and $n|k$ then $m|k$

#### Proof $m|k, n|k$ and $gcd(m,n) = 1$

impleis $ k = k_1 m , k= k_2 n , 1 = xm+yn$

implies $k= k1 = k (xm + yn)

= kxm + kyn
= k2nxm + k1yn
= mn(k2x+k1y)

implies $mn|k$

ii) If $m|kn$ for some k then $m|k$

Proof gcd(m,n) =1 and $m|kn$

implies xm + yn =1 and kn = k_1m

implies k = k.1 = k(xm + yn)
= kxm + kyn
= kxm + k1my
= m(kx + k1 y)
m|k

eg

Defn : $A_n$ integer p is a prime if

i $p \geq 2$
ii if d|p and d>0 than d can only be one or the prime

# Euclid's lemma

Let p be a prime

If p|mn then p|m or p|n

And secondly if

p|m_1 \dots m_n 
thanfor some i
p|m_n

Proof of i

Let d = gcd(p,m)

By the definition of the gcd

d|p so d=1 or d=p

if d|p than p|m

If d=1 than gcd p,m =1 so p?n by theorem 2.4.4(gcd(m,n) =1 and m|k implies m|k)

Proof ii)

use induction on k.

This proof is left as an exercise.

Thm 2.5.3 Prime factorization theorem

i) Every integer $n \geq 2$ is the product of one or more primes.

The factorization is unique up to the order of factors 

We will not prove this statemen

if n is the product of primes where all.

Def 2.5.4 GCD and GCD and LCM

Let n1 to nr be positive intergers

The greatest common devisor of these integers is donated by gcd(n_1, n_r)

is the positive common deviser that is a multiple if every common deviser.

ii The least common multiple of these integers denoted by lcm(n_1 \dots n_r) is the positive common multiple that is a devisor that is a devisor of every common multiple.

eg) Find the gcd of 4 6 and 10 and the lcm of 4 6 10.

[(2^2) (2,3)(2,5)]

[2^2 3^0 5^0 (2^1 3^1 5^0) (2^1 3^0 5^1)]

Take the lowest power of all of them 

2

for lcm use the highest power of each 60

gcd(12,20,18) = 2 3 5

lcm 

2

lcm(12,20,18) = 2

lcm = 180

gcd(12,20,18) = 2 3 5

lcm 

2

lcm(12,20,18) = 2

lcm = 180

e.g


