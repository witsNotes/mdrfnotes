---
title : "integers"
---

# Properties

We need several less familiar properties of divisibility and the primes in \mathbb{Z}

## Well ordering Axiom

Every none-empty set of positive integers has a smallest member

### Proof
From basic analysis we know that every none empty set of reals that is bounded bellow has an infimium. Hence every none empty set of positive integers(bounded below by zero) has an infimium in R and hence a smallest member. (is this complete?)

## Division Algorithm

Theorem 2.2.1

Let $n,d \in \mathbb{Z} | d \geq 1$ 

$\exists q, r \in \mathbb{Z}  s.t n = q d +r \cap 0 \leq r < d$

q and r are called the quotient and remainder respectively and are uniquely determined by n and d.


### Proof

Let $X  = \left\{n-td | t \in \mathbb{Z} , n-td \geq 0 )\right\}$

Show X is not empty.

>##### Not empty
>
>$n \geq 0 \implies n - 0d \in X$ Will always evaluate at greater than 0
>
>And
>
>$n < 0 \implies n - nd = n(1-d) \geq 0 \implies n-nd \in x$
>
>
>So $X \neq \phi$ and is bounded bellow by zero
>
>

By the well ordering axiom x has a smallest element.

if $X$ has a smallest element we let r be the smallest element in X
So $r \geq 0$ and $r = n-qd$ for some $q \in \mathbb{Z}$

So $n = qd + r$

$q \in Z r \geq 0$

>##### Show $r < d$
>
>
>Assume $r \geq d \implies 0 \leq r-d = n -qd -d = n - (q+1) d \in X$
>
>But r-d < r and r is the smallest element in X
>
>Contradiction So our assumption must be incorrect
>

Lastly the statement of the theorem requires tat q and r are unique so we will now shw unqueness.

>##### Uniqueness
>
>Assume that two such elements exist.
>
>Without loss of generalities can be written that $r \leq r_1$
>
>Thus if we let $n = q_1 d + r_1$ then
>
>$qd+r = q_1 d + r_1$
>
>$r_1 - r = (q - q_1)d$
>
>but $r_1 - r  \leq r_1 < d$
>
>So $r_1 - r$ is a multiple of d. If the number is a multiple of d which is also less than d implies $r_1 -r = 0$ and $q - q_1 = 0$; so $r_1 = r$ and $q_1 = q$ thereby proving uniqueness. 
>



## Example 2.2.2

Find the quotient and the remainder of n if

 #. n = -17, d = 5

  $n = qd + r$
  $-17 = -4 5 +34$
$\frac{n}{d} = -\frac{17}{5} = -4 + \frac{3}{5}$

### Geometrically.

Mark of the real line in multiples of d. Then n must lie between the multiple $q_d$ and $q+1 d$ for q in Z thus $q_d \leq n \ leq |q+1) d$ Check


## Divisor Definition

Let $n , d \in Z$ If d is not a divisor of n we write $d | \cross n$

Theorem 2.3.1

* Let $n,m,d \in \mathbb{Z}$

    $n|n \forall n \in \mathbb{Z}$

    Proof 

    $n = 1 n$ and $1 \in \mathbb{Z} \implies n | n$
* $d | m and m | n then d | n$

    Proof

  $d|m  \implies m = kd$
  and 
  $m | n \implies n = k_1d$

  Where $k , k_1 \in \mathbb{Z}$

  $\implies n = k_1 (k d)$
  = $kk_1d$  commutativity

  $d|n$ since$ $kk_1 \in \mathbb{Z}$


* if $d|n$ and $n|d$ then $d = \pm n$

  Proof $d|n$ and $n|d$ 
 
  $\imples n = k * d$ and $d = k_1n$ where $k,k_1 \in \mathbb{Z}$

  So $n= kk_1n$


  So $kk_1 = 1$ 

  $k = k_1 = (1,-1)$ since $k k_1\in \mathbb{Z}$.

*  If $d|n$ and $d|m$ then $d|xn + ym$ where $x,y \in \mathbb{Z}$

Proof 

$n = kd \quad  m = k_1d$ 


$xn+ym =kxd + k_1yd = (kx+k_1y)d$


$\implies d | xn + ym$ as $xk + yk_1 \in \mathbb{Z}$Z

Note $xnn + ym$ is called a linear combination of n and m.

#### Example 2.3.2

If $d \geq 1$ and $d | 3k+5$ and $d| 7k+2$ show
$d = 1$ or $d = 29$

$3k +5 = k_1 d \quad  7k +2 = k_2 d$


Eliminate k

$21k + 35 = 7k1 d 7k$ 

$-35 + 3 k_1 d = -6 + 4 kd$

Take common d to one side

$d(7k_1-3k_2) = 29$

$d|29$

$d = 1,29$


# Appendix

