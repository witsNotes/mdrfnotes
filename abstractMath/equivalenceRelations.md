---
title : "Equivalence Relations"
---

These form a very important part of what we have in the later chapters.

We define a relation $\equiv$ of a set A as and equivalence relation in A if $\forall a,b,c \in A

$\forall a,b,c \in A$ we have

i. $a \equiv a \forall a \in A$ reflexive property
ii. symetric a \equiv b \implies b \equiv a
iii. Transitive $a \equiv b \and b \equiv c \implie a \equiv b$

We must find a rule which satifies both conditions.

## Definition 2.1.2

The definition of the equivalence class.
Let the set of the set of (...) be an equivalence relation.

The equivalence class which we can denote [a]
 equal to the set of all x's inside a such that x is equivalent to a.

We say that A is the equivalence class of A generated by a.

eg) On $A = \mathbb{Z}$ with the equivalnce relation a-b = 2k

find elements equivalent to 1

This all odd integers
1 is also include as 1-1 is 0 which is a multiple of two.

remeber by definition equivalance is symmetric


## Equivalency class ii

Equivalence class for zero

All even number.

so Z fits in two equivalence classes. 

Further examples.

Equility is an equivalence relation on any set a for a and b elements of z say for instance say z is equal to b than b is a.

Equility is a special type of equivalnce relation.

a=a is reflexive

symetricness is also trivial.


## For trianlges in $\mathbb{R}^2$

The triangles are convgruent
All angles are same.

This is clearly symmetric reflexive and transitive

## For sets

Use order of sets as an equivalence relation.

# Rational numbers

We define the rationional numbers as the set of all $\frac{p}{q} | p,q \in \mathbb{Z}$

We define equivalence on the rationals as 

\frac{a}{b} \equiv \frac{c}{d} \iff ad = bc

This is an equivalence relation of the rationals.

Now we look at the three properties in turn.

Reflexivness

$\frac{a}{b} \equiv \frac{a}{b} since ab = ab \forall a,n in \mathbb{R}$

Symetricness

$\frac{a}{b} = \frac{c}{d} \implies ad = bc \implies bc = ad \implies$ Reflexivity

Transitivity

$$\frac{a]{b} = \frac{c}{d} \and \frac{e}{f} = \frac{c}{d} \implies ad = cb \and ce = fd \implis adce = cbfd \implies ae = fb$$

So this relation is an equivalence relation on all q

So the equivalnce class of $\right[\frac{a}{b}\left] is$

# Equivalnce paths

a is a member of it's own equivalnce path for all a in A (by the reflexive property)

Therefor A is an element of it's equivalnce path forall a in a

If we have the equivalence path.

# Proof

This is subtle so get from someone else

This is an iff and only if stament so we must proove it in both direction

1. If two equivalance classes are equal then the two elements must be equivalant.

By the symetric property A is equivalnce to a.

2. In the opposite direction we start of with two elements being equivalent. A is a member of the equialence class of a and b is a member of the equivalance class of A. and we must proove the the two equivalance classes are equal.

If we take c inside the equiavanle class of a

SO c is equivalante to a. and c is equivalent to b so c is an element of b.

Similairly the equivalence class of b is a subset of the equivalence class of A. direction we start of with two elements being equivalent. A is a member of the equialence class of a and b is a member of the equivalance class of A. and we must proove the the two equivalance classes are equal.

If we take c inside the equiavanle class of a

SO c is equivalante to a. and c is equivalent to b so c is an element of b.

Similairly the equivalence class of b is a subset of the equivalence class of A.

For the third propety.

If equivalence classes are not equal then the inteersection between to equivalance classes is the empty set

Proof by contradiction. And use the contrapositive.

If our theorem says if a then b. We start the proof by saying if a 

Asonsume there is an element in the class


## Equivalence examples

is less than an equivalance poperty

# Partition

## Definition

A partition is mutually exclusive exhaustive and non empty subsets of A

This is donated by

$$\sum = \Union A | A_i \in A \wedge A_i \intersection A_j = \phi  A_i \neq \phi$$


$$a \in [a] \forall a \in A$$

### Proof

The equivalence class of a is all elements in the set A such that b\equiv a

A is in the equivalance class of A by the reflexive property
[a] \equiv [b] \iff a \equiv b

### Proof
[a] = [b] \impliies b \in \mathbb[a] \implies b \equiv a \implies a \equiv b

Prove the other way

$$a\equiv b \implies b \equiv a \text{by sytmetric porperty} \implies a \in [b] \wedge b \in [a]$$

Take any element c in [a]

Now $$c \in [a] \implies c \equiv a \implpies c \equiv b$$ By the transitive porperty

$$\implies c \in [b]$$

Simlairly prove $[b] \subset [a]$

## Part 3

If you have the equivalace class of a not equal to the equivalance of b than the intersection of the two sets is the epty set.

This is used to form partitions for whole sets.

### Proove by contrapositive

Proove the cntrapositive.

let $c \in [a] \union [b]$

$$\implies c \in [a] and c \in [b]$$
$$\implies c \equiv a \wedge c \equiv b$$
$$\implies a \equiv b$$

# Last part

Let a = d which is an equivalance class than d = [a]

pick c in d $a \equiv c$ $[a]=[c]=[d]$

# Tranversal

What is a transversal

$A \neq 0$ and an $\equiv$ is an equivalance.  Them the partion into equivalant and not equivalant onto b is a partition.


## Example

Let A be the set of the integers

A \equiv b ff a - b = 2k k \in k

This is a partion of of A \{[0][1]\}


## Definition

What is a transversal

Let #\Sigma by a set of distinct equivalance classes of a An equivalance class under a specific equivalance relation

Let $\Tau$ be a set consisiting of exactly one element from each equivalnce class


Then the set $|Tua$ is a transveral over A under operations wee definie

There is no ordering implied in either the partion of trversal. Obviously it is a set.

