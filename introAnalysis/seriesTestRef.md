---
title: "Series tests"
---

# Imports

import seriesDefinitions as sd

# Series tests
## Divergence test

If $\nexists \lim_{n \to \infty} a_n \vee \lim_{n\to \infty}a_n \neq 0$ Then the series diverges


### Proof

#### Contrapositive

Let $s_n = a_1 + \dots + a_n$

Solve for a term of the sequence in terms of the series.
Than $a_n = s_n - s_{n-1} = 0$

Since the series converges the sum of the series exists. Also n+1 approaches infinity as n approaches Infinity. So the limit of $s_{n-1}$ is also the sum of the series.

## Convergence tests

### Geometric test

Given a geometric series the series converges if $|r| < 1$ and diverges if $|r| > 1$ and other tests need to be used if $|r| = 1$ 


#### Proof

$$s_n = a + ar + ar^2 + \dots + ar^n$$

$$r s_n = ar + ar^2 + \cdots + ar^n + ar^{n+1}$$

subtracting one from the other we get

$$s_n - rs_n = a - ar^{n+1}$$


$$s_n = \frac{a(1-r^{n+1})}{1+r}$$

and so by theorem 2.5 $s_n$
and thus the geometric series converges if |r| <1, with

$$\sum _{n=0}^{\infty} a r^n = \lim_{n \to \infty} s_n = \frac{a}{1-r}$$
 and diverges if |r| > 1 or r = -1. Finally, for r = 1,$s_n = a(n+1), and thus s_n = a(n+1) and thus $s_n \to \infty$ as $n \to \infty$

#### Examples

To do still


### Comparison test

Take the series A and B with non-negative terms where $a_{i} \leq b_i \; \forall i \in I$

If B converges than A converges

The contrapositive is also often useful

#### Note on alignment

Needs formalization.

The exact alignment of the series is arbitrary we could say $a_i < b_{20+1}$ for example and be fine. Because no finite number of terms will affect the convergence/divergence of a sequence.

#### Proof

Let $s_k$ be the $k^{th}$ sum of A and $t_k$ be the $k^{th}$ sum of B. So both $\{s_n\} \; \{t_n\}$ are increasing sequences and $\forall k \; s_k \leq t_k$

$\implies$ If B converges $t_k \leq M \forall k \in \mathbb{N}$(t is bounded), and $s_k \leq t_k \leq M \forall k \in \mathbb{N}$

So ${s_n}$ is bounded and therefor converges by

>#### Series convergence $\iff$ partial sums are bounded
>
>\begin{align*}
>a_i \geq 0 \forall i \in \mathbb{N}^* \wedge s_n = \sum_{i=i_0}^{n}{a_i}
>\\
>\implies \sum_{i=i_0}^{\infty}{i_n} \; \text{con} \iff s_n \; \text{is bounded}
>\end{align*}
>
>##### Proof
>
>$s_{n+1} = s_n + a_{n+1} \geq s_n$ So $\{s\}_{n=0}^{\infty}$ is an increasing sequence. By theorem 2.10 this sequence and hence the series converges $\iff$ the sequence $\{s_n\}$ is bounded.
>
>
>

#### Alternative proof

From theorem 5.4(Basic sum laws) Both $\sum |a_n|$ and $\sum 2|a_n|$ converge.

$$0\leq a_n + |a_n| \leq 2|a_n|$$

So by comparison test

$$\sum a_n + |a_n| $$

Converges

$$\sum a_n = \sum a_n + |a_n| - \sum |a_n|$$

So again by theorem 5.4 it converges.

$0 \leq an + |an| \leq 2|an|$


### Alternating series

If an alternating series satisfies


i) $b_n$ is decreasing (not strictly) 

ii) $\lim_{n \to \infty} b_n = 0$

Then the series converges

#### Proof

We are trying to show


$$\forall \epsilon \exists K \quad \text{s.t} \quad \forall m \geq k \geq K \quad \left| \sum_{i=k}^{m} (-1)^i b_i \right| < \epsilon$$

Start by noting because $\{b_n\}$ is decreasing and positive.

$$\lim_{i\to \infty}b_i =0 \implies \forall \epsilon \exists K \quad \text{s.t} \quad \forall \, k > K \quad b_k \leq b_K < \epsilon$$

We now show that 
$$\left| \sum_{i=k}^{m} (-1)^i b_i \right| \leq b_k \forall k$$

This is done for two case

  #. If $m-k$ is even
      Replace $m$ with $k+2l$

      $$(-1)^k \sum_{i=k}^{k+2l} (-1)^i b_i = \left(\sum_{i=0}^{l-1}(b_{k+2i} - b_{k+2i+1})\right) + b_{k+2m} = b_{k} - \sum_{i=0}^{l-1}(b_{k+2i +1} - b_{k+2i+2})$$

      Each bracketed term is greater than or equal to zero. So they can all be dropped to get inequalities.

      $$0 \leq b_{k+2l} \leq (-1)^k \sum_{n=k}^{k+2m} (-1)^n b_n \leq b_{k}$$

 #. If m-k is odd
      Replace $m$ with $k+2l +1$
      $$(-1)^k \sum_{i=k}^{k+2l + 1} (-1)^i b_i = b_i - \sum_{i=0}^{l-1}(b_{k+2i +1} - b_{k+2i+2}) - b_{k+2i+1}$$


      $$0 \leq (-1)^k \sum_{n=k}^{k+2l+1} (-1)^n b_n \leq b_{k} - b_{k+2l+1} \leq b_k$$

Finally transitivity gives

$$\forall \epsilon \exists K \quad \text{s.t} \quad \forall m \geq k \geq K \quad \left| \sum_{i=k}^{m} (-1)^i b_i \right| \leq b_k \leq b_K < \epsilon$$


#### Example

The alternating series 
$$\sum_{n=1}{\infty} (-1)^{n-1} \frac{3n}{4n-1}$$ Does not converges since the limit of $\frac{3}{n {4n -1}}$ does not converge to zero

### Ratio test

#### Result

Let A be a series with terms $a_i$ 
\begin{align*}
  \lim \sup \left|\frac{a_{i+1}}{a_i}\right| &= L \\
  \lim \inf \left|\frac{a_{i+1}}{a_i}\right| &= l
\end{align*}

If $a_n \neq 0 \; \forall n$ then

1. If $L < 1$ then the series converges absolutely

2. If $l > 1$ than the series diverges.


#### Proof


##### Part 1

This is done by a comparison with a convergent geometric series B where $b_j = b_0 r^j$

B contains $r^{j}$ so in order to show $a_i < b_j$ write $a_i$ as a product with $j$ terms that are each less than $r$ and an term to math $b_0$.
  
Hence for $i > k$

$$|a_m| = |a_k|  \prod_{i=k}^{m-1} \left| \frac{a_{i+1}}{a_i}\right|$$

Now we just need to pick k large enough that our information about the behavior of the ratio as i goes to infinity to be useful. Set $r = L + \epsilon | \epsilon > 0 , \, r <1$. Which gives us $\exists r \in \mathbb{R}$(how? find chapter 1) and $r > L$

$$\forall r \, \exists \, K \in \mathbb{N} \quad \text{s.t} \quad \forall i \geq K \; \left| \frac{a_{i+1}}{a_i} \right| < r$$

So we just need $k \geq K$ and $j = m-k$($(m-1) -(k-1)$ inclusive of $k^{th}$ term) and we have $a_i < b_j \forall i > k \geq K$ and B converges. So A converges.


##### Part 2

We use

>## Divergence test
>
>If $\nexists \lim_{n \to \infty} a_n \vee \lim_{n\to \infty}a_n \neq 0$ Then the series diverges
>
>
>### Proof
>
>#### Contrapositive
>
>Let $s_n = a_1 + \dots + a_n$
>
>Solve for a term of the sequence in terms of the series.
>Than $a_n = s_n - s_{n-1} = 0$
>
>Since the series converges the sum of the series exists. Also n+1 approaches infinity as n approaches Infinity. So the limit of $s_{n-1}$ is also the sum of the series.
>

and show that the sequence $\{a_i\}$ does not converge to zero. Right each $a_i$ as a product of ratios so we can use our information about the ratios.

$$|a_m| = |a_k|  \prod_{i=k}^{m-1} \left| \frac{a_{i+1}}{a_i}\right|$$

Now put a lower bound on i so limiting conditions apply.

Let $l' \in (1,l)$
$$\implies \exists K \in \mathbb{N} | \left( \left|\frac{a_{i+1}}{a_i}\right| > l' \forall i \geq K\right)$$ 
Hence for $m > K$
$$|a_m| = |a_K|  \prod_{i=K}^{m-1} \left| \frac{a_{i+1}}{a_i}\right| > |a_K|$$

so that $a_n \not\to 0$ as $n\to \infty$ Hence the sequence diverges by the test for divergence. 

### Root test

* If $\lim \sup |\sqrt[n]{a_n}|<1$ The series converges (Copied verbatim should one of these be infimium)
* If $\lim \inf |\sqrt[n]{a_n}|>1$ The series diverges

#### Proof

We are going to us a comparison test with  a geometric series $B (b_j = r^j | r < 1) $to show that the series converges absolutely and therefor converges.

Set $r = L + \epsilon | \epsilon > 0 , r <1$

$\implies \exists K \in \mathbb{N}| \root[i]{a_i} < r \quad \forall i \geq K$

Since $\sum (r)^n$ is a convergent geometric series, it follows from the comparison test that the series $a_n$ starting at K converges absolutely and so will the series starting at $a_0.$
as an converges absolutely.

Let L' = (1,L) .Then $\forall K \in \mathbb{N} \exists m \geq k \quad \text{s.t } \quad \root{m}{|a_n|} > L' 

$$$\left| a_i \right| > L'^{m} > 1$$

So $a_i$ Does not converge to zero

## Power series

This test differs from the others as here we give a range of a parameter x for which the series converges and diverges.

If we have a series in the form

>## Power series
>
>A series in the form
>$$\sum c_i (x-a)^i \quad \text{where} \quad x ,a (c_i | i \in \mathbb{N}) \in \mathbb{R}$$ is the power series centered around a( also a series about a, or a series in (x-a))

### Radius of convergence

For each power series the is a number (R) such that for all x where $|x-a| < R$ the series converges. R is called the radius of convergence.

 #. $R=0$ if $\lim_{i \to \infty} \sqrt[i]{|c_n|}= \infty$
 #. $R= \frac{1}{\lim_{i\to \infty} sup\sqrt[i]{|c_n|}}$ if $0 \leq \lim_{i \to \infty  }sup \sqrt[i]{|c_n|} \in R$
 #. $R=\infty$ if $\lim_{i\to \infty} sup\sqrt[i]{|c_n|}= 0$

### Domains for power series

    Thoerem 5.12

  #. If $R=0$ then the series converges only at $x=a$
  #. If $R= \infty$ the series converges for all the reals
  #. If $0< R\in \mathbb{R}$ then the series converges for $|x-a| < R$ and diverges for $|x-a| > R$ and must be checked at each boundry (Called the interval of convergence)

#### Proof

##### Part 1

If $\lim sup \sqrt[n]{|c_n|} = \infty$ then forall $x\neq a$ $\lim_{i \to \infty} sup \sqrt[i]{|c_n||x-a|^n } = \sqrt[i]{|c_i|}x-a|= \infty$ 

So by

>### Root test
>
>* If $\lim \sup |\sqrt[n]{a_n}|<1$ The series converges (Copied verbatim should one of these be infimium)
>* If $\lim \inf |\sqrt[n]{a_n}|>1$ The series diverges
>
>#### Proof
>
>We are going to us a comparison test with  a geometric series $B (b_j = r^j | r < 1) $to show that the series converges absolutely and therefor converges.
>
>Set $r = L + \epsilon | \epsilon > 0 , r <1$
>
>$\implies \exists K \in \mathbb{N}| \root[i]{a_i} < r \quad \forall i \geq K$
>
>Since $\sum (r)^n$ is a convergent geometric series, it follows from the comparison test that the series $a_n$ starting at K converges absolutely and so will the series starting at $a_0.$
>as an converges absolutely.
>
>Let L' = (1,L) .Then $\forall K \in \mathbb{N} \exists m \geq k \quad \text{s.t } \quad \root{m}{|a_n|} > L' 
>
>$$$\left| a_i \right| > L'^{m} > 1$$
>
>So $a_i$ Does not converge to zero
>

The series converges if x is not equal to a.

##### Part 2

If $\lim_{i \to \infty} sup \sqrt[i]{|c_i|} = r \in \mathbb{R}$

$$\lim_{i \to \infty} sup \sqrt[i]{|c_i(x-a)^n|} = |x-a| \lim_{i \to \infty} sup \sqrt[i]{|c_i|}$$

So if $\lim_{i \to \infty} \sqrt[i]{|c_i|} = 0$ Then the series converges by the root test.

##### Part 3

So if $\lim_{i \to \infty} \sqrt[i]{|c_i|} > 0 \in R$ Then the series converges if $|x-a| \lim_{i \to \infty} sup \sqrt[i]{|c_i|}$ by the root test.

So the series converges if

$|x-a| < \frac{1}{\lim_{i \to \infty} sup \sqrt[i]{|c_i|}}$ and diverges for $|x-a| > \frac{1}{\lim_{i \to \infty} sup \sqrt[i]{|c_i|}}$ 

rooth of abs Cn \in Reals,then 

lim sup root abs Cn(n-1)^n = |x-a|lim sup nthrooth |Cn|

Thus the powerseries con if forall.


If finally $< lim sup _{n\to infty{ \in R} then by the root theorem test the series conveges if$

$|x-a| lim sup nth rooth of abs cn < 1$
$|x-a| < 1/limsup nthroot abs Cn$

and diverges if 

$|x-a| > rhs.$

#### Calculate ratio of convegence.

Use the formula the limit an / an+1( Do not need proof)


##### Example 5.1 a

* $\sum \frac{(2n)^n}{n}$

$= 2^n /n * x ^n$

Calculate ratio of convergence

R = \lim_{n \to \infty} |an/an+1| = lim |a^n / n div 2^n+1/n+1| = lim |1/2 +1/2n| = 1/2$

converges on (-\frac{1}{2}, \frac{1}{2})


Convergent by alternating series test.

if n = \frac{1}{2} then 

$it = 1/n$ diverges$

Div

R = [-\frac{1}{2} , \frac{1}{2})

### 
## Tutorial 5.2.1

 4. Prove weather the following sequences converge or not
    #.$$\sum_{n=1}^{\infty}\left(\frac{(-1)^{n}n}{2n+1}\right)^{2n}$$
      Try the root test 
      \begin{align*}
      &\lim_{n\to\infty}\sqrt[n]{\left(\left|\frac{-1^{n}n}{2n+1}^{2n}}\right|\right)}
      =&\lim_{n\to\infty}\left(\left|\frac{-1^{n}n}{2n+1}^{2}\right|\right)
      =&\lim_{n\to\infty}\left(\left|\frac{-1^{n}n}{2n+1}^{2}\right|\right)
      =&\left(\frac{1}{4}\right) \leq 1
      \end{align*}
    #.$$\sum_{n=0}^{\infty}\frac{n!2^{n}}{(2n!)}$$
      Try ratio test
      \begin{align*}
      &\left|\frac{\frac{(n+1)!2^{n+1}}n+1}{(2(n+1))!}}{}\right|
      \end{align*}
 msple sadf Correct 
 
This test differs from the rest in that here we find the range of a parameter x for which a series converges.

rooth of abs Cn \in Reals,then 

lim sup root abs Cn(n-1)^n = |x-a|lim sup nthrooth |Cn|

Thus the powerseries con if forall.


If finally $< lim sup _{n\to infty{ \in R} then by the root theorem test the series conveges if$

$|x-a| lim sup nth rooth of abs cn < 1$
$|x-a| < 1/limsup nthroot abs Cn$

and diverges if 

$|x-a| > rhs.$

#### Calculate ratio of convegence.

Use the formula the limit an / an+1( Do not need proof)


##### Example 5.1 a

* $\sum \frac{(2n)^n}{n}$

$= 2^n /n * x ^n$

Calculate ratio of convergence

R = \lim_{n \to \infty} |an/an+1| = lim |a^n / n div 2^n+1/n+1| = lim |1/2 +1/2n| = 1/2$

converges on (-\frac{1}{2}, \frac{1}{2})


Convergent by alternating series test.

if n = \frac{1}{2} then 

$it = 1/n$ diverges$

Div

R = [-\frac{1}{2} , \frac{1}{2})

### 
## Tutorial 5.2.1

 4. Prove weather the following sequences converge or not
    #.$$\sum_{n=1}^{\infty}\left(\frac{(-1)^{n}n}{2n+1}\right)^{2n}$$
      Try the root test 
      \begin{align*}
      &\lim_{n\to\infty}\sqrt[n]{\left(\left|\frac{-1^{n}n}{2n+1}^{2n}}\right|\right)}
      =&\lim_{n\to\infty}\left(\left|\frac{-1^{n}n}{2n+1}^{2}\right|\right)
      =&\lim_{n\to\infty}\left(\left|\frac{-1^{n}n}{2n+1}^{2}\right|\right)
      =&\left(\frac{1}{4}\right) \leq 1
      \end{align*}
    #.$$\sum_{n=0}^{\infty}\frac{n!2^{n}}{(2n!)}$$
      Try ratio test
      \begin{align*}
      &\left|\frac{\frac{(n+1)!2^{n+1}}n+1}{(2(n+1))!}}{}\right|
      \end{align*}
 msple sadf Correct 
 

#### Warning

Do not confuse this proof with the ratio proof!

### General advise.

If it looks divergent check this first. 

Otherwise geometric test first.

For $\ln{n}$ use a comparison test.

# Appendix

