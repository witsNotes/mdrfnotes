---
title: "Limits and continuity of functions"
---

# Imports

import functionLimits as fl 

import continuity as c

# Functions

># Function limits
>
>
>## Limits
>
>### General definition
>
>If we can find a deleted neighbourhood around $x_0$ such that f(x) always falls in a deleted neighbourhood around $y_0$
>
>K and epsilon are synonymous
>
>The K and $\epsilon$ works on the range and the delta works on the domain.
>
> >### Deleted neighborhood
>>
>>Let $a \in \mathbb{R}$ An interval in the form $(c,d)$ with $c<a<d$ is called a neighbourhood of a, and the set $(c,d)/{a}$ is called the deleted neighbourhood of a.
>>
>
>
>### $x_0 y_0 \in \mathbb{R}$
>
>Let f be a real values function and a and L be members of the reals and assume that the domain of f contains the deleted neighbourhood of a.
>
>$$\forall \epsilon > 0 \exists \delta > 0 \quad \text{s.t} \quad a-\delta < x < a + \delta \implies |f(x) -L | \epsilon$$
>
>#### One sided limit
>
>$$\forall \epsilon > 0 \exists \delta \quad \text{s.t} \quad a < x  < a+\delta \implies |f(x)-L | < \epsilon$$
>
>
>### $x \to \infty \vee - \infty$
>
>#### Positive infinity
>
>Let x be a real function defined on an $[c,\to \infty)$ or $(c,\infty)$
>
>If l (the limit of f(x) as x approaches infinity) exists it is defined by.
>
>$$\forall \epsilon > 0 \exists K > 0 \quad \text{s.t} \quad x > K \implies |f(x) - l | < \epsilon$$
>
>
>#### Negative infinity
>
>Let x be a real function defined on an $(-\infty ,c)$ or $(-\infty,c]$
>
>If l (the limit of f(x) as x approaches negative infinity) exists it is defined by.
>
>$$\forall E > 0 \exists K < 0 \quad \text{s.t} \quad x < K \implies |f(x) - l | < \epsilon$$
>
>
>### $y \to \infty \vee - \infty$
>
>This is when the limit goes to infinity or does not exist.
>
>#### Positive infinity
>
>
>Let f be a real function which includes the deleted neighborhood of a
>
>f(x) approaches infinity $\iff$
>
>$$\forall k>0 \exists \delta > 0 s.t a - \delta < x < a+d f(x) > k$$
>
>#### Negative infinity
>
>Let f be a real function which includes the deleted neighborhood of a
>
>f(x) approaches negative infinity iff
>
>$$\forall k<0 \exists \delta > 0 \quad \text{s.t} \quad a - \delta < x < a+d f(x) < k$$
>
>### $y,x \to \infty \vee -\infty$
>
>f(x) approaches infinity as x approaches infinity
>
>$$\forall A > 0 \exists K > 0 s.t x > K \implies f(x) > A$$
>
>For the negative case this is similar.
>
>Infinites can also be defined using the one-sided limits.
>
>### Limit laws
>
>    Theorem 3.3
>
>We can calculate limits using these laws unless "From definition" is used in the question.
>
>Let $a, c \in \mathbb{R}$ and suppose that the real function f and g are defined in a deleted neighbourhood of a and that $\lim_{ x \to a } f(x) = L \in \mathbb{R}$  and $\lim_{ x \to a} g(x) = M \in \mathbb{R}$
>
> #. $\lim_{ x \to a} c = c$
> #. $\lim_{ x \to a}[f(x) + g(x)] = L + M$
> #. $\lim_{ x \to a} [f(x)-g(x)] = L-M$ 
> #. $\lim_{ x \to a} [cf(x)] = cL$ 
> #. $\lim_{ x \to a} [f(x)g(x)] = LM$
> #. ratio
>    #. if $M = 0 \wedge L = 0$ use L'Hopital 
>    #. if $M = 0 \wedge L \neq 0$ the limit does not exist
>    #. if $M\neq 0$ $\lim_{ x \to a} \frac{ f(x) }{ g(x)} = \frac{ L }{ M }$ 
> #. $\lim_{ x \to a} x = a$ 
> #. $\forall n \in \mathbb{N}$ 
>    #. $\lim_{ x \to a} x^n = a^n$
>    #. $\lim_{ x \to a} \sqrt[n]{x} = \sqrt[n]{a}$ If n is even we assume a > 0
>    #. $\lim_{ x \to a} \sqrt[n]{f(x)}= \sqrt[n]{L}$ 
> #. $\lim_{ x \to a} \left| f(x)\right| \implies L = 0$ 
>
>All The limit laws
>
>Remember to use la hoptial when on fractions when top and bottom limit is equal to one
>
>#### Proofs of 2 6 and 7.3
>
>other proofs are exactly the same as in chapter 2
>
>##### 2
>
>The sum of the limits is the limit of the sums. 
>
>We already now that the limit of f and g exist.
>
>let $\epsilon > 0$ then there exists $\delta_1 \wedge \delta_2$ s.t 
>
>$|f(x) - L | < \frac{\epsilon}{2} if 0 < |x - a| < \delta_1$
>$|g(x) - L | < \frac{\epsilon}{2} if 0 < |x -a| < \delta_2$
>
>Put delta as the minimum delta between delta 1 and delta 2. So $|x-a| < \delta_1 \and |x-a| < \delta_2$ 
>
>Therefor $|(f(x) + g(x)) - (L + M)| = |(fx - L) + (g x - M)|$
>
>by Triangle inequality
>
>$$\leq |f (x) - L| + | g(x) - M |$$
>
>$$\leq \frac{epsilon}{2} + \frac{\epsilon}{2} = \epsilon$$
>
>Conclusion
>
>f(x) + g(x) approaches L+M as $x \to a$
>
>##### 6
>
>Product rule
>
>###### Case 1
>
>L and M are both zero
>
>Let $\epsilon > 0 \exist \delta_1 \wedge \delta_2$ s.t
>
>$|f(x) -L | < 1 \quad \forall |x-a| < \delta_1$
>
>and $|g(x) -M | \< \epsilon if 0 < |x-a| < \delta_2$
>
>Use the minimum delta 
>
>$$|f(x)g(x) - LM| = |f(x)g(x)| = |fx|\times|gx| < 1 \times \espsilon = \epsilon$$
>
>###### Case 2
>
>For all functions.
>
>We expand $f(x)g(x)$ to get terms which we can use previous laws on
>
>$$f(x)g(x) = (f(x) -L)(g(x)-M) + L(g(x)-M) -f(x)M$$
>
>Using 1 2 and 3 and the definition of limits we get.
>
>$$\lim_{x \to a}f(x)g(x) = LM$$
>
>##### 7.3
>
>####### Case 1
>
>$f(x)=1$
>
>We must show
>
>$$\left|\frac{1}{g(x)} - \frac{1}{M} \right| < \epsilon$$
>
>
>Since $g(x) \neq 0$ 
>
>
>## Examples
>
>$$f(x) = \frac{1}{}$$
>
>find
>
>$$\lim_{n \to \infty} f(x)$$
>
>And
>
>$$\lim_{x \to -infty f(x)$$
>
>
>Start by guessing L = 0 for both and show their is no contradiction
>
>Let $\epsilon > 0$ and $k=\frac{1}{\epsilon} > 0$
>
>3 Then x > K gives 
>
>$$\left| \frac{1}{x} \right| = \frac{1}{x} < \frac{1}{k} = \epsilon$$
>
>When ever we work with limit definitions we will always follow this four step pattern.
>
>In a test we must rewrite all steps for negative infinity
>
>3 $$\left| \frac{1}{x} \right| = -\frac{1}{x} < -\frac{1}{k} = \epsilon$$
>
>

># Continuity
>
>## Definition
>
>    3.7
>
>Continuity is now give in terms of a delta epsilon definition.
>
>$$\forall \epsilon > 0 \exists \delta > 0 \forall 0 \leq |x-a| < \delta \implies |f(x) - f(a)|$$
>
>Subtle difference L is replaced with f(a)
>
>This is the same but without the deleted interval
>
>## Theorem 3.13
>
>The following functions are all continuous
>
>* All trig functions on thier domains
>* The exponential and logarithmic function
>* Absolute value function
>* Polynomials
>* Rationals
>* Roots
>
>### Proofs
>
>i ii iii and vii all easily follow from limit laws others are part of tutorials.
>
>#### vi
>
>forall epsilon > 0 let delta = epsilon than forall
>
>## Theorem 3.14
>
>Let a be a real function and f be a real function definied in the neighbourhood of a. Then f is contionious at a iff for each sequence xn in the domain of f with the limit of fa =f(a) then the sequence f(x_{a}) satisfies $\lim_{n\to\infty} f(na)=f(x)$ If we can find a sequence.
>
>If we take a sequecnce and substitute those x values into the function we map the domain onto the function. Then we will have the sequence of our function tending towards f(a)
>
>### Proof
>
>#### Part one forward direction
>
>Assume f is continuous
>
>Let $(x_{a})$ be a seq in the dom(f) with $\lim_{n\to \infty} x_{a} = a$
>
>We must show $\lim_{} f(an) f(a)$. Hence let $\epsilon > 0$.
>
>Since f is cont at a, there is $\delta > 0$ such that
>
>$$\left|x-a\right| < \delta \implies \left|f(x) - f(a)\right| < \epsilon$$
>
>Since $\lim_{n\to \infty} x_{n} = a$, there is $k \in \mathbb{R}$
>
>
>
>## The range of continuous functions with bounded domains are bounded
>
>    Theorem 3.17
>
>### Proof
>
>*Use proof by contradiction*
>
>Assume the $f([a,b])$ is unbounded. We need to transform the function into a sequence. We do this by bisecting the interval and  replacing the upper or lower bound with the midpoint. Then we look at the sequence of upper and lower bounds. 
>
>
>Then let $d$ be the midpoint of the interval.
>
>So
>$f([a,b]) = f([a,d]) \cup f([d,b])$ is unbounded $\implies$ either $f([a,d])$ or $f([d,b])$ is unbounded.
>
>By induction we find an interval $[a_n, b_n]$ where such that $a_n$ is increasing $b_n$ is decreasing  and $f([a_n,b_n])$ is unbounded. 
>
>$$b_n = a_n + \frac{1}{2}(b_{n-1} + a_{n-1}) = a_n + 2^{-n}(a-b)$$
>
>So by limit laws 
>
>$c := \lim a_n = \lim b_n$  and  $c \in [a, b]$(why)
>
>Since $f$ is continuous at $c$  $\exists \delta > 0 \quad \text{such that} \quad (f((c-\delta, c+\delta) \cap [a,b]) \subset (f(c)-1,f(c) + 1)$
>
>$$\exists K_0, K_1 \in \mathbb{N} \quad \text{such that} \quad \forall n > K \, |c_n - b_n| < \delta, \; \forall n > K_1 |c_n - a_n| < \delta$$
>
>Taking $K_2 = \max{K_0,K_1} \quad \forall n > K_2 \quad c_n - \delta < a_n < c < b_{n} < c_{n} - \delta$
>
>And so
> $$f([ a_{n} , b_{n} ]) \subset (f((c-\delta, c+\delta) \cap [a,b]) \subset (f(c)-1,f(c) + 1)$$
>
> Which gives $f([ a_{n}, b_{n}])$ is both bounded and unbounded.
>
>
>## A function on a closed interval has a minimum and maximum
>
>    Theorem 3.18
>
>### Proof
>
>Let $f$ be a function defined on the closed interval $[a,b]$ we need to show $is x_{0} x_{2} \in [a,b] \quad \text{such that}$
>
>  #. $f(x_1) = \sup f([a,b])$
>  #. $f(x_2) = \inf f([a,b])$
>
>#### Part 2
>
>By 
>
> >## The range of continuous functions with bounded domains are bounded
>>
>>    Theorem 3.17
>>
> >### Proof
>>
>>*Use proof by contradiction*
>>
>>Assume the $f([a,b])$ is unbounded. We need to transform the function into a sequence. We do this by bisecting the interval and  replacing the upper or lower bound with the midpoint. Then we look at the sequence of upper and lower bounds. 
>>
>>
>>Then let $d$ be the midpoint of the interval.
>>
>>So
>>$f([a,b]) = f([a,d]) \cup f([d,b])$ is unbounded $\implies$ either $f([a,d])$ or $f([d,b])$ is unbounded.
>>
>>By induction we find an interval $[a_n, b_n]$ where such that $a_n$ is increasing $b_n$ is decreasing  and $f([a_n,b_n])$ is unbounded. 
>>
>>$$b_n = a_n + \frac{1}{2}(b_{n-1} + a_{n-1}) = a_n + 2^{-n}(a-b)$$
>>
>>So by limit laws 
>>
>>$c := \lim a_n = \lim b_n$  and  $c \in [a, b]$(why)
>>
>>Since $f$ is continuous at $c$  $\exists \delta > 0 \quad \text{such that} \quad (f((c-\delta, c+\delta) \cap [a,b]) \subset (f(c)-1,f(c) + 1)$
>>
>>$$\exists K_0, K_1 \in \mathbb{N} \quad \text{such that} \quad \forall n > K \, |c_n - b_n| < \delta, \; \forall n > K_1 |c_n - a_n| < \delta$$
>>
>>Taking $K_2 = \max{K_0,K_1} \quad \forall n > K_2 \quad c_n - \delta < a_n < c < b_{n} < c_{n} - \delta$
>>
>>And so
>> $$f([ a_{n} , b_{n} ]) \subset (f((c-\delta, c+\delta) \cap [a,b]) \subset (f(c)-1,f(c) + 1)$$
>>
>> Which gives $f([ a_{n}, b_{n}])$ is both bounded and unbounded.
>>
>>
>
>We know that $f([a,b])$ has an superium 
>
>*By contradiction*
>
>Assume that $\nexists x \in [a,b] \quad \text{such that} \quad f(x) = \sup f([a,b])$ 
>
>$$g x := \frac{1}{\sup f([a,b]) - f(x)} \, , x \in [a,b]$$
>
>So by theorem 3.18 and 3.13 g is both continuous and bounded on the domain $[a,b]$
>
>$$\exists K \quad \text{such that} \quad 0 < g(x) \leq K forall x \in [a,b]$$
>
>$$\implies \frac{1}{K} \leq \frac{1}{g(x)} = M -f(x) \\ 
>\implies f(x) < M - \frac{1}{K}$$
>
>So M there is a number smaller than M which bounds $f([a,b])$ Which is a contradiction
>
>

## Proof of part 2

Assume f is not cont at a. Then 
$$\exists \epsilon > 0 \forall \delta > 0, \exists x \in dom(f) , |x-a| < \delta$$

and $\left| f(x) - f(a)\right| \geq \epsilon$

In particular, for $\delta = \frac{1}{n}$

This producers a contradiction therefor the function f is continius on a.

## Tutorial 3.4.1
## Intermediate value theorem


If we have a function f. If we pick a value a and a value b so that a is not equal to b and we look at the interval a b. If the value y = k goes through f(a) and f(b) than y goes through both values. There must exist a value c in (a,b) such that f(c) =k

### Proof

Create a new function g(x) = f(x) - k so we now have values greater than or less than zero.

g(a) g(b) < 0 as only one is positive. 
Then keep bisecting the interval and use new intervals until we get to zero.

If the process does not decrease $b_{n}$ decrease $a_{n}$ increases

<<<<<<< HEAD
Both increasing and decreasing sequences tend to c. It follows that the 

$$limt of g(c)^{2} = limg(a) \lim_{b} \leq 0$$
=======
Both increasing and decreasing sequences tend to c. It follows that the limt of $$g(c)^{2} = limg(a) \lim_{b} \leq 0$$
>>>>>>> 30f8de8bd588de7a343136dafb496a98c103c2a2

As a sqaure can never be negative the only solution is that the limit is zero.


Suppose there is a close interval


## Test question

Prove sin x is continuous in $x \in \mathbb{R}$

Let $\epsilon > 0$ $f\left|f(x) - f(a)\right| < \delta$

$$\left|sin(x) - sin(a)\right| = \left|2cos\frac{x+a}{2} sin\frac{x-a}{2}\right|$$

Since $\left|\right|$
How do we prove this.

<<<<<<< HEAD
## When is a set an

Closed is resrved by a functions as endpoint.


## Theorem 3.18

A continius function on a closed interval achievs its infimium and suppremuim.

## Theorem 3.19

If f is contiunius on a closed interval than eithier f(x) is a single value or a closed interval

### Proove

Use cor 3.16 and Thm 3.17 with 3.18 it follows tha f([a,b]) it follows that either a singlton or a bounded interval which contains both its inf and its sup. But such an interval is of the form [c.d] with c<d. The supremuim and infiumium condition ensures a inteerval not a supremium.

Let f be function and i be an interval and i be a function which is the inverse if f is strictly monotonic continius function. Then the inverse of the function exists and is continius.

#### Proof

by cor 3.16 f(i) is an inteval

Start with let x be increasing.

then also f inverse is strictly increasing.

Let b be an element of f of i. ie b = f(a) for some a in i

The domain of a function is the range of the inverse and the range of a.

If a is not the left endpoint of i then b is not the left end point of b(a) and for y in f(i) with y < b = f(a) we have f^{-1} we have f^{-1} f(y) < f^{-1}(f(a)) = a

Therefor f inverse is bounded above and increasing on f(i) intersectino with minus infinity b and thus alpha $\lim_{ y \to b^-} f^{-1}(y)$ 

We know this limit exists and alpha \leq f(-1)(b) = a, see Thm 3.8

Assume that alpha is less than a. Since a in I is not the left end point of I , $I\intersection (\alpha,a) \neq \phi.$ Let $x_1 \in I \intersection (\alpha,a) 

Then f(x_1 < f(a) = b ) $\implies$
  $x_1 = f^{-1} (f(x_0)) \leq \alpha.

  This gives the contradciton. $x_0 > \alpha$ which gives

  \lim_{ y \to b^{-1}} f^{-1} y = a = f^{-1}(b)$$

  $\implies$ $f^{-1}$ is cont from the left. Similarly one can show that f^{-1} is continius from the right. Therefor f^{-1} is continius

The case f strictly decreasing is similar.

Since we have monotonicty it means we need to proove both that x is increasing and decraseing.


=======
## Theorem 3.17

If f is continuous on the interval [a,b] -> f(I) is bounded

### Proof

Only a one way proof as the backwards is not always true

Contradiction:

    Assume the f([a,b]) is unbounded

    Let B be  the midpoint of [a,b]

    $\implies$ one of of the sets f([a,B]), f(B,b) would be unbounded
    because otherwise f([a,b]) = f([a,b]) = f(a,d) union f([a,b]) would be bounded
    By induction, we find subintervals [a_n,b_n] of [a,b] such that (a_n) forms a sequence which is increasing (bn) is de, f([a_n, b_n]) is unbounded and b_n = a_n + \frac{ 1 }{ 2 } \left(b_{n-1} -a_[n-1] \right) 

    We infer that both sequences converge with $C = \lim_{ n \to \infty} b_m = \lim_{ n \to \infty} a_n \quad c \in [a,b]

# Since F is continuous on C there is a delta greateer than zero such that.

  f(c-\delta , c+ \delta) \intersection [a,b] \subset \left(f(c) -1, f(c) + 1\right) 
  
  since $C=\lim_{ n \to \infty} b_n \exists k \in \mathbb{N}$ s.t $|b_n -c| < \delta$ for $a > K$
  Similarly, there is $m \in \mathbb{N}$ st $\left| a_n -c\right| < \delta \forall n > M$ 
  let n > max{k,m} $\implies$ 

  $$c- \delta < a_n \leq c \leq b_n < c+d$$

  and $$f([a_n, b_n]) cf((c- \delta ) n [a,b] c (f(c) -1, f(c) + 1))$$

# Theorem 3.18

A continuous function on a closed bounded interval has a maxium and minium ie achieves its suppremium and infimium

Let [a,b] be a closed bounded inerval and f be a function on [a,b] We ust show that there are x_1 , x_2 \in [a,b] st f(x_1) = inf f([a,b]) and f(x_1) = sup(f([a,b]))

Sup:

  By thm 3.1.7 S = f(([a,b])) is bounded let M=sup S by proof of constant

  assume that f(x) neq M \forall x \in [a,b] Before g(x) = \frac{1}{m-f(x)} , x \in [a,b]

  and by theorem 3.17 g([a,b]) is bouunded

  So there is a k part of the reals such that zero is less than or equal to g(x) is less than or equal to K \in \mathbb{R} \quad \text{s.t} \quad 0 leq g(x) leq k \forall [a,b].

  Hence forall x in [a,b]

  Would be an upperbounded of f([a,b]) which contradicts the fact that m = sup f([a,b])
>>>>>>> 30f8de8bd588de7a343136dafb496a98c103c2a2

# Appendix

