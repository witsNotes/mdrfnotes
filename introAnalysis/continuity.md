---
title : "Continuity"
author : "Jezri Krinsky"
---

# Imports

# Continuity

## Definition

    3.7

Continuity is now give in terms of a delta epsilon definition.

$$\forall \epsilon > 0 \exists \delta > 0 \forall 0 \leq |x-a| < \delta \implies |f(x) - f(a)| < \epsilon $$

Subtle difference L is replaced with f(a)

This is the same but without the deleted interval

## Theorem 3.13

The following functions are all continuous

* All trig functions on thier domains
* The exponential and logarithmic function
* Absolute value function
* Polynomials
* Rationals
* Roots

### Proofs

i ii iii and vii all easily follow from limit laws others are part of tutorials.

#### vi

forall epsilon > 0 let delta = epsilon than forall

## Theorem 3.14

Let a be a real function and f be a real function definied in the neighbourhood of a. Then f is contionious at a iff for each sequence xn in the domain of f with the limit of fa =f(a) then the sequence f(x_{a}) satisfies $\lim_{n\to\infty} f(na)=f(x)$ If we can find a sequence.

If we take a sequecnce and substitute those x values into the function we map the domain onto the function. Then we will have the sequence of our function tending towards f(a)

### Proof

#### Part one forward direction

Assume f is continuous

Let $(x_{a})$ be a seq in the dom(f) with $\lim_{n\to \infty} x_{a} = a$

We must show $\lim_{} f(an) f(a)$. Hence let $\epsilon > 0$.

Since f is cont at a, there is $\delta > 0$ such that

$$\left|x-a\right| < \delta \implies \left|f(x) - f(a)\right| < \epsilon$$

Since $\lim_{n\to \infty} x_{n} = a$, there is $k \in \mathbb{R}$



## The range of continuous functions with bounded domains are bounded

    Theorem 3.17

### Proof

*Use proof by contradiction*

Assume the $f([a,b])$ is unbounded. We need to transform the function into a sequence. We do this by bisecting the interval and  replacing the upper or lower bound with the midpoint. Then we look at the sequence of upper and lower bounds. 


Then let $d$ be the midpoint of the interval.

So
$f([a,b]) = f([a,d]) \cup f([d,b])$ is unbounded $\implies$ either $f([a,d])$ or $f([d,b])$ is unbounded.

By induction we find an interval $[a_n, b_n]$ where such that $a_n$ is increasing $b_n$ is decreasing  and $f([a_n,b_n])$ is unbounded. 

$$b_n = a_n + \frac{1}{2}(b_{n-1} + a_{n-1}) = a_n + 2^{-n}(a-b)$$

So by limit laws 

$c := \lim a_n = \lim b_n$  and  $c \in [a, b]$(why)

Since $f$ is continuous at $c$  $\exists \delta > 0 \quad \text{such that} \quad (f((c-\delta, c+\delta) \cap [a,b]) \subset (f(c)-1,f(c) + 1)$

$$\exists K_0, K_1 \in \mathbb{N} \quad \text{such that} \quad \forall n > K \, |c_n - b_n| < \delta, \; \forall n > K_1 |c_n - a_n| < \delta$$

Taking $K_2 = \max{K_0,K_1} \quad \forall n > K_2 \quad c_n - \delta < a_n < c < b_{n} < c_{n} - \delta$

And so
 $$f([ a_{n} , b_{n} ]) \subset (f((c-\delta, c+\delta) \cap [a,b]) \subset (f(c)-1,f(c) + 1)$$

 Which gives $f([ a_{n}, b_{n}])$ is both bounded and unbounded.


## A function on a closed interval has a minimum and maximum

    Theorem 3.18

### Proof

Let $f$ be a function defined on the closed interval $[a,b]$ we need to show $\exists x_{0} x_{2} \in [a,b] \quad \text{such that}$

  #. $f(x_1) = \sup f([a,b])$
  #. $f(x_2) = \inf f([a,b])$

#### Part 2

By 

##### __The range of continuous functions with bounded domains are bounded__

We know that $f([a,b])$ has an superium 

*By contradiction*

Assume that $\nexists x \in [a,b] \quad \text{such that} \quad f(x) = \sup f([a,b])$ 

$$g x := \frac{1}{\sup f([a,b]) - f(x)} \, , x \in [a,b]$$

So by theorem 3.18 and 3.13 g is both continuous and bounded on the domain $[a,b]$

$$\exists K \quad \text{such that} \quad 0 < g(x) \leq K \forall x \in [a,b]$$

$$\implies \frac{1}{K} \leq \frac{1}{g(x)} = M -f(x) \\ 
\implies f(x) < M - \frac{1}{K}$$

So M there is a number smaller than M which bounds $f([a,b])$ Which is a contradiction


### If f is continuous on $[a,b]$ either $f([a,b])$ is a singleton or a closed interval

    Corollary 3.19

#### Proof

By Thoerem 3.16 to 3.18 it follows that either an singleton or an interval containing both its infinium and superiuium ie one in the form $[c,d]$

## The inverse of a continuous function with a closed interval as a range is continues on the associated range

    Theoerm 20

Inverses only exists for strictly monotonic functions.

By corollary 3.16 we now that the range must be a closed interval.

*If* $f$ is strictly increasing.

