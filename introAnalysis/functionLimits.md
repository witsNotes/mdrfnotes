---
title: "Function limits"
---

# Imports

# Function limits


## Limits

### General definition

If we can find a deleted neighbourhood around $x_0$ such that f(x) always falls in a deleted neighbourhood around $y_0$

K and epsilon are synonymous

The K and $\epsilon$ works on the range and the delta works on the domain.

#### __Limits ; Deleted neighborhood__


### $x_0 y_0 \in \mathbb{R}$

Let f be a real values function and a and L be members of the reals and assume that the domain of f contains the deleted neighbourhood of a.

$$\forall \epsilon > 0 \exists \delta > 0 \quad \text{s.t} \quad a-\delta < x < a + \delta \implies |f(x) -L | \epsilon$$

#### One sided limit

$$\forall \epsilon > 0 \exists \delta \quad \text{s.t} \quad a < x  < a+\delta \implies |f(x)-L | < \epsilon$$


### **Deleted neighborhood**

Let $a \in \mathbb{R}$ An interval in the form $(c,d)$ with $c<a<d$ is called a neighbourhood of a, and the set $(c,d)/{a}$ is called the deleted neighbourhood of a.

### $x \to \infty \vee - \infty$

#### Positive infinity

Let x be a real function defined on an $[c,\to \infty)$ or $(c,\infty)$

If l (the limit of f(x) as x approaches infinity) exists it is defined by.

$$\forall \epsilon > 0 \exists K > 0 \quad \text{s.t} \quad x > K \implies |f(x) - l | < \epsilon$$


#### Negative infinity

Let x be a real function defined on an $(-\infty ,c)$ or $(-\infty,c]$

If l (the limit of f(x) as x approaches negative infinity) exists it is defined by.

$$\forall E > 0 \exists K < 0 \quad \text{s.t} \quad x < K \implies |f(x) - l | < \epsilon$$


### $y \to \infty \vee - \infty$

This is when the limit goes to infinity or does not exist.

#### Positive infinity


Let f be a real function which includes the deleted neighborhood of a

f(x) approaches infinity $\iff$

$$\forall k>0 \exists \delta > 0 s.t a - \delta < x < a+d f(x) > k$$

#### Negative infinity

Let f be a real function which includes the deleted neighborhood of a

f(x) approaches negative infinity iff

$$\forall k<0 \exists \delta > 0 \quad \text{s.t} \quad a - \delta < x < a+d f(x) < k$$

### $y,x \to \infty \vee -\infty$

f(x) approaches infinity as x approaches infinity

$$\forall A > 0 \exists K > 0 s.t x > K \implies f(x) > A$$

For the negative case this is similar.

Infinites can also be defined using the one-sided limits.

### Limit laws

    Theorem 3.3

We can calculate limits using these laws unless "From definition" is used in the question.

Let $a, c \in \mathbb{R}$ and suppose that the real function f and g are defined in a deleted neighbourhood of a and that $\lim_{ x \to a } f(x) = L \in \mathbb{R}$  and $\lim_{ x \to a} g(x) = M \in \mathbb{R}$

 #. $\lim_{ x \to a} c = c$
 #. $\lim_{ x \to a}[f(x) + g(x)] = L + M$
 #. $\lim_{ x \to a} [f(x)-g(x)] = L-M$ 
 #. $\lim_{ x \to a} [cf(x)] = cL$ 
 #. $\lim_{ x \to a} [f(x)g(x)] = LM$
 #. ratio
    #. if $M = 0 \wedge L = 0$ use L'Hopital 
    #. if $M = 0 \wedge L \neq 0$ the limit does not exist
    #. if $M\neq 0$ $\lim_{ x \to a} \frac{ f(x) }{ g(x)} = \frac{ L }{ M }$ 
 #. $\lim_{ x \to a} x = a$ 
 #. $\forall n \in \mathbb{N}$ 
    #. $\lim_{ x \to a} x^n = a^n$
    #. $\lim_{ x \to a} \sqrt[n]{x} = \sqrt[n]{a}$ If n is even we assume a > 0
    #. $\lim_{ x \to a} \sqrt[n]{f(x)}= \sqrt[n]{L}$ 
 #. $\lim_{ x \to a} \left| f(x)\right| \implies L = 0$ 

All The limit laws

Remember to use la hoptial when on fractions when top and bottom limit is equal to one

#### Proofs of 2 6 and 7.3

other proofs are exactly the same as in chapter 2

##### 2

The sum of the limits is the limit of the sums. 

We already now that the limit of f and g exist.

let $\epsilon > 0$ then there exists $\delta_1 \wedge \delta_2$ s.t 

$|f(x) - L | < \frac{\epsilon}{2} if 0 < |x - a| < \delta_1$
$|g(x) - L | < \frac{\epsilon}{2} if 0 < |x -a| < \delta_2$

Put delta as the minimum delta between delta 1 and delta 2. So $|x-a| < \delta_1 \and |x-a| < \delta_2$ 

Therefor $|(f(x) + g(x)) - (L + M)| = |(fx - L) + (g x - M)|$

by Triangle inequality

$$\leq |f (x) - L| + | g(x) - M |$$

$$\leq \frac{epsilon}{2} + \frac{\epsilon}{2} = \epsilon$$

Conclusion

f(x) + g(x) approaches L+M as $x \to a$

##### 6

Product rule

###### Case 1

L and M are both zero

Let $\epsilon > 0 \exist \delta_1 \wedge \delta_2$ s.t

$|f(x) -L | < 1 \quad \forall |x-a| < \delta_1$

and $|g(x) -M | \< \epsilon if 0 < |x-a| < \delta_2$

Use the minimum delta 

$$|f(x)g(x) - LM| = |f(x)g(x)| = |fx|\times|gx| < 1 \times \espsilon = \epsilon$$

###### Case 2

For all functions.

We expand $f(x)g(x)$ to get terms which we can use previous laws on

$$f(x)g(x) = (f(x) -L)(g(x)-M) + L(g(x)-M) -f(x)M$$

Using 1 2 and 3 and the definition of limits we get.

$$\lim_{x \to a}f(x)g(x) = LM$$

##### 7.3

####### Case 1

$f(x)=1$

We must show

$$\left|\frac{1}{g(x)} - \frac{1}{M} \right| < \epsilon$$


Since $g(x) \neq 0$ 


## Examples

$$f(x) = \frac{1}{}$$

find

$$\lim_{n \to \infty} f(x)$$

And

$$\lim_{x \to -infty f(x)$$


Start by guessing L = 0 for both and show their is no contradiction

Let $\epsilon > 0$ and $k=\frac{1}{\epsilon} > 0$

3 Then x > K gives 

$$\left| \frac{1}{x} \right| = \frac{1}{x} < \frac{1}{k} = \epsilon$$

When ever we work with limit definitions we will always follow this four step pattern.

In a test we must rewrite all steps for negative infinity

3 $$\left| \frac{1}{x} \right| = -\frac{1}{x} < -\frac{1}{k} = \epsilon$$


