---
title: "Limits and continuity of functions"
---

# Imports

import functionLimits as fl 

import continuity as c

# Functions

##__fl:Function limits__

##__c:Continuity__

## Proof of part 2

Assume f is not cont at a. Then 
$$\exists \epsilon > 0 \forall \delta > 0, \exists x \in dom(f) , |x-a| < \delta$$

and $\left| f(x) - f(a)\right| \geq \epsilon$

In particular, for $\delta = \frac{1}{n}$

This producers a contradiction therefor the function f is continius on a.

## Tutorial 3.4.1

## Intermediate value theorem


If we have a function f. If we pick a value a and a value b so that a is not equal to b and we look at the interval a b. If the value y = k goes through f(a) and f(b) than y goes through both values. There must exist a value c in (a,b) such that f(c) =k

### Proof

Create a new function g(x) = f(x) - k so we now have values greater than or less than zero.

g(a) g(b) < 0 as only one is positive. 
Then keep bisecting the interval and use new intervals until we get to zero.

If the process does not decrease $b_{n}$ decrease $a_{n}$ increases

<<<<<<< HEAD
Both increasing and decreasing sequences tend to c. It follows that the 

$$limt of g(c)^{2} = limg(a) \lim_{b} \leq 0$$
=======
Both increasing and decreasing sequences tend to c. It follows that the limt of $$g(c)^{2} = limg(a) \lim_{b} \leq 0$$
>>>>>>> 30f8de8bd588de7a343136dafb496a98c103c2a2

As a sqaure can never be negative the only solution is that the limit is zero.


Suppose there is a close interval


## Test question

Prove sin x is continuous in $x \in \mathbb{R}$

Let $\epsilon > 0$ $f\left|f(x) - f(a)\right| < \delta$

$$\left|sin(x) - sin(a)\right| = \left|2cos\frac{x+a}{2} sin\frac{x-a}{2}\right|$$

Since $\left|\right|$
How do we prove this.

<<<<<<< HEAD
## When is a set an

Closed is resrved by a functions as endpoint.


## Theorem 3.18

A continius function on a closed interval achievs its infimium and suppremuim.

## Theorem 3.19

If f is contiunius on a closed interval than eithier f(x) is a single value or a closed interval

### Proove

Use cor 3.16 and Thm 3.17 with 3.18 it follows tha f([a,b]) it follows that either a singlton or a bounded interval which contains both its inf and its sup. But such an interval is of the form [c.d] with c<d. The supremuim and infiumium condition ensures a inteerval not a supremium.

Let f be function and i be an interval and i be a function which is the inverse if f is strictly monotonic continius function. Then the inverse of the function exists and is continius.

#### Proof

by cor 3.16 f(i) is an inteval

Start with let x be increasing.

then also f inverse is strictly increasing.

Let b be an element of f of i. ie b = f(a) for some a in i

The domain of a function is the range of the inverse and the range of a.

If a is not the left endpoint of i then b is not the left end point of b(a) and for y in f(i) with y < b = f(a) we have f^{-1} we have f^{-1} f(y) < f^{-1}(f(a)) = a

Therefor f inverse is bounded above and increasing on f(i) intersectino with minus infinity b and thus alpha $\lim_{ y \to b^-} f^{-1}(y)$ 

We know this limit exists and alpha \leq f(-1)(b) = a, see Thm 3.8

Assume that alpha is less than a. Since a in I is not the left end point of I , $I\intersection (\alpha,a) \neq \phi.$ Let $x_1 \in I \intersection (\alpha,a) 

Then f(x_1 < f(a) = b ) $\implies$
  $x_1 = f^{-1} (f(x_0)) \leq \alpha.

  This gives the contradciton. $x_0 > \alpha$ which gives

  \lim_{ y \to b^{-1}} f^{-1} y = a = f^{-1}(b)$$

  $\implies$ $f^{-1}$ is cont from the left. Similarly one can show that f^{-1} is continius from the right. Therefor f^{-1} is continius

The case f strictly decreasing is similar.

Since we have monotonicty it means we need to proove both that x is increasing and decraseing.


=======
## Theorem 3.17

If f is continuous on the interval [a,b] -> f(I) is bounded

### Proof

Only a one way proof as the backwards is not always true

Contradiction:

    Assume the f([a,b]) is unbounded

    Let B be  the midpoint of [a,b]

    $\implies$ one of of the sets f([a,B]), f(B,b) would be unbounded
    because otherwise f([a,b]) = f([a,b]) = f(a,d) union f([a,b]) would be bounded
    By induction, we find subintervals [a_n,b_n] of [a,b] such that (a_n) forms a sequence which is increasing (bn) is de, f([a_n, b_n]) is unbounded and b_n = a_n + \frac{ 1 }{ 2 } \left(b_{n-1} -a_[n-1] \right) 

    We infer that both sequences converge with $C = \lim_{ n \to \infty} b_m = \lim_{ n \to \infty} a_n \quad c \in [a,b]

# Since F is continuous on C there is a delta greateer than zero such that.

  f(c-\delta , c+ \delta) \intersection [a,b] \subset \left(f(c) -1, f(c) + 1\right) 
  
  since $C=\lim_{ n \to \infty} b_n \exists k \in \mathbb{N}$ s.t $|b_n -c| < \delta$ for $a > K$
  Similarly, there is $m \in \mathbb{N}$ st $\left| a_n -c\right| < \delta \forall n > M$ 
  let n > max{k,m} $\implies$ 

  $$c- \delta < a_n \leq c \leq b_n < c+d$$

  and $$f([a_n, b_n]) cf((c- \delta ) n [a,b] c (f(c) -1, f(c) + 1))$$

# Theorem 3.18

A continuous function on a closed bounded interval has a maxium and minium ie achieves its suppremium and infimium

Let [a,b] be a closed bounded inerval and f be a function on [a,b] We ust show that there are x_1 , x_2 \in [a,b] st f(x_1) = inf f([a,b]) and f(x_1) = sup(f([a,b]))

Sup:

  By thm 3.1.7 S = f(([a,b])) is bounded let M=sup S by proof of constant

  assume that f(x) neq M \forall x \in [a,b] Before g(x) = \frac{1}{m-f(x)} , x \in [a,b]

  and by theorem 3.17 g([a,b]) is bouunded

  So there is a k part of the reals such that zero is less than or equal to g(x) is less than or equal to K \in \mathbb{R} \quad \text{s.t} \quad 0 leq g(x) leq k \forall [a,b].

  Hence forall x in [a,b]

  Would be an upperbounded of f([a,b]) which contradicts the fact that m = sup f([a,b])
>>>>>>> 30f8de8bd588de7a343136dafb496a98c103c2a2
