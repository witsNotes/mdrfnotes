---
title: "Series definitions"
---

# Series definitions

## Series

A series is defined as:
  $$\sum_{i=n_0}^{\infty}a_i \quad \text{s.t} \quad a_n \in \mathbb{R} \forall i \geq i_0 \in \mathbb{Z}$$

### Syntactic sugar

This symbol is used by both to show the series and the sum of the series if the series converges. 

## Partial sum

The number $s_n = \sum_{i=i_0}^{n} a_n$ is called the nth partial sum of the series.

## Convergence

A series converges if the sequence of its partial sums converge. 

### Corollary

A series $\sum_{i=i_0}^{\infty}a_i$ converges $\iff$

$$\forall \epsilon > 0 \quad \exists K \in \mathbb{N} \quad \text{s.t} \quad \forall m \geq k \geq K \quad \left|\sum_{i=k}^{m} a_i \right|< \epsilon$$

#### Proof

\begin{align*}
    \sum_{i=n_0}^{\infty} \quad \text{con} \quad &\iff \{s_n\}_{n=n_0}^{\infty} \quad \text{con} \quad \text{where $s_n$ is the nth partial sum of the series}\\

    &\iff \forall \epsilon > 0 \exists K \quad \text{s.t} \quad \forall m \geq k \geq K \quad \left|s_m - s_k\right| < \epsilon\quad \text{Theorem 2.12} \\
    &\iff \forall \epsilon > 0 \exists K \quad \text{s.t} \quad \forall m \geq k \geq K \quad \left|\sum_{i=k}^{m}\right| < \epsilon
    \end{align*}

## Sum of the series

If

$$\lim_{n \to \infty} = \sum_{i={i_0}}^{\infty} a_i = L \quad | L \in \mathbb{R}$$

Then L is called the sum of the series.

Otherwise the limit of the sum of the series does not exists.

## Divergence

Any sequence which does not converge is said to diverge.

## Geometric sequence

A sequence of the form

$$\sum_{i=i_0}^{\infty} ar^i \quad | a \neq 0 \wedge r \in \mathbb{R}$$ 


## Alternating series 

The alternating series is a series of the form.

$$\sum _{i=1}^{\infty} (-1)^ib_i \quad \text{or} \quad \sum_{i=1}^{\infty} (-1)^{i-1}b_i \quad |b_n \geq 0$$

## Absolute convergence 

Given a series

$$\sum_{i=i_0}^\infty a_i$$ The series converges absolutely if

$$\sum_{n=n_0}^{\infty}\left| a_i\right| $$ Converges

## Conditional convergent

If a series is convergent but not absolutely convergent then it is conditionally convergent.

Conditionally convergent series can be made to equal anything one wants, if one picks out the elements of the sum in correct order (Possibly add proof)

## Power series

A series in the form
$$\sum c_i (x-a)^i \quad \text{where} \quad x ,a (c_i | i \in \mathbb{N}) \in \mathbb{R}$$ is the power series centered around a( also a series about a, or a series in (x-a))
