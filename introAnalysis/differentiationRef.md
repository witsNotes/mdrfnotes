---
title: "Differentiation"
---

# Imports

import functions as f

import functionLimits as fl

# Differentiation

>Note : This is note itself examinable but other dependent things are. Check with new

\begin{align*}
A \subset R \quad \text{s.t}& \quad \forall a \in A \\
&\exists \epsilon > 0 \quad \text{s.t} \quad (a-\epsilon, a] \subset A \wedge [a,a+\epsilon) \subset A \iff (a-\epsilon, a+\epsilon) \subset A 
\end{align*}


## Definition

Let $f:A \to \mathbb{R} \wedge a \in A$ 

f is called differentiable at a if the derivative f'(a)

$$\exists f'(a) (\text{the derivative of f at a }) := \lim_{ x \to a} \frac{ f(x) - f(a) }{ x-a }$$

## Differentiability implies continuity (Theorem 4.1)

Let $f : A \to \mathbb{R} \wedge a \in A$. If f is differentiable at a then f is continuous at a

### Proof

\begin{align*}
&f(x) = f(a) + \frac{ f(x) - f(a) }{ x-a} (x-a) \\
\iff& \lim_{ x \to a} f(x) = f(a) + \frac{ f(x) -f(a) }{x-a} = f(a) + [f'(a)](0) = f(a)
\end{align*}

Using 

>### Limit laws
>
>    Theorem 3.3
>
>We can calculate limits using these laws unless "From definition" is used in the question.
>
>Let $a, c \in \mathbb{R}$ and suppose that the real function f and g are defined in a deleted neighbourhood of a and that $\lim_{ x \to a } f(x) = L \in \mathbb{R}$  and $\lim_{ x \to a} g(x) = M \in \mathbb{R}$
>
> #. $\lim_{ x \to a} c = c$
> #. $\lim_{ x \to a}[f(x) + g(x)] = L + M$
> #. $\lim_{ x \to a} [f(x)-g(x)] = L-M$ 
> #. $\lim_{ x \to a} [cf(x)] = cL$ 
> #. $\lim_{ x \to a} [f(x)g(x)] = LM$
> #. ratio
>    #. if $M = 0 \wedge L = 0$ use L'Hopital 
>    #. if $M = 0 \wedge L \neq 0$ the limit does not exist
>    #. if $M\neq 0$ $\lim_{ x \to a} \frac{ f(x) }{ g(x)} = \frac{ L }{ M }$ 
> #. $\lim_{ x \to a} x = a$ 
> #. $\forall n \in \mathbb{N}$ 
>    #. $\lim_{ x \to a} x^n = a^n$
>    #. $\lim_{ x \to a} \sqrt[n]{x} = \sqrt[n]{a}$ If n is even we assume a > 0
>    #. $\lim_{ x \to a} \sqrt[n]{f(x)}= \sqrt[n]{L}$ 
> #. $\lim_{ x \to a} \left| f(x)\right| \implies L = 0$ 
>
>All The limit laws
>
>Remember to use la hoptial when on fractions when top and bottom limit is equal to one
>
>#### Proofs of 2 6 and 7.3
>
>other proofs are exactly the same as in chapter 2
>
>##### 2
>
>The sum of the limits is the limit of the sums. 
>
>We already now that the limit of f and g exist.
>
>let $\epsilon > 0$ then there exists $\delta_1 \wedge \delta_2$ s.t 
>
>$|f(x) - L | < \frac{\epsilon}{2} if 0 < |x - a| < \delta_1$
>$|g(x) - L | < \frac{\epsilon}{2} if 0 < |x -a| < \delta_2$
>
>Put delta as the minimum delta between delta 1 and delta 2. So $|x-a| < \delta_1 \and |x-a| < \delta_2$ 
>
>Therefor $|(f(x) + g(x)) - (L + M)| = |(fx - L) + (g x - M)|$
>
>by Triangle inequality
>
>$$\leq |f (x) - L| + | g(x) - M |$$
>
>$$\leq \frac{epsilon}{2} + \frac{\epsilon}{2} = \epsilon$$
>
>Conclusion
>
>f(x) + g(x) approaches L+M as $x \to a$
>
>##### 6
>
>Product rule
>
>###### Case 1
>
>L and M are both zero
>
>Let $\epsilon > 0 \exist \delta_1 \wedge \delta_2$ s.t
>
>$|f(x) -L | < 1 \quad \forall |x-a| < \delta_1$
>
>and $|g(x) -M | \< \epsilon if 0 < |x-a| < \delta_2$
>
>Use the minimum delta 
>
>$$|f(x)g(x) - LM| = |f(x)g(x)| = |fx|\times|gx| < 1 \times \espsilon = \epsilon$$
>
>###### Case 2
>
>For all functions.
>
>We expand $f(x)g(x)$ to get terms which we can use previous laws on
>
>$$f(x)g(x) = (f(x) -L)(g(x)-M) + L(g(x)-M) -f(x)M$$
>
>Using 1 2 and 3 and the definition of limits we get.
>
>$$\lim_{x \to a}f(x)g(x) = LM$$
>
>##### 7.3
>
>####### Case 1
>
>$f(x)=1$
>
>We must show
>
>$$\left|\frac{1}{g(x)} - \frac{1}{M} \right| < \epsilon$$
>
>
>Since $g(x) \neq 0$ 
>
>

### Inverse

The inverse statement is false and the proof is to find a simple counter example. Such as the absolute value function.

## Derivative laws

    Theorem 4.2

Let $f,g : A \to R, a \in A,$  f and g differentiable as a and $c \in \mathbb{R}$. Then.

  #.Linearity of derivatives
      #. 
Bunch of limit laws

Sum is continuum

Proof of limit rules.


### Proof of the product rule.


$f \dot g$


# Appendix

