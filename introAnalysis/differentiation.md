---
title: "Differentiation"
---

# Imports

import functions as f

import functionLimits as fl

# Differentiation

>Note : This is note itself examinable but other dependent things are. Check with new

\begin{align*}
A \subset R \quad \text{s.t}& \quad \forall a \in A \\
&\exists \epsilon > 0 \quad \text{s.t} \quad (a-\epsilon, a] \subset A \wedge [a,a+\epsilon) \subset A \iff (a-\epsilon, a+\epsilon) \subset A 
\end{align*}


## Definition

Let $f:A \to \mathbb{R} \wedge a \in A$ 

f is called differentiable at a if the derivative f'(a)

$$\exists f'(a) (\text{the derivative of f at a }) := \lim_{ x \to a} \frac{ f(x) - f(a) }{ x-a }$$

## Differentiability implies continuity (Theorem 4.1)

Let $f : A \to \mathbb{R} \wedge a \in A$. If f is differentiable at a then f is continuous at a

### Proof

\begin{align*}
&f(x) = f(a) + \frac{ f(x) - f(a) }{ x-a} (x-a) \\
\iff& \lim_{ x \to a} f(x) = f(a) + \frac{ f(x) -f(a) }{x-a} = f(a) + [f'(a)](0) = f(a)
\end{align*}

Using 

#### __ fl:Limit laws __

### Inverse

The inverse statement is false and the proof is to find a simple counter example. Such as the absolute value function.

## Derivative laws

    Theorem 4.2

Let $f,g : A \to R, a \in A,$  f and g differentiable as a and $c \in \mathbb{R}$. Then.

  #.Linearity of derivatives
      #. 
Bunch of limit laws

Sum is continuum

Proof of limit rules.


### Proof of the product rule.


$f \dot g$

