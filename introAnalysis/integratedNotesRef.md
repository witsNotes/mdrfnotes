---
title: " Integrated Notes"
author : "Jezri Krinsky "
startTime: "26 August 2018"
---
	
# Imports

import reals as r

import series as s

import functions as f


import sequences as seq

import differentiation as dif

# Integrated Notes

># Reals
>
>todo

># Sequences
>
>todo
>
>

># Series
>
> >## Series
>>
>>A series is defined as:
>>  $$\sum_{i=n_0}^{\infty}a_i \quad \text{s.t} \quad a_n \in \mathbb{R} \forall i \geq i_0 \in \mathbb{Z}$$
>>
> >### Syntactic sugar
>>
>>This symbol is used by both to show the series and the sum of the series if the series converges. 
>>
>
> ># Series tests
> >## Divergence test
>>
>>If $\nexists \lim_{n \to \infty} a_n \vee \lim_{n\to \infty}a_n \neq 0$ Then the series diverges
>>
>>
> >### Proof
>>
> >#### Contrapositive
>>
>>Let $s_n = a_1 + \dots + a_n$
>>
>>Solve for a term of the sequence in terms of the series.
>>Than $a_n = s_n - s_{n-1} = 0$
>>
>>Since the series converges the sum of the series exists. Also n+1 approaches infinity as n approaches Infinity. So the limit of $s_{n-1}$ is also the sum of the series.
>>
> >## Convergence tests
>>
> >### Geometric test
>>
>>Given a geometric series the series converges if $|r| < 1$ and diverges if $|r| > 1$ and other tests need to be used if $|r| = 1$ 
>>
>>
> >#### Proof
>>
>>$$s_n = a + ar + ar^2 + \dots + ar^n$$
>>
>>$$r s_n = ar + ar^2 + \cdots + ar^n + ar^{n+1}$$
>>
>>subtracting one from the other we get
>>
>>$$s_n - rs_n = a - ar^{n+1}$$
>>
>>
>>$$s_n = \frac{a(1-r^{n+1})}{1+r}$$
>>
>>and so by theorem 2.5 $s_n$
>>and thus the geometric series converges if |r| <1, with
>>
>>$$\sum _{n=0}^{\infty} a r^n = \lim_{n \to \infty} s_n = \frac{a}{1-r}$$
>> and diverges if |r| > 1 or r = -1. Finally, for r = 1,$s_n = a(n+1), and thus s_n = a(n+1) and thus $s_n \to \infty$ as $n \to \infty$
>>
> >#### Examples
>>
>>To do still
>>
>>
> >### Comparison test
>>
>>Take the series A and B with non-negative terms where $a_{i} \leq b_i \; \forall i \in I$
>>
>>If B converges than A converges
>>
>>The contrapositive is also often useful
>>
> >#### Note on alignment
>>
>>Needs formalization.
>>
>>The exact alignment of the series is arbitrary we could say $a_i < b_{20+1}$ for example and be fine. Because no finite number of terms will affect the convergence/divergence of a sequence.
>>
> >#### Proof
>>
>>Let $s_k$ be the $k^{th}$ sum of A and $t_k$ be the $k^{th}$ sum of B. So both $\{s_n\} \; \{t_n\}$ are increasing sequences and $\forall k \; s_k \leq t_k$
>>
>>$\implies$ If B converges $t_k \leq M \forall k \in \mathbb{N}$(t is bounded), and $s_k \leq t_k \leq M \forall k \in \mathbb{N}$
>>
>>So ${s_n}$ is bounded and therefor converges by
>>
> > >#### Series convergence $\iff$ partial sums are bounded
>>>
>>>\begin{align*}
>>>a_i \geq 0 \forall i \in \mathbb{N}^* \wedge s_n = \sum_{i=i_0}^{n}{a_i}
>>>\\
>>>\implies \sum_{i=i_0}^{\infty}{i_n} \; \text{con} \iff s_n \; \text{is bounded}
>>>\end{align*}
>>>
> > >##### Proof
>>>
>>>$s_{n+1} = s_n + a_{n+1} \geq s_n$ So $\{s\}_{n=0}^{\infty}$ is an increasing sequence. By theorem 2.10 this sequence and hence the series converges $\iff$ the sequence $\{s_n\}$ is bounded.
>>>
>>>
>>>
>>
> >#### Alternative proof
>>
>>From theorem 5.4(Basic sum laws) Both $\sum |a_n|$ and $\sum 2|a_n|$ converge.
>>
>>$$0\leq a_n + |a_n| \leq 2|a_n|$$
>>
>>So by comparison test
>>
>>$$\sum a_n + |a_n| $$
>>
>>Converges
>>
>>$$\sum a_n = \sum a_n + |a_n| - \sum |a_n|$$
>>
>>So again by theorem 5.4 it converges.
>>
>>$0 \leq an + |an| \leq 2|an|$
>>
>>
> >### Alternating series
>>
>>If an alternating series satisfies
>>
>>
>>i) $b_n$ is decreasing (not strictly) 
>>
>>ii) $\lim_{n \to \infty} b_n = 0$
>>
>>Then the series converges
>>
> >#### Proof
>>
>>We are trying to show
>>
>>
>>$$\forall \epsilon \exists K \quad \text{s.t} \quad \forall m \geq k \geq K \quad \left| \sum_{i=k}^{m} (-1)^i b_i \right| < \epsilon$$
>>
>>Start by noting because $\{b_n\}$ is decreasing and positive.
>>
>>$$\lim_{i\to \infty}b_i =0 \implies \forall \epsilon \exists K \quad \text{s.t} \quad \forall \, k > K \quad b_k \leq b_K < \epsilon$$
>>
>>We now show that 
>>$$\left| \sum_{i=k}^{m} (-1)^i b_i \right| \leq b_k \forall k$$
>>
>>This is done for two case
>>
>>  #. If $m-k$ is even
>>      Replace $m$ with $k+2l$
>>
>>      $$(-1)^k \sum_{i=k}^{k+2l} (-1)^i b_i = \left(\sum_{i=0}^{l-1}(b_{k+2i} - b_{k+2i+1})\right) + b_{k+2m} = b_{k} - \sum_{i=0}^{l-1}(b_{k+2i +1} - b_{k+2i+2})$$
>>
>>      Each bracketed term is greater than or equal to zero. So they can all be dropped to get inequalities.
>>
>>      $$0 \leq b_{k+2l} \leq (-1)^k \sum_{n=k}^{k+2m} (-1)^n b_n \leq b_{k}$$
>>
>> #. If m-k is odd
>>      Replace $m$ with $k+2l +1$
>>      $$(-1)^k \sum_{i=k}^{k+2l + 1} (-1)^i b_i = b_i - \sum_{i=0}^{l-1}(b_{k+2i +1} - b_{k+2i+2}) - b_{k+2i+1}$$
>>
>>
>>      $$0 \leq (-1)^k \sum_{n=k}^{k+2l+1} (-1)^n b_n \leq b_{k} - b_{k+2l+1} \leq b_k$$
>>
>>Finally transitivity gives
>>
>>$$\forall \epsilon \exists K \quad \text{s.t} \quad \forall m \geq k \geq K \quad \left| \sum_{i=k}^{m} (-1)^i b_i \right| \leq b_k \leq b_K < \epsilon$$
>>
>>
> >#### Example
>>
>>The alternating series 
>>$$\sum_{n=1}{\infty} (-1)^{n-1} \frac{3n}{4n-1}$$ Does not converges since the limit of $\frac{3}{n {4n -1}}$ does not converge to zero
>>
> >### Ratio test
>>
> >#### Result
>>
>>Let A be a series with terms $a_i$ 
>>\begin{align*}
>>  \lim \sup \left|\frac{a_{i+1}}{a_i}\right| &= L \\
>>  \lim \inf \left|\frac{a_{i+1}}{a_i}\right| &= l
>>\end{align*}
>>
>>If $a_n \neq 0 \; \forall n$ then
>>
>>1. If $L < 1$ then the series converges absolutely
>>
>>2. If $l > 1$ than the series diverges.
>>
>>
> >#### Proof
>>
>>
> >##### Part 1
>>
>>This is done by a comparison with a convergent geometric series B where $b_j = b_0 r^j$
>>
>>B contains $r^{j}$ so in order to show $a_i < b_j$ write $a_i$ as a product with $j$ terms that are each less than $r$ and an term to math $b_0$.
>>  
>>Hence for $i > k$
>>
>>$$|a_m| = |a_k|  \prod_{i=k}^{m-1} \left| \frac{a_{i+1}}{a_i}\right|$$
>>
>>Now we just need to pick k large enough that our information about the behavior of the ratio as i goes to infinity to be useful. Set $r = L + \epsilon | \epsilon > 0 , \, r <1$. Which gives us $\exists r \in \mathbb{R}$(how? find chapter 1) and $r > L$
>>
>>$$\forall r \, \exists \, K \in \mathbb{N} \quad \text{s.t} \quad \forall i \geq K \; \left| \frac{a_{i+1}}{a_i} \right| < r$$
>>
>>So we just need $k \geq K$ and $j = m-k$($(m-1) -(k-1)$ inclusive of $k^{th}$ term) and we have $a_i < b_j \forall i > k \geq K$ and B converges. So A converges.
>>
>>
> >##### Part 2
>>
>>We use
>>
> > >## Divergence test
>>>
>>>If $\nexists \lim_{n \to \infty} a_n \vee \lim_{n\to \infty}a_n \neq 0$ Then the series diverges
>>>
>>>
> > >### Proof
>>>
> > >#### Contrapositive
>>>
>>>Let $s_n = a_1 + \dots + a_n$
>>>
>>>Solve for a term of the sequence in terms of the series.
>>>Than $a_n = s_n - s_{n-1} = 0$
>>>
>>>Since the series converges the sum of the series exists. Also n+1 approaches infinity as n approaches Infinity. So the limit of $s_{n-1}$ is also the sum of the series.
>>>
>>
>>and show that the sequence $\{a_i\}$ does not converge to zero. Right each $a_i$ as a product of ratios so we can use our information about the ratios.
>>
>>$$|a_m| = |a_k|  \prod_{i=k}^{m-1} \left| \frac{a_{i+1}}{a_i}\right|$$
>>
>>Now put a lower bound on i so limiting conditions apply.
>>
>>Let $l' \in (1,l)$
>>$$\implies \exists K \in \mathbb{N} | \left( \left|\frac{a_{i+1}}{a_i}\right| > l' \forall i \geq K\right)$$ 
>>Hence for $m > K$
>>$$|a_m| = |a_K|  \prod_{i=K}^{m-1} \left| \frac{a_{i+1}}{a_i}\right| > |a_K|$$
>>
>>so that $a_n \not\to 0$ as $n\to \infty$ Hence the sequence diverges by the test for divergence. 
>>
> >### Root test
>>
>>* If $\lim \sup |\sqrt[n]{a_n}|<1$ The series converges (Copied verbatim should one of these be infimium)
>>* If $\lim \inf |\sqrt[n]{a_n}|>1$ The series diverges
>>
> >#### Proof
>>
>>We are going to us a comparison test with  a geometric series $B (b_j = r^j | r < 1) $to show that the series converges absolutely and therefor converges.
>>
>>Set $r = L + \epsilon | \epsilon > 0 , r <1$
>>
>>$\implies \exists K \in \mathbb{N}| \root[i]{a_i} < r \quad \forall i \geq K$
>>
>>Since $\sum (r)^n$ is a convergent geometric series, it follows from the comparison test that the series $a_n$ starting at K converges absolutely and so will the series starting at $a_0.$
>>as an converges absolutely.
>>
>>Let L' = (1,L) .Then $\forall K \in \mathbb{N} \exists m \geq k \quad \text{s.t } \quad \root{m}{|a_n|} > L' 
>>
>>$$$\left| a_i \right| > L'^{m} > 1$$
>>
>>So $a_i$ Does not converge to zero
>>
> >## Power series
>>
>>This test differs from the others as here we give a range of a parameter x for which the series converges and diverges.
>>
>>If we have a series in the form
>>
> > >## Power series
>>>
>>>A series in the form
>>>$$\sum c_i (x-a)^i \quad \text{where} \quad x ,a (c_i | i \in \mathbb{N}) \in \mathbb{R}$$ is the power series centered around a( also a series about a, or a series in (x-a))
>>
> >### Radius of convergence
>>
>>For each power series the is a number (R) such that for all x where $|x-a| < R$ the series converges. R is called the radius of convergence.
>>
>> #. $R=0$ if $\lim_{i \to \infty} \sqrt[i]{|c_n|}= \infty$
>> #. $R= \frac{1}{\lim_{i\to \infty} sup\sqrt[i]{|c_n|}}$ if $0 \leq \lim_{i \to \infty  }sup \sqrt[i]{|c_n|} \in R$
>> #. $R=\infty$ if $\lim_{i\to \infty} sup\sqrt[i]{|c_n|}= 0$
>>
> >### Domains for power series
>>
>>    Thoerem 5.12
>>
>>  #. If $R=0$ then the series converges only at $x=a$
>>  #. If $R= \infty$ the series converges for all the reals
>>  #. If $0< R\in \mathbb{R}$ then the series converges for $|x-a| < R$ and diverges for $|x-a| > R$ and must be checked at each boundry (Called the interval of convergence)
>>
> >#### Proof
>>
> >##### Part 1
>>
>>If $\lim sup \sqrt[n]{|c_n|} = \infty$ then forall $x\neq a$ $\lim_{i \to \infty} sup \sqrt[i]{|c_n||x-a|^n } = \sqrt[i]{|c_i|}x-a|= \infty$ 
>>
>>So by
>>
> > >### Root test
>>>
>>>* If $\lim \sup |\sqrt[n]{a_n}|<1$ The series converges (Copied verbatim should one of these be infimium)
>>>* If $\lim \inf |\sqrt[n]{a_n}|>1$ The series diverges
>>>
> > >#### Proof
>>>
>>>We are going to us a comparison test with  a geometric series $B (b_j = r^j | r < 1) $to show that the series converges absolutely and therefor converges.
>>>
>>>Set $r = L + \epsilon | \epsilon > 0 , r <1$
>>>
>>>$\implies \exists K \in \mathbb{N}| \root[i]{a_i} < r \quad \forall i \geq K$
>>>
>>>Since $\sum (r)^n$ is a convergent geometric series, it follows from the comparison test that the series $a_n$ starting at K converges absolutely and so will the series starting at $a_0.$
>>>as an converges absolutely.
>>>
>>>Let L' = (1,L) .Then $\forall K \in \mathbb{N} \exists m \geq k \quad \text{s.t } \quad \root{m}{|a_n|} > L' 
>>>
>>>$$$\left| a_i \right| > L'^{m} > 1$$
>>>
>>>So $a_i$ Does not converge to zero
>>>
>>
>>The series converges if x is not equal to a.
>>
> >##### Part 2
>>
>>If $\lim_{i \to \infty} sup \sqrt[i]{|c_i|} = r \in \mathbb{R}$
>>
>>$$\lim_{i \to \infty} sup \sqrt[i]{|c_i(x-a)^n|} = |x-a| \lim_{i \to \infty} sup \sqrt[i]{|c_i|}$$
>>
>>So if $\lim_{i \to \infty} \sqrt[i]{|c_i|} = 0$ Then the series converges by the root test.
>>
> >##### Part 3
>>
>>So if $\lim_{i \to \infty} \sqrt[i]{|c_i|} > 0 \in R$ Then the series converges if $|x-a| \lim_{i \to \infty} sup \sqrt[i]{|c_i|}$ by the root test.
>>
>>So the series converges if
>>
>>$|x-a| < \frac{1}{\lim_{i \to \infty} sup \sqrt[i]{|c_i|}}$ and diverges for $|x-a| > \frac{1}{\lim_{i \to \infty} sup \sqrt[i]{|c_i|}}$ 
>>
>>rooth of abs Cn \in Reals,then 
>>
>>lim sup root abs Cn(n-1)^n = |x-a|lim sup nthrooth |Cn|
>>
>>Thus the powerseries con if forall.
>>
>>
>>If finally $< lim sup _{n\to infty{ \in R} then by the root theorem test the series conveges if$
>>
>>$|x-a| lim sup nth rooth of abs cn < 1$
>>$|x-a| < 1/limsup nthroot abs Cn$
>>
>>and diverges if 
>>
>>$|x-a| > rhs.$
>>
> >#### Calculate ratio of convegence.
>>
>>Use the formula the limit an / an+1( Do not need proof)
>>
>>
> >##### Example 5.1 a
>>
>>* $\sum \frac{(2n)^n}{n}$
>>
>>$= 2^n /n * x ^n$
>>
>>Calculate ratio of convergence
>>
>>R = \lim_{n \to \infty} |an/an+1| = lim |a^n / n div 2^n+1/n+1| = lim |1/2 +1/2n| = 1/2$
>>
>>converges on (-\frac{1}{2}, \frac{1}{2})
>>
>>
>>Convergent by alternating series test.
>>
>>if n = \frac{1}{2} then 
>>
>>$it = 1/n$ diverges$
>>
>>Div
>>
>>R = [-\frac{1}{2} , \frac{1}{2})
>>
> >### 
> >## Tutorial 5.2.1
>>
>> 4. Prove weather the following sequences converge or not
>>    #.$$\sum_{n=1}^{\infty}\left(\frac{(-1)^{n}n}{2n+1}\right)^{2n}$$
>>      Try the root test 
>>      \begin{align*}
>>      &\lim_{n\to\infty}\sqrt[n]{\left(\left|\frac{-1^{n}n}{2n+1}^{2n}}\right|\right)}
>>      =&\lim_{n\to\infty}\left(\left|\frac{-1^{n}n}{2n+1}^{2}\right|\right)
>>      =&\lim_{n\to\infty}\left(\left|\frac{-1^{n}n}{2n+1}^{2}\right|\right)
>>      =&\left(\frac{1}{4}\right) \leq 1
>>      \end{align*}
>>    #.$$\sum_{n=0}^{\infty}\frac{n!2^{n}}{(2n!)}$$
>>      Try ratio test
>>      \begin{align*}
>>      &\left|\frac{\frac{(n+1)!2^{n+1}}n+1}{(2(n+1))!}}{}\right|
>>      \end{align*}
>> msple sadf Correct 
>> 
>>This test differs from the rest in that here we find the range of a parameter x for which a series converges.
>>
>>rooth of abs Cn \in Reals,then 
>>
>>lim sup root abs Cn(n-1)^n = |x-a|lim sup nthrooth |Cn|
>>
>>Thus the powerseries con if forall.
>>
>>
>>If finally $< lim sup _{n\to infty{ \in R} then by the root theorem test the series conveges if$
>>
>>$|x-a| lim sup nth rooth of abs cn < 1$
>>$|x-a| < 1/limsup nthroot abs Cn$
>>
>>and diverges if 
>>
>>$|x-a| > rhs.$
>>
> >#### Calculate ratio of convegence.
>>
>>Use the formula the limit an / an+1( Do not need proof)
>>
>>
> >##### Example 5.1 a
>>
>>* $\sum \frac{(2n)^n}{n}$
>>
>>$= 2^n /n * x ^n$
>>
>>Calculate ratio of convergence
>>
>>R = \lim_{n \to \infty} |an/an+1| = lim |a^n / n div 2^n+1/n+1| = lim |1/2 +1/2n| = 1/2$
>>
>>converges on (-\frac{1}{2}, \frac{1}{2})
>>
>>
>>Convergent by alternating series test.
>>
>>if n = \frac{1}{2} then 
>>
>>$it = 1/n$ diverges$
>>
>>Div
>>
>>R = [-\frac{1}{2} , \frac{1}{2})
>>
> >### 
> >## Tutorial 5.2.1
>>
>> 4. Prove weather the following sequences converge or not
>>    #.$$\sum_{n=1}^{\infty}\left(\frac{(-1)^{n}n}{2n+1}\right)^{2n}$$
>>      Try the root test 
>>      \begin{align*}
>>      &\lim_{n\to\infty}\sqrt[n]{\left(\left|\frac{-1^{n}n}{2n+1}^{2n}}\right|\right)}
>>      =&\lim_{n\to\infty}\left(\left|\frac{-1^{n}n}{2n+1}^{2}\right|\right)
>>      =&\lim_{n\to\infty}\left(\left|\frac{-1^{n}n}{2n+1}^{2}\right|\right)
>>      =&\left(\frac{1}{4}\right) \leq 1
>>      \end{align*}
>>    #.$$\sum_{n=0}^{\infty}\frac{n!2^{n}}{(2n!)}$$
>>      Try ratio test
>>      \begin{align*}
>>      &\left|\frac{\frac{(n+1)!2^{n+1}}n+1}{(2(n+1))!}}{}\right|
>>      \end{align*}
>> msple sadf Correct 
>> 
>>
> >#### Warning
>>
>>Do not confuse this proof with the ratio proof!
>>
> >### General advise.
>>
>>If it looks divergent check this first. 
>>
>>Otherwise geometric test first.
>>
>>For $\ln{n}$ use a comparison test.
>## Todo
>
>* fix ratio test
>* Add proof that conditional convergent series can be made to equal anything if terms are ordered correctly.
>* Proof for absolute value test
>* Add examples
>
>## Irrelevant insights
>
>For the whole of limits we often use a $\forall \epsilon$ statement. But what we need to know is that the series is true for arbitrarily small epsilons(if the series approaches a real number) so if we must limit the upper size of epsilon that is fine.
>
>## Notation
>
>Throughout this section we will use i as out general term in the sequence being summed over and n for our general partial sum.
>
>Further $I = \{i \in \mathbb{I} \quad \text{s.t} \quad i > i_0\}$ where $i_0$ is the first term of some sequence
>
>## Results
>
>### Sum laws
>
>Series are linear with respect to scalar multiplies and addition.
>
>### Other series laws
>
>Go over calculus 1 rules for sums of arithmetic squares and cubes.
>
>
>
>
>### Radius of convergence
>
>The radius of convergence (infinity or the number R) is defined as follows
>
>\begin{align*}
>  \text{if} $z=\infty  \quad &R = 0$
>  \text{if} z\in \mathbb{R} \wedge z > 0 \quad &R=\frac{1}{z}\\
>  \text{if} z=0 \quad &R=\infty
>  \text{where} z = \lim \sum_{n \to \infty} \sqrt[n]{c_n}
>\end{align*}
>
>A power series will be convergent for all x values in a+R and a-R and divergent outside, but we don't know at the boundaries.
>
>We must test each boundary separately with other tests.
>
>How to calculate the radius
>
>### Domain of a powers
>
>There are three alternatives for the domain of a powerseries.
>
>1. If R = 0 than the series converges only at x = a
>2. If R = infinity then the series converges for the whole of $\mathbb{R}$
>3. If R is a real greater than zero then the series converges for $|x-a|$ < R and diverges for $|x-a|$ > R
>
>#### Proof
>
>##### i
>We note that if $lim sup _{n\to \infty} = \infty$ then for x not equal to a 
>
>$lim sup \sqrt[n]{|Cn (x-a)^n|} = \lim_{n\to \infty} \sqrt[n]{|C_n|}|x-a| = \infty$
>
>In view of the root test this shows that the power series diverges if $x \neq a$
>
>##### ii and iii
>
>
>If $\lim \sup \sqrt[n]{|c_n|} \in \mathbb{R}$ then
>
>$$\lim \sup \sqrt[n]{|c_n|} \in \mathbb{R} = |x-a|\lim \sup_{\sqrt[n]{c_n}$$
>
>Hence the power series converges for all $x \in \mathbb{R}$ if $\lim\sup\sqrt[n]{c_n}=0$
>
>Finally
>
>Use the root test for part 3
>
>
>## Extras

># Functions
>
> ># Function limits
>>
>>
> >## Limits
>>
> >### General definition
>>
>>If we can find a deleted neighbourhood around $x_0$ such that f(x) always falls in a deleted neighbourhood around $y_0$
>>
>>K and epsilon are synonymous
>>
>>The K and $\epsilon$ works on the range and the delta works on the domain.
>>
> > >### Deleted neighborhood
>>>
>>>Let $a \in \mathbb{R}$ An interval in the form $(c,d)$ with $c<a<d$ is called a neighbourhood of a, and the set $(c,d)/{a}$ is called the deleted neighbourhood of a.
>>>
>>
>>
> >### $x_0 y_0 \in \mathbb{R}$
>>
>>Let f be a real values function and a and L be members of the reals and assume that the domain of f contains the deleted neighbourhood of a.
>>
>>$$\forall \epsilon > 0 \exists \delta > 0 \quad \text{s.t} \quad a-\delta < x < a + \delta \implies |f(x) -L | \epsilon$$
>>
> >#### One sided limit
>>
>>$$\forall \epsilon > 0 \exists \delta \quad \text{s.t} \quad a < x  < a+\delta \implies |f(x)-L | < \epsilon$$
>>
>>
> >### $x \to \infty \vee - \infty$
>>
> >#### Positive infinity
>>
>>Let x be a real function defined on an $[c,\to \infty)$ or $(c,\infty)$
>>
>>If l (the limit of f(x) as x approaches infinity) exists it is defined by.
>>
>>$$\forall \epsilon > 0 \exists K > 0 \quad \text{s.t} \quad x > K \implies |f(x) - l | < \epsilon$$
>>
>>
> >#### Negative infinity
>>
>>Let x be a real function defined on an $(-\infty ,c)$ or $(-\infty,c]$
>>
>>If l (the limit of f(x) as x approaches negative infinity) exists it is defined by.
>>
>>$$\forall E > 0 \exists K < 0 \quad \text{s.t} \quad x < K \implies |f(x) - l | < \epsilon$$
>>
>>
> >### $y \to \infty \vee - \infty$
>>
>>This is when the limit goes to infinity or does not exist.
>>
> >#### Positive infinity
>>
>>
>>Let f be a real function which includes the deleted neighborhood of a
>>
>>f(x) approaches infinity $\iff$
>>
>>$$\forall k>0 \exists \delta > 0 s.t a - \delta < x < a+d f(x) > k$$
>>
> >#### Negative infinity
>>
>>Let f be a real function which includes the deleted neighborhood of a
>>
>>f(x) approaches negative infinity iff
>>
>>$$\forall k<0 \exists \delta > 0 \quad \text{s.t} \quad a - \delta < x < a+d f(x) < k$$
>>
> >### $y,x \to \infty \vee -\infty$
>>
>>f(x) approaches infinity as x approaches infinity
>>
>>$$\forall A > 0 \exists K > 0 s.t x > K \implies f(x) > A$$
>>
>>For the negative case this is similar.
>>
>>Infinites can also be defined using the one-sided limits.
>>
> >### Limit laws
>>
>>    Theorem 3.3
>>
>>We can calculate limits using these laws unless "From definition" is used in the question.
>>
>>Let $a, c \in \mathbb{R}$ and suppose that the real function f and g are defined in a deleted neighbourhood of a and that $\lim_{ x \to a } f(x) = L \in \mathbb{R}$  and $\lim_{ x \to a} g(x) = M \in \mathbb{R}$
>>
>> #. $\lim_{ x \to a} c = c$
>> #. $\lim_{ x \to a}[f(x) + g(x)] = L + M$
>> #. $\lim_{ x \to a} [f(x)-g(x)] = L-M$ 
>> #. $\lim_{ x \to a} [cf(x)] = cL$ 
>> #. $\lim_{ x \to a} [f(x)g(x)] = LM$
>> #. ratio
>>    #. if $M = 0 \wedge L = 0$ use L'Hopital 
>>    #. if $M = 0 \wedge L \neq 0$ the limit does not exist
>>    #. if $M\neq 0$ $\lim_{ x \to a} \frac{ f(x) }{ g(x)} = \frac{ L }{ M }$ 
>> #. $\lim_{ x \to a} x = a$ 
>> #. $\forall n \in \mathbb{N}$ 
>>    #. $\lim_{ x \to a} x^n = a^n$
>>    #. $\lim_{ x \to a} \sqrt[n]{x} = \sqrt[n]{a}$ If n is even we assume a > 0
>>    #. $\lim_{ x \to a} \sqrt[n]{f(x)}= \sqrt[n]{L}$ 
>> #. $\lim_{ x \to a} \left| f(x)\right| \implies L = 0$ 
>>
>>All The limit laws
>>
>>Remember to use la hoptial when on fractions when top and bottom limit is equal to one
>>
> >#### Proofs of 2 6 and 7.3
>>
>>other proofs are exactly the same as in chapter 2
>>
> >##### 2
>>
>>The sum of the limits is the limit of the sums. 
>>
>>We already now that the limit of f and g exist.
>>
>>let $\epsilon > 0$ then there exists $\delta_1 \wedge \delta_2$ s.t 
>>
>>$|f(x) - L | < \frac{\epsilon}{2} if 0 < |x - a| < \delta_1$
>>$|g(x) - L | < \frac{\epsilon}{2} if 0 < |x -a| < \delta_2$
>>
>>Put delta as the minimum delta between delta 1 and delta 2. So $|x-a| < \delta_1 \and |x-a| < \delta_2$ 
>>
>>Therefor $|(f(x) + g(x)) - (L + M)| = |(fx - L) + (g x - M)|$
>>
>>by Triangle inequality
>>
>>$$\leq |f (x) - L| + | g(x) - M |$$
>>
>>$$\leq \frac{epsilon}{2} + \frac{\epsilon}{2} = \epsilon$$
>>
>>Conclusion
>>
>>f(x) + g(x) approaches L+M as $x \to a$
>>
> >##### 6
>>
>>Product rule
>>
> >###### Case 1
>>
>>L and M are both zero
>>
>>Let $\epsilon > 0 \exist \delta_1 \wedge \delta_2$ s.t
>>
>>$|f(x) -L | < 1 \quad \forall |x-a| < \delta_1$
>>
>>and $|g(x) -M | \< \epsilon if 0 < |x-a| < \delta_2$
>>
>>Use the minimum delta 
>>
>>$$|f(x)g(x) - LM| = |f(x)g(x)| = |fx|\times|gx| < 1 \times \espsilon = \epsilon$$
>>
> >###### Case 2
>>
>>For all functions.
>>
>>We expand $f(x)g(x)$ to get terms which we can use previous laws on
>>
>>$$f(x)g(x) = (f(x) -L)(g(x)-M) + L(g(x)-M) -f(x)M$$
>>
>>Using 1 2 and 3 and the definition of limits we get.
>>
>>$$\lim_{x \to a}f(x)g(x) = LM$$
>>
> >##### 7.3
>>
> >####### Case 1
>>
>>$f(x)=1$
>>
>>We must show
>>
>>$$\left|\frac{1}{g(x)} - \frac{1}{M} \right| < \epsilon$$
>>
>>
>>Since $g(x) \neq 0$ 
>>
>>
> >## Examples
>>
>>$$f(x) = \frac{1}{}$$
>>
>>find
>>
>>$$\lim_{n \to \infty} f(x)$$
>>
>>And
>>
>>$$\lim_{x \to -infty f(x)$$
>>
>>
>>Start by guessing L = 0 for both and show their is no contradiction
>>
>>Let $\epsilon > 0$ and $k=\frac{1}{\epsilon} > 0$
>>
>>3 Then x > K gives 
>>
>>$$\left| \frac{1}{x} \right| = \frac{1}{x} < \frac{1}{k} = \epsilon$$
>>
>>When ever we work with limit definitions we will always follow this four step pattern.
>>
>>In a test we must rewrite all steps for negative infinity
>>
>>3 $$\left| \frac{1}{x} \right| = -\frac{1}{x} < -\frac{1}{k} = \epsilon$$
>>
>>
>
> ># Continuity
>>
> >## Definition
>>
>>    3.7
>>
>>Continuity is now give in terms of a delta epsilon definition.
>>
>>$$\forall \epsilon > 0 \exists \delta > 0 \forall 0 \leq |x-a| < \delta \implies |f(x) - f(a)|$$
>>
>>Subtle difference L is replaced with f(a)
>>
>>This is the same but without the deleted interval
>>
> >## Theorem 3.13
>>
>>The following functions are all continuous
>>
>>* All trig functions on thier domains
>>* The exponential and logarithmic function
>>* Absolute value function
>>* Polynomials
>>* Rationals
>>* Roots
>>
> >### Proofs
>>
>>i ii iii and vii all easily follow from limit laws others are part of tutorials.
>>
> >#### vi
>>
>>forall epsilon > 0 let delta = epsilon than forall
>>
> >## Theorem 3.14
>>
>>Let a be a real function and f be a real function definied in the neighbourhood of a. Then f is contionious at a iff for each sequence xn in the domain of f with the limit of fa =f(a) then the sequence f(x_{a}) satisfies $\lim_{n\to\infty} f(na)=f(x)$ If we can find a sequence.
>>
>>If we take a sequecnce and substitute those x values into the function we map the domain onto the function. Then we will have the sequence of our function tending towards f(a)
>>
> >### Proof
>>
> >#### Part one forward direction
>>
>>Assume f is continuous
>>
>>Let $(x_{a})$ be a seq in the dom(f) with $\lim_{n\to \infty} x_{a} = a$
>>
>>We must show $\lim_{} f(an) f(a)$. Hence let $\epsilon > 0$.
>>
>>Since f is cont at a, there is $\delta > 0$ such that
>>
>>$$\left|x-a\right| < \delta \implies \left|f(x) - f(a)\right| < \epsilon$$
>>
>>Since $\lim_{n\to \infty} x_{n} = a$, there is $k \in \mathbb{R}$
>>
>>
>>
> >## The range of continuous functions with bounded domains are bounded
>>
>>    Theorem 3.17
>>
> >### Proof
>>
>>*Use proof by contradiction*
>>
>>Assume the $f([a,b])$ is unbounded. We need to transform the function into a sequence. We do this by bisecting the interval and  replacing the upper or lower bound with the midpoint. Then we look at the sequence of upper and lower bounds. 
>>
>>
>>Then let $d$ be the midpoint of the interval.
>>
>>So
>>$f([a,b]) = f([a,d]) \cup f([d,b])$ is unbounded $\implies$ either $f([a,d])$ or $f([d,b])$ is unbounded.
>>
>>By induction we find an interval $[a_n, b_n]$ where such that $a_n$ is increasing $b_n$ is decreasing  and $f([a_n,b_n])$ is unbounded. 
>>
>>$$b_n = a_n + \frac{1}{2}(b_{n-1} + a_{n-1}) = a_n + 2^{-n}(a-b)$$
>>
>>So by limit laws 
>>
>>$c := \lim a_n = \lim b_n$  and  $c \in [a, b]$(why)
>>
>>Since $f$ is continuous at $c$  $\exists \delta > 0 \quad \text{such that} \quad (f((c-\delta, c+\delta) \cap [a,b]) \subset (f(c)-1,f(c) + 1)$
>>
>>$$\exists K_0, K_1 \in \mathbb{N} \quad \text{such that} \quad \forall n > K \, |c_n - b_n| < \delta, \; \forall n > K_1 |c_n - a_n| < \delta$$
>>
>>Taking $K_2 = \max{K_0,K_1} \quad \forall n > K_2 \quad c_n - \delta < a_n < c < b_{n} < c_{n} - \delta$
>>
>>And so
>> $$f([ a_{n} , b_{n} ]) \subset (f((c-\delta, c+\delta) \cap [a,b]) \subset (f(c)-1,f(c) + 1)$$
>>
>> Which gives $f([ a_{n}, b_{n}])$ is both bounded and unbounded.
>>
>>
> >## A function on a closed interval has a minimum and maximum
>>
>>    Theorem 3.18
>>
> >### Proof
>>
>>Let $f$ be a function defined on the closed interval $[a,b]$ we need to show $is x_{0} x_{2} \in [a,b] \quad \text{such that}$
>>
>>  #. $f(x_1) = \sup f([a,b])$
>>  #. $f(x_2) = \inf f([a,b])$
>>
> >#### Part 2
>>
>>By 
>>
> > >## The range of continuous functions with bounded domains are bounded
>>>
>>>    Theorem 3.17
>>>
> > >### Proof
>>>
>>>*Use proof by contradiction*
>>>
>>>Assume the $f([a,b])$ is unbounded. We need to transform the function into a sequence. We do this by bisecting the interval and  replacing the upper or lower bound with the midpoint. Then we look at the sequence of upper and lower bounds. 
>>>
>>>
>>>Then let $d$ be the midpoint of the interval.
>>>
>>>So
>>>$f([a,b]) = f([a,d]) \cup f([d,b])$ is unbounded $\implies$ either $f([a,d])$ or $f([d,b])$ is unbounded.
>>>
>>>By induction we find an interval $[a_n, b_n]$ where such that $a_n$ is increasing $b_n$ is decreasing  and $f([a_n,b_n])$ is unbounded. 
>>>
>>>$$b_n = a_n + \frac{1}{2}(b_{n-1} + a_{n-1}) = a_n + 2^{-n}(a-b)$$
>>>
>>>So by limit laws 
>>>
>>>$c := \lim a_n = \lim b_n$  and  $c \in [a, b]$(why)
>>>
>>>Since $f$ is continuous at $c$  $\exists \delta > 0 \quad \text{such that} \quad (f((c-\delta, c+\delta) \cap [a,b]) \subset (f(c)-1,f(c) + 1)$
>>>
>>>$$\exists K_0, K_1 \in \mathbb{N} \quad \text{such that} \quad \forall n > K \, |c_n - b_n| < \delta, \; \forall n > K_1 |c_n - a_n| < \delta$$
>>>
>>>Taking $K_2 = \max{K_0,K_1} \quad \forall n > K_2 \quad c_n - \delta < a_n < c < b_{n} < c_{n} - \delta$
>>>
>>>And so
>>> $$f([ a_{n} , b_{n} ]) \subset (f((c-\delta, c+\delta) \cap [a,b]) \subset (f(c)-1,f(c) + 1)$$
>>>
>>> Which gives $f([ a_{n}, b_{n}])$ is both bounded and unbounded.
>>>
>>>
>>
>>We know that $f([a,b])$ has an superium 
>>
>>*By contradiction*
>>
>>Assume that $\nexists x \in [a,b] \quad \text{such that} \quad f(x) = \sup f([a,b])$ 
>>
>>$$g x := \frac{1}{\sup f([a,b]) - f(x)} \, , x \in [a,b]$$
>>
>>So by theorem 3.18 and 3.13 g is both continuous and bounded on the domain $[a,b]$
>>
>>$$\exists K \quad \text{such that} \quad 0 < g(x) \leq K forall x \in [a,b]$$
>>
>>$$\implies \frac{1}{K} \leq \frac{1}{g(x)} = M -f(x) \\ 
>>\implies f(x) < M - \frac{1}{K}$$
>>
>>So M there is a number smaller than M which bounds $f([a,b])$ Which is a contradiction
>>
>>
>
>## Proof of part 2
>
>Assume f is not cont at a. Then 
>$$\exists \epsilon > 0 \forall \delta > 0, \exists x \in dom(f) , |x-a| < \delta$$
>
>and $\left| f(x) - f(a)\right| \geq \epsilon$
>
>In particular, for $\delta = \frac{1}{n}$
>
>This producers a contradiction therefor the function f is continius on a.
>
>## Tutorial 3.4.1
>## Intermediate value theorem
>
>
>If we have a function f. If we pick a value a and a value b so that a is not equal to b and we look at the interval a b. If the value y = k goes through f(a) and f(b) than y goes through both values. There must exist a value c in (a,b) such that f(c) =k
>
>### Proof
>
>Create a new function g(x) = f(x) - k so we now have values greater than or less than zero.
>
>g(a) g(b) < 0 as only one is positive. 
>Then keep bisecting the interval and use new intervals until we get to zero.
>
>If the process does not decrease $b_{n}$ decrease $a_{n}$ increases
>
><<<<<<< HEAD
>Both increasing and decreasing sequences tend to c. It follows that the 
>
>$$limt of g(c)^{2} = limg(a) \lim_{b} \leq 0$$
>=======
>Both increasing and decreasing sequences tend to c. It follows that the limt of $$g(c)^{2} = limg(a) \lim_{b} \leq 0$$
>>>>>>>> 30f8de8bd588de7a343136dafb496a98c103c2a2
>
>As a sqaure can never be negative the only solution is that the limit is zero.
>
>
>Suppose there is a close interval
>
>
>## Test question
>
>Prove sin x is continuous in $x \in \mathbb{R}$
>
>Let $\epsilon > 0$ $f\left|f(x) - f(a)\right| < \delta$
>
>$$\left|sin(x) - sin(a)\right| = \left|2cos\frac{x+a}{2} sin\frac{x-a}{2}\right|$$
>
>Since $\left|\right|$
>How do we prove this.
>
><<<<<<< HEAD
>## When is a set an
>
>Closed is resrved by a functions as endpoint.
>
>
>## Theorem 3.18
>
>A continius function on a closed interval achievs its infimium and suppremuim.
>
>## Theorem 3.19
>
>If f is contiunius on a closed interval than eithier f(x) is a single value or a closed interval
>
>### Proove
>
>Use cor 3.16 and Thm 3.17 with 3.18 it follows tha f([a,b]) it follows that either a singlton or a bounded interval which contains both its inf and its sup. But such an interval is of the form [c.d] with c<d. The supremuim and infiumium condition ensures a inteerval not a supremium.
>
>Let f be function and i be an interval and i be a function which is the inverse if f is strictly monotonic continius function. Then the inverse of the function exists and is continius.
>
>#### Proof
>
>by cor 3.16 f(i) is an inteval
>
>Start with let x be increasing.
>
>then also f inverse is strictly increasing.
>
>Let b be an element of f of i. ie b = f(a) for some a in i
>
>The domain of a function is the range of the inverse and the range of a.
>
>If a is not the left endpoint of i then b is not the left end point of b(a) and for y in f(i) with y < b = f(a) we have f^{-1} we have f^{-1} f(y) < f^{-1}(f(a)) = a
>
>Therefor f inverse is bounded above and increasing on f(i) intersectino with minus infinity b and thus alpha $\lim_{ y \to b^-} f^{-1}(y)$ 
>
>We know this limit exists and alpha \leq f(-1)(b) = a, see Thm 3.8
>
>Assume that alpha is less than a. Since a in I is not the left end point of I , $I\intersection (\alpha,a) \neq \phi.$ Let $x_1 \in I \intersection (\alpha,a) 
>
>Then f(x_1 < f(a) = b ) $\implies$
>  $x_1 = f^{-1} (f(x_0)) \leq \alpha.
>
>  This gives the contradciton. $x_0 > \alpha$ which gives
>
>  \lim_{ y \to b^{-1}} f^{-1} y = a = f^{-1}(b)$$
>
>  $\implies$ $f^{-1}$ is cont from the left. Similarly one can show that f^{-1} is continius from the right. Therefor f^{-1} is continius
>
>The case f strictly decreasing is similar.
>
>Since we have monotonicty it means we need to proove both that x is increasing and decraseing.
>
>
>=======
>## Theorem 3.17
>
>If f is continuous on the interval [a,b] -> f(I) is bounded
>
>### Proof
>
>Only a one way proof as the backwards is not always true
>
>Contradiction:
>
>    Assume the f([a,b]) is unbounded
>
>    Let B be  the midpoint of [a,b]
>
>    $\implies$ one of of the sets f([a,B]), f(B,b) would be unbounded
>    because otherwise f([a,b]) = f([a,b]) = f(a,d) union f([a,b]) would be bounded
>    By induction, we find subintervals [a_n,b_n] of [a,b] such that (a_n) forms a sequence which is increasing (bn) is de, f([a_n, b_n]) is unbounded and b_n = a_n + \frac{ 1 }{ 2 } \left(b_{n-1} -a_[n-1] \right) 
>
>    We infer that both sequences converge with $C = \lim_{ n \to \infty} b_m = \lim_{ n \to \infty} a_n \quad c \in [a,b]
>

># Differentiation
>
>>Note : This is note itself examinable but other dependent things are. Check with new
>
>\begin{align*}
>A \subset R \quad \text{s.t}& \quad \forall a \in A \\
>&\exists \epsilon > 0 \quad \text{s.t} \quad (a-\epsilon, a] \subset A \wedge [a,a+\epsilon) \subset A \iff (a-\epsilon, a+\epsilon) \subset A 
>\end{align*}
>
>
>## Definition
>
>Let $f:A \to \mathbb{R} \wedge a \in A$ 
>
>f is called differentiable at a if the derivative f'(a)
>
>$$\exists f'(a) (\text{the derivative of f at a }) := \lim_{ x \to a} \frac{ f(x) - f(a) }{ x-a }$$
>
>## Differentiability implies continuity (Theorem 4.1)
>
>Let $f : A \to \mathbb{R} \wedge a \in A$. If f is differentiable at a then f is continuous at a
>
>### Proof
>
>\begin{align*}
>&f(x) = f(a) + \frac{ f(x) - f(a) }{ x-a} (x-a) \\
>\iff& \lim_{ x \to a} f(x) = f(a) + \frac{ f(x) -f(a) }{x-a} = f(a) + [f'(a)](0) = f(a)
>\end{align*}
>
>Using 
>
> >### Limit laws
>>
>>    Theorem 3.3
>>
>>We can calculate limits using these laws unless "From definition" is used in the question.
>>
>>Let $a, c \in \mathbb{R}$ and suppose that the real function f and g are defined in a deleted neighbourhood of a and that $\lim_{ x \to a } f(x) = L \in \mathbb{R}$  and $\lim_{ x \to a} g(x) = M \in \mathbb{R}$
>>
>> #. $\lim_{ x \to a} c = c$
>> #. $\lim_{ x \to a}[f(x) + g(x)] = L + M$
>> #. $\lim_{ x \to a} [f(x)-g(x)] = L-M$ 
>> #. $\lim_{ x \to a} [cf(x)] = cL$ 
>> #. $\lim_{ x \to a} [f(x)g(x)] = LM$
>> #. ratio
>>    #. if $M = 0 \wedge L = 0$ use L'Hopital 
>>    #. if $M = 0 \wedge L \neq 0$ the limit does not exist
>>    #. if $M\neq 0$ $\lim_{ x \to a} \frac{ f(x) }{ g(x)} = \frac{ L }{ M }$ 
>> #. $\lim_{ x \to a} x = a$ 
>> #. $\forall n \in \mathbb{N}$ 
>>    #. $\lim_{ x \to a} x^n = a^n$
>>    #. $\lim_{ x \to a} \sqrt[n]{x} = \sqrt[n]{a}$ If n is even we assume a > 0
>>    #. $\lim_{ x \to a} \sqrt[n]{f(x)}= \sqrt[n]{L}$ 
>> #. $\lim_{ x \to a} \left| f(x)\right| \implies L = 0$ 
>>
>>All The limit laws
>>
>>Remember to use la hoptial when on fractions when top and bottom limit is equal to one
>>
> >#### Proofs of 2 6 and 7.3
>>
>>other proofs are exactly the same as in chapter 2
>>
> >##### 2
>>
>>The sum of the limits is the limit of the sums. 
>>
>>We already now that the limit of f and g exist.
>>
>>let $\epsilon > 0$ then there exists $\delta_1 \wedge \delta_2$ s.t 
>>
>>$|f(x) - L | < \frac{\epsilon}{2} if 0 < |x - a| < \delta_1$
>>$|g(x) - L | < \frac{\epsilon}{2} if 0 < |x -a| < \delta_2$
>>
>>Put delta as the minimum delta between delta 1 and delta 2. So $|x-a| < \delta_1 \and |x-a| < \delta_2$ 
>>
>>Therefor $|(f(x) + g(x)) - (L + M)| = |(fx - L) + (g x - M)|$
>>
>>by Triangle inequality
>>
>>$$\leq |f (x) - L| + | g(x) - M |$$
>>
>>$$\leq \frac{epsilon}{2} + \frac{\epsilon}{2} = \epsilon$$
>>
>>Conclusion
>>
>>f(x) + g(x) approaches L+M as $x \to a$
>>
> >##### 6
>>
>>Product rule
>>
> >###### Case 1
>>
>>L and M are both zero
>>
>>Let $\epsilon > 0 \exist \delta_1 \wedge \delta_2$ s.t
>>
>>$|f(x) -L | < 1 \quad \forall |x-a| < \delta_1$
>>
>>and $|g(x) -M | \< \epsilon if 0 < |x-a| < \delta_2$
>>
>>Use the minimum delta 
>>
>>$$|f(x)g(x) - LM| = |f(x)g(x)| = |fx|\times|gx| < 1 \times \espsilon = \epsilon$$
>>
> >###### Case 2
>>
>>For all functions.
>>
>>We expand $f(x)g(x)$ to get terms which we can use previous laws on
>>
>>$$f(x)g(x) = (f(x) -L)(g(x)-M) + L(g(x)-M) -f(x)M$$
>>
>>Using 1 2 and 3 and the definition of limits we get.
>>
>>$$\lim_{x \to a}f(x)g(x) = LM$$
>>
> >##### 7.3
>>
> >####### Case 1
>>
>>$f(x)=1$
>>
>>We must show
>>
>>$$\left|\frac{1}{g(x)} - \frac{1}{M} \right| < \epsilon$$
>>
>>
>>Since $g(x) \neq 0$ 
>>
>>
>
>### Inverse
>
>The inverse statement is false and the proof is to find a simple counter example. Such as the absolute value function.
>
>## Derivative laws
>
>    Theorem 4.2
>
>Let $f,g : A \to R, a \in A,$  f and g differentiable as a and $c \in \mathbb{R}$. Then.
>
>  #.Linearity of derivatives
>      #. 
>Bunch of limit laws
>
>Sum is continuum
>
>Proof of limit rules.
>
>
>### Proof of the product rule.
>
>
>$f \dot g$
>




# Appendix

