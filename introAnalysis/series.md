---
title: "Series"
---

# Imports

import seriesDefinitions as sd
import seriesTest as st

# Series

## __sd: Series definitions; Series__

## __st: Series tests__
## Todo

* fix ratio test
* Add proof that conditional convergent series can be made to equal anything if terms are ordered correctly.
* Proof for absolute value test
* Add examples

## Irrelevant insights

For the whole of limits we often use a $\forall \epsilon$ statement. But what we need to know is that the series is true for arbitrarily small epsilons(if the series approaches a real number) so if we must limit the upper size of epsilon that is fine.

## Notation

Throughout this section we will use i as out general term in the sequence being summed over and n for our general partial sum.

Further $I = \{i \in \mathbb{I} \quad \text{s.t} \quad i > i_0\}$ where $i_0$ is the first term of some sequence

## Results

### Sum laws

Series are linear with respect to scalar multiplies and addition.

### Other series laws

Go over calculus 1 rules for sums of arithmetic squares and cubes.




### Radius of convergence

The radius of convergence (infinity or the number R) is defined as follows

\begin{align*}
  \text{if} $z=\infty  \quad &R = 0$
  \text{if} z\in \mathbb{R} \wedge z > 0 \quad &R=\frac{1}{z}\\
  \text{if} z=0 \quad &R=\infty
  \text{where} z = \lim \sum_{n \to \infty} \sqrt[n]{c_n}
\end{align*}

A power series will be convergent for all x values in a+R and a-R and divergent outside, but we don't know at the boundaries.

We must test each boundary separately with other tests.

How to calculate the radius

### Domain of a powers

There are three alternatives for the domain of a powerseries.

1. If R = 0 than the series converges only at x = a
2. If R = infinity then the series converges for the whole of $\mathbb{R}$
3. If R is a real greater than zero then the series converges for $|x-a|$ < R and diverges for $|x-a|$ > R

#### Proof

##### i
We note that if $lim sup _{n\to \infty} = \infty$ then for x not equal to a 

$lim sup \sqrt[n]{|Cn (x-a)^n|} = \lim_{n\to \infty} \sqrt[n]{|C_n|}|x-a| = \infty$

In view of the root test this shows that the power series diverges if $x \neq a$

##### ii and iii


If $\lim \sup \sqrt[n]{|c_n|} \in \mathbb{R}$ then

$$\lim \sup \sqrt[n]{|c_n|} \in \mathbb{R} = |x-a|\lim \sup_{\sqrt[n]{c_n}$$

Hence the power series converges for all $x \in \mathbb{R}$ if $\lim\sup\sqrt[n]{c_n}=0$

Finally

Use the root test for part 3


## Extras

